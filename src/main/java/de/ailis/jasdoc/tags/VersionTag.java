/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Version tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class VersionTag implements MergeableTag<VersionTag>
{
    /** The versions. */
    private final List<String> versions = new ArrayList<String>();

    /**
     * Constructor.
     *
     * @param versions
     *            The versions. Must not be null or empty.
     */
    public VersionTag(final Collection<String> versions)
    {
        if (versions == null)
            throw new IllegalArgumentException("versions is null");
        if (versions.isEmpty())
            throw new IllegalArgumentException("versions is empty");
        for (final String version: versions) addVersion(version);
    }

    /**
     * Constructor.
     *
     * @param version
     *            The version. Must not be null.
     */
    public VersionTag(final String version)
    {
        addVersion(version);
    }

    /**
     * Adds a version.
     *
     * @param version
     *            The version to add.
     */
    private void addVersion(final String version)
    {
        if (version == null)
            throw new IllegalArgumentException("version is null");
        if (version.isEmpty()) throw new IllegalArgumentException(
            "@version tag without argument");
        this.versions.add(HTMLUtils.simplify(version));
    }

    /**
     * Returns the versions.
     *
     * @return The versions.
     */
    public List<String> getVersions()
    {
        return Collections.unmodifiableList(this.versions);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        for (final String version : this.versions)
            builder.append("@version " + version + "\n");
        return builder.toString();
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public VersionTag merge(final VersionTag other)
    {
        final List<String> versions = new ArrayList<String>();
        versions.addAll(this.getVersions());
        versions.addAll(other.getVersions());
        return new VersionTag(versions);
    }
}
