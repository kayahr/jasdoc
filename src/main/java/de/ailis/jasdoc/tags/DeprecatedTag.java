/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Deprecated tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class DeprecatedTag implements
    MergeableTag<DeprecatedTag>
{
    /** The description. */
    private final String description;

    /** The short description. */
    private final String shortDescription;

    /**
     * Constructor.
     *
     * @param description
     *            The description. Must not be null.
     */
    public DeprecatedTag(final String description)
    {
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.description = HTMLUtils.clean(description);
        this.shortDescription =
            HTMLUtils.simplify(HTMLUtils.getFirstSentence(description));

    }

    /**
     * Returns the description.
     *
     * @return The description.
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Returns the short description.
     *
     * @return The short description.
     */
    public String getShortDescription()
    {
        return this.shortDescription;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@deprecated " + this.description;
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public DeprecatedTag merge(final DeprecatedTag other)
    {
        return new DeprecatedTag(this.description + " "
            + other.description);
    }
}
