/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.ailis.jasdoc.types.AbstractType;
import de.ailis.jasdoc.types.ClassType;
import de.ailis.jasdoc.types.TypeFactory;

/**
 * Implements tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ImplementsTag implements
    MergeableTag<ImplementsTag>
{
    /** The implemented types. */
    private final List<ClassType> types = new ArrayList<ClassType>();

    /**
     * Constructor.
     *
     * @param types
     *            The implemented types. Must not be null or empty.
     */
    public ImplementsTag(final Collection<ClassType> types)
    {
        if (types == null) throw new IllegalArgumentException("types is null");
        if (types.isEmpty())
            throw new IllegalArgumentException("types is empty");
        this.types.addAll(types);
    }

    /**
     * Constructor.
     *
     * @param type
     *            The implemented type. Must not be null.
     */
    public ImplementsTag(final ClassType type)
    {
        if (type == null) throw new IllegalArgumentException("type is null");
        this.types.add(type);
    }

    /**
     * Creates a new implements tag with the specified tag
     * arguments.
     *
     * @param arg
     *            The tag arguments.
     * @param typeFactory
     *            The type factory.
     */
    ImplementsTag(final String arg, final TypeFactory typeFactory)
    {
        if (arg == null) throw new IllegalArgumentException("arg is null");
        if (arg.isEmpty()) throw new IllegalArgumentException(
            "@implements tag without type");
        final AbstractType type = typeFactory.parseInitial(arg);
        if (!(type instanceof ClassType))
            throw new IllegalArgumentException("Invalid implementation type: "
                + arg);
        this.types.add((ClassType) type);
    }

    /**
     * Returns the implemented types.
     *
     * @return The implemented types.
     */
    public List<ClassType> getTypes()
    {
        return Collections.unmodifiableList(this.types);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        for (final ClassType type: this.types)
            builder.append("@implements {" + type.toExpression() + "}\n");
        return builder.toString();
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public ImplementsTag merge(final ImplementsTag other)
    {
        final Set<ClassType> types = new HashSet<ClassType>();
        types.addAll(this.getTypes());
        types.addAll(other.getTypes());
        return new ImplementsTag(types);
    }
}
