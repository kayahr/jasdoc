/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.types.AbstractType;
import de.ailis.jasdoc.types.TypeFactory;
import de.ailis.jasdoc.util.HTMLUtils;
import de.ailis.jasdoc.util.JsTypeUtils;

/**
 * Param tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ParamTag implements Tag
{
    /** The parameter type. */
    private final AbstractType type;

    /** The parameter name. */
    private final String name;

    /** The parameter description. */
    private final String description;

    /**
     * Constructor.
     *
     * @param type
     *            The parameter type. Must not be null.
     * @param name
     *            The parameter name. Must not be null.
     * @param description
     *            The parameter description. Must not be null.
     */
    public ParamTag(final AbstractType type, final String name,
        final String description)
    {
        if (type == null) throw new IllegalArgumentException("type is null");
        if (name == null) throw new IllegalArgumentException("name is null");
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.type = type;
        this.name = name;
        this.description = description;
    }

    /**
     * Constructor which parses the needed arguments from the specified string.
     *
     * @param args
     *            The tag parameters as white-space separated string.
     * @param typeFactory
     *            The type factory.
     */
    ParamTag(final String args, final TypeFactory typeFactory)
    {
        final String[] parts = JsTypeUtils.splitByTypeAndName(args);
        if (parts[1].isEmpty())
            throw new IllegalArgumentException("No name in @Param tag");
        this.type = typeFactory.parseInitial(parts[0]);
        this.name = HTMLUtils.strip(parts[1]);
        this.description = HTMLUtils.clean(parts[2]);
    }

    /**
     * Returns the parameter type.
     *
     * @return The parameter type.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * Returns the parameter name.
     *
     * @return The parameter name.
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Returns the parameter description.
     *
     * @return The parameter description.
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@param {" + this.type.toExpression() + "} " + this.name + " "
            + this.description;
    }
}
