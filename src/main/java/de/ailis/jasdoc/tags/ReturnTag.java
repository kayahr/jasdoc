/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.types.AbstractType;
import de.ailis.jasdoc.types.TypeFactory;
import de.ailis.jasdoc.util.HTMLUtils;
import de.ailis.jasdoc.util.JsTypeUtils;

/**
 * Return tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ReturnTag implements Tag
{
    /** The return type. */
    private final AbstractType type;

    /** The return description. */
    private final String description;

    /**
     * Constructor.
     *
     * @param type
     *            The return type. Must not be null.
     * @param description
     *            The return description. Must not be null.
     */
    public ReturnTag(final AbstractType type, final String description)
    {
        if (type == null) throw new IllegalArgumentException("type is null");
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.type = type;
        this.description = description;
    }

    /**
     * Constructor which parses the needed arguments from the specified string.
     *
     * @param args
     *            The tag parameters as white-space separated string.
     * @param typeFactory
     *            The type factory.
     */
    ReturnTag(final String args, final TypeFactory typeFactory)
    {
        final String[] parts = JsTypeUtils.splitByType(args);
        this.type = typeFactory.parseInitial(parts[0]);
        this.description = HTMLUtils.clean(parts[1]);
    }

    /**
     * Returns the return type.
     *
     * @return The return type.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * Returns the return description.
     *
     * @return The return description.
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@param {" + this.type + "} " + this.description;
    }
}
