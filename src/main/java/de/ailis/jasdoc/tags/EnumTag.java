/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.types.AbstractType;
import de.ailis.jasdoc.types.NumberType;
import de.ailis.jasdoc.types.TypeFactory;

/**
 * Enum tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class EnumTag implements UniqueTag
{
    /** The type. */
    private final AbstractType type;

    /**
     * Constructor.
     *
     * @param type
     *            The type. Must not be null.
     */
    public EnumTag(final AbstractType type)
    {
        if (type == null) throw new IllegalArgumentException("type is null");
        this.type = type;
    }

    /**
     * Creates a new enum tag with the specified tag arguments.
     *
     * @param arg
     *            The tag arguments.
     * @param typeFactory
     *            The type factory.
     */
    EnumTag(final String arg, final TypeFactory typeFactory)
    {
        if (arg == null) throw new IllegalArgumentException("arg is null");
        if (arg.isEmpty())
        {
            this.type = new NumberType();
        }
        else
            this.type = typeFactory.parseInitial(arg);
    }

    /**
     * Returns the type.
     *
     * @return The type.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@enum {" + this.type.toExpression() + "}";
    }
}
