/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.ast.AstNode;

import de.ailis.jasdoc.doc.Configuration;
import de.ailis.jasdoc.types.TypeFactory;

/**
 * TagParser.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */
public final class TagParser
{
    /** The logger. */
    private static final Log LOG = LogFactory.getLog(TagParser.class);

    /** Regular expression pattern to trim a JsDoc comment block. */
    private static final Pattern TRIM_BLOCK_PATTERN = Pattern
        .compile("(?s)^\\s*/\\*\\**\\s*(.*?)\\s*\\**/\\s*$");

    /** Regular expression pattern to trim a JsDoc comment line. */
    private static final Pattern TRIM_LINE_PATTERN = Pattern
        .compile("(?sm)^\\s*\\**\\s{0,1}(.*?)\\s*\\**\\s*$");

    /**
     * Regular expression pattern to split a tag block into an anntotation and
     * its argument.
     */
    private static final Pattern ANNOTATION_PATTERN = Pattern.compile(
        "(?s)^([a-zA-Z]+)\\s*(.*)$");

    /** The type factory. */
    private final TypeFactory typeFactory;

    /** The configuration. */
    private final Configuration config;

    /**
     * Constructor.
     * 
     * @param config
     *            The configuration.
     * @param typeFactory
     *            The type factory.
     */
    public TagParser(final Configuration config, final TypeFactory typeFactory)
    {
        this.config = config;
        this.typeFactory = typeFactory;
    }

    /**
     * Parses the specified JavaScript documentation comment and returns all the
     * tags parsed from it.
     * 
     * @param comment
     *            The comment to parse.
     * @param node
     *            The JavaScript node for fetching the source position if an
     *            error occurred.
     * @return The tags parsed from the comment.
     */
    public Tags parse(final String comment, final AstNode node)
    {
        return parse(comment, null, node);
    }

    /**
     * Parses the specified JavaScript documentation comment and returns all the
     * tags parsed from it.
     * 
     * @param comment
     *            The comment to parse.
     * @param additionals
     *            Additional tags to add.
     * @param node
     *            The JavaScript node for fetching the source position if an
     *            error occurred.
     * @return The tags parsed from the comment.
     */
    public Tags parse(final String comment,
        final Tag[] additionals, final AstNode node)
    {
        final TypeFactory typeFactory = this.typeFactory;
        final Tags tags = new Tags();
        if (comment == null || comment.length() == 0) return tags;

        // Prepare the comment string by trimming the comment block and all
        // comment lines
        String data = trimLines(trimBlock(comment));

        // If comment doesn't start with an tag then prepend a
        // description tag to satisfy the parser (Because
        // all unannotated text is considered to be part of the description).
        if (!data.matches("(?s)^\\s*@.*")) data = "@description\n" + data;

        // Split the data into tag blocks and iterate over them.
        final String[] blocks = data.split("(?m)^\\s*@");
        for (final String block : blocks)
        {
            final Matcher matcher = ANNOTATION_PATTERN.matcher(block);
            if (!matcher.matches()) continue;

            // Split block into an tag name and an argument
            final String annoName = matcher.group(1);
            final String annoArg = matcher.group(2).trim();

            // Process tags (Unknown tags are silently ignored)
            try
            {
                if ("description".equals(annoName))
                    tags.add(new DescriptionTag(annoArg));
                else if ("class".equals(annoName))
                    tags.add(new ClassTag(annoArg));
                else if ("interface".equals(annoName))
                    tags.add(new InterfaceTag());
                else if ("namespace".equals(annoName))
                    tags.add(new NamespaceTag(annoArg));
                else if ("param".equals(annoName))
                    tags.add(new ParamTag(annoArg, typeFactory));
                else if ("return".equals(annoName))
                    tags.add(new ReturnTag(annoArg, typeFactory));
                else if ("type".equals(annoName))
                    tags.add(new TypeTag(annoArg, typeFactory));
                else if ("extends".equals(annoName))
                    tags.add(new ExtendsTag(annoArg, typeFactory));
                else if ("augments".equals(annoName))
                    tags.add(new ExtendsTag(annoArg, typeFactory));
                else if ("implements".equals(annoName))
                    tags.add(new ImplementsTag(annoArg, typeFactory));
                else if ("deprecated".equals(annoName))
                    tags.add(new DeprecatedTag(annoArg));
                else if ("enum".equals(annoName))
                    tags.add(new EnumTag(annoArg, typeFactory));
                else if ("constructor".equals(annoName))
                    tags.add(new ConstructorTag());
                else if ("private".equals(annoName))
                    tags.add(new PrivateTag());
                else if ("protected".equals(annoName))
                    tags.add(new ProtectedTag());
                else if ("const".equals(annoName))
                    tags.add(new ConstTag());
                else if ("final".equals(annoName))
                    tags.add(new FinalTag());
                else if ("constant".equals(annoName))
                    tags.add(new ConstTag());
                else if (this.config.isAuthorIncluded()
                    && "author".equals(annoName))
                    tags.add(new AuthorTag(annoArg));
                else if ("since".equals(annoName))
                    tags.add(new SinceTag(annoArg));
                else if ("see".equals(annoName))
                    tags.add(new SeeTag(annoArg));
                else if ("version".equals(annoName))
                    tags.add(new VersionTag(annoArg));
                else if ("inheritDoc".equals(annoName))
                    tags.add(new InheritDocTag());
            }
            catch (final IllegalArgumentException e)
            {
                LOG.warn(e.getMessage() + " in " + node.getAstRoot().
                    getSourceName() + " line " + node.getLineno(), e);
            }
        }

        // Add additional tags
        if (additionals != null)
            for (final Tag tag : additionals)
                tags.add(tag);

        return tags;
    }

    /**
     * Removes the beginning and end markers of the JsDoc comment.
     * 
     * @param comment
     *            The JsDoc comment to trim.
     * @return The trimmed JsDoc comment.
     */
    private static String trimBlock(final String comment)
    {
        final Matcher matcher = TRIM_BLOCK_PATTERN.matcher(comment);
        if (matcher.matches())
            return matcher.group(1);
        else
            return comment;
    }

    /**
     * Removes all space and star characters from all lines which are not part
     * of the documentation itself.
     * 
     * @param comment
     *            The JsDoc comment to trim.
     * @return The trimmed JsDoc comment.
     */
    private static String trimLines(final String comment)
    {
        final Matcher matcher = TRIM_LINE_PATTERN.matcher(comment);
        if (matcher.matches())
            return matcher.replaceAll("$1");
        else
            return comment;
    }
}
