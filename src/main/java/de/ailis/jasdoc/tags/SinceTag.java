/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Since tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class SinceTag implements MergeableTag<SinceTag>
{
    /** The since texts. */
    private final List<String> sinceTexts = new ArrayList<String>();

    /**
     * Constructor.
     *
     * @param sinceTexts
     *            The since texts. Must not be null or empty.
     */
    public SinceTag(final Collection<String> sinceTexts)
    {
        if (sinceTexts == null)
            throw new IllegalArgumentException("sinceTexts is null");
        if (sinceTexts.isEmpty())
            throw new IllegalArgumentException("sinceTexts is empty");
        for (final String sinceText: sinceTexts) addSince(sinceText);
    }

    /**
     * Constructor.
     *
     * @param since
     *            The since text. Must not be null.
     */
    public SinceTag(final String since)
    {
        addSince(since);
    }

    /**
     * Adds a since text.
     *
     * @param since
     *            The since text to add.
     */
    private void addSince(final String since)
    {
        if (since == null)
            throw new IllegalArgumentException("author is null");
        if (since.isEmpty()) throw new IllegalArgumentException(
            "@since tag without argument");
        this.sinceTexts.add(HTMLUtils.simplify(since));
    }

    /**
     * Returns the since texts.
     *
     * @return The since texts.
     */
    public List<String> getSince()
    {
        return Collections.unmodifiableList(this.sinceTexts);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        for (final String since : this.sinceTexts)
            builder.append("@since " + since + "\n");
        return builder.toString();
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public SinceTag merge(final SinceTag other)
    {
        final List<String> sinceTexts = new ArrayList<String>();
        sinceTexts.addAll(this.getSince());
        sinceTexts.addAll(other.getSince());
        return new SinceTag(sinceTexts);
    }
}
