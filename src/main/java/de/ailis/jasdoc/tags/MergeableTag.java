/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;


/**
 * Interface for mergeable tags.
 *
 * @author Klaus Reimer (k@ailis.de)
 * @param <T>
 *            The tag type.
 */
public interface MergeableTag<T extends Tag> extends
    UniqueTag
{
    /**
     * Merges this tag with the specified tag and returns a new
     * tag of the same type containing the data from both merged
     * tags.
     *
     * @param other
     *            The other tag to merge this one with.
     * @return The new tag with the merged content.
     */
    T merge(final T other);
}
