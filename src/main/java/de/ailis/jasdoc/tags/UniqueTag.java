/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;


/**
 * Interface for unique tags.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface UniqueTag extends Tag
{
    // Empty
}
