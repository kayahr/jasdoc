/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

/**
 * Private tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class PrivateTag implements UniqueTag
{
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@private";
    }
}
