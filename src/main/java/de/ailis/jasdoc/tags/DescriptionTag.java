/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Description tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class DescriptionTag implements
    MergeableTag<DescriptionTag>
{
    /** The description. */
    private final String description;

    /**
     * Constructor.
     *
     * @param description
     *            The description. Must not be null.
     */
    public DescriptionTag(final String description)
    {
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.description = HTMLUtils.clean(description);
    }

    /**
     * Returns the description.
     *
     * @return The description.
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@description " + this.description;
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public DescriptionTag merge(final DescriptionTag other)
    {
        return new DescriptionTag(this.description + " "
            + other.description);
    }
}
