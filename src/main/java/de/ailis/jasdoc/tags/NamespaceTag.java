/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Namespace tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class NamespaceTag implements
    MergeableTag<NamespaceTag>
{
    /** The namespace description. */
    private final String description;

    /**
     * Constructor.
     *
     * @param description
     *            The namespace description. Must not be null.
     */
    public NamespaceTag(final String description)
    {
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.description = HTMLUtils.clean(description);
    }

    /**
     * Returns the namespace description.
     *
     * @return The namespace description.
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@namespace " + this.description;
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public NamespaceTag merge(final NamespaceTag other)
    {
        return new NamespaceTag(this.description + " "
            + other.description);
    }
}
