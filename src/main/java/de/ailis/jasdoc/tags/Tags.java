/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tags.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class Tags
{
    /**
     * Index to quickly navigate to the last added tag of a specific
     * type.
     */
    private final Map<Class<?>, Tag> index =
        new HashMap<Class<?>, Tag>();

    /** The list of tags. */
    private final List<Tag> tags =
        new ArrayList<Tag>();

    /**
     * Constructs a new empty list of tags.
     */
    public Tags()
    {
        // Empty
    }

    /**
     * Constructs a new list of tags pre-filled with the specified
     * tags.
     *
     * @param tags
     *            The initial tags.
     */
    public Tags(final Tag... tags)
    {
        for (final Tag tag: tags)
            add(tag);
    }

    /**
     * Adds all specified tags.
     *
     * @param tags
     *            the tags to add.
     */
    void add(final Tags tags)
    {
        for (final Tag tag: tags.tags)
            add(tag);
    }

    /**
     * Merges two tags container and returns the new merged one.
     *
     * @param a
     *            First tags container to merge.
     * @param b
     *            Second tag container to merge.
     * @return New merged tags container.
     */
    public static Tags merge(final Tags a, final Tags b)
    {
        final Tags tags = new Tags();
        tags.add(a);
        tags.add(b);
        return tags;
    }

    /**
     * Adds an tag to the list of tags.
     *
     * @param tag
     *            The tag to add.
     */
    void add(final Tag tag)
    {
        final Class<?> c = tag.getClass();
        final Tag other = this.index.get(c);
        if (other instanceof MergeableTag)
        {
            @SuppressWarnings("unchecked")
            final MergeableTag<Tag> mergeable =
                (MergeableTag<Tag>) other;
            this.tags.remove(other);
            final Tag merged = mergeable.merge(tag);
            this.index.put(c, merged);
            this.tags.add(merged);
        }
        else if (other instanceof UniqueTag)
        {
            this.tags.remove(other);
            this.index.put(c, tag);
            this.tags.add(tag);
        }
        else
        {
            this.index.put(c, tag);
            this.tags.add(tag);
        }
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return this.tags.toString();
    }

    /**
     * Returns the tag of the specified type. If multiple tags
     * exists then the last one is returned.
     *
     * @param <T>
     *            The tag type.
     * @param type
     *            The tag type.
     * @return The tag or null if not present.
     */
    public <T extends Tag> T getTag(final Class<T> type)
    {
        return (T) this.index.get(type);
    }

    /**
     * Checks if the specified tag is present.
     *
     * @param type
     *            The tag type to check.
     * @return True if tag is present, false if not.
     */
    public boolean isTagPresent(final Class<? extends Tag> type)
    {
        return getTag(type) != null;
    }

    /**
     * Returns the param tag for the parameter with the specified name.
     * If there is no such tag then null is returned.
     *
     * @param name
     *            The parameter name.
     * @return The param tag or null if not found.
     */
    public ParamTag getParamTag(final String name)
    {
        for (final Tag tag: this.tags)
        {
            if (tag instanceof ParamTag)
            {
                final ParamTag paramAnno = (ParamTag) tag;
                if (paramAnno.getName().equals(name)) return paramAnno;
            }
        }
        return null;
    }
}
