/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import de.ailis.jasdoc.util.HTMLUtils;


/**
 * Class tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ClassTag implements MergeableTag<ClassTag>
{
    /** The class description. */
    private final String description;

    /**
     * Constructor.
     *
     * @param description
     *            The class description. Must not be null.
     */
    public ClassTag(final String description)
    {
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.description = HTMLUtils.clean(description);
    }

    /**
     * Returns the class description.
     *
     * @return The class description.
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "@class " + this.description;
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public ClassTag merge(final ClassTag other)
    {
        return new ClassTag(this.description + " "
            + other.description);
    }
}
