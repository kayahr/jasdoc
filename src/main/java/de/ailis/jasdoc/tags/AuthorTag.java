/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Author tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class AuthorTag implements MergeableTag<AuthorTag>
{
    /** The authors. */
    private final List<String> authors = new ArrayList<String>();

    /**
     * Constructor.
     *
     * @param authors
     *            The authors. Must not be null or empty.
     */
    public AuthorTag(final Collection<String> authors)
    {
        if (authors == null)
            throw new IllegalArgumentException("authors is null");
        if (authors.isEmpty())
            throw new IllegalArgumentException("authors is empty");
        for (final String author: authors) addAuthor(author);
    }

    /**
     * Constructor.
     *
     * @param author
     *            The author. Must not be null.
     */
    public AuthorTag(final String author)
    {
        addAuthor(author);
    }

    /**
     * Adds an author.
     *
     * @param author
     *            The author to add.
     */
    private void addAuthor(final String author)
    {
        if (author == null)
            throw new IllegalArgumentException("author is null");
        if (author.isEmpty()) throw new IllegalArgumentException(
            "@author tag without argument");
        this.authors.add(HTMLUtils.simplify(HTMLUtils.convertEMail(author)));
    }

    /**
     * Returns the authors.
     *
     * @return The authors.
     */
    public List<String> getAuthors()
    {
        return Collections.unmodifiableList(this.authors);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        for (final String author : this.authors)
            builder.append("@author " + author + "\n");
        return builder.toString();
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public AuthorTag merge(final AuthorTag other)
    {
        final List<String> authors = new ArrayList<String>();
        authors.addAll(this.getAuthors());
        authors.addAll(other.getAuthors());
        return new AuthorTag(authors);
    }
}
