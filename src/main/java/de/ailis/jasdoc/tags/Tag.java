/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

/**
 * Interface for tags.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface Tag
{
    // Empty
}
