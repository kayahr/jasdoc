/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.util.HTMLUtils;

/**
 * See tag.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class SeeTag implements MergeableTag<SeeTag>
{
    /** The references. */
    private final List<String> references = new ArrayList<String>();

    /**
     * Constructor.
     *
     * @param references
     *            The references. Must not be null or empty.
     */
    public SeeTag(final Collection<String> references)
    {
        if (references == null)
            throw new IllegalArgumentException("references is null");
        if (references.isEmpty())
            throw new IllegalArgumentException("references is empty");
        for (final String reference: references) addReference(reference);
    }

    /**
     * Constructor.
     *
     * @param reference
     *            The reference. Must not be null.
     */
    public SeeTag(final String reference)
    {
        addReference(reference);
    }

    /**
     * Adds an reference.
     *
     * @param reference
     *            The reference to add.
     */
    private void addReference(final String reference)
    {
        if (reference == null)
            throw new IllegalArgumentException("reference is null");
        if (reference.isEmpty()) throw new IllegalArgumentException(
            "@see tag without argument");
        this.references.add(HTMLUtils.simplify(HTMLUtils.convertEMail(reference)));
    }

    /**
     * Returns the references.
     *
     * @return The references.
     */
    public List<String> getReferences()
    {
        return Collections.unmodifiableList(this.references);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        for (final String reference : this.references)
            builder.append("@see " + reference + "\n");
        return builder.toString();
    }

    /**
     * @see MergeableTag#merge(Tag)
     */
    @Override
    public SeeTag merge(final SeeTag other)
    {
        final List<String> references = new ArrayList<String>();
        references.addAll(this.getReferences());
        references.addAll(other.getReferences());
        return new SeeTag(references);
    }
}
