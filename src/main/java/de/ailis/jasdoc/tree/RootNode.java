/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tree;

import de.ailis.jasdoc.tags.Tags;


/**
 * The root node in the symbol tree.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class RootNode extends Node
{
    /**
     * Constructor.
     */
    public RootNode()
    {
        super("", new Tags(), null);
    }
}
