/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tree;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.CompilerEnvirons;
import org.mozilla.javascript.Parser;
import org.mozilla.javascript.ast.AstRoot;

import de.ailis.jasdoc.doc.Configuration;

/**
 * JavaScript tree parser.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class TreeParser
{
    /** The logger. */
    private static final Log LOG = LogFactory.getLog(TreeParser.class);

    /** The compiler environment. */
    private final CompilerEnvirons compilerEnv;

    /** The documentation tree builder. */
    private final TreeBuilder treeBuilder;

    /**
     * Constructor.
     *
     * @param config
     *            The configuration.
     */
    public TreeParser(final Configuration config)
    {
        final CompilerEnvirons env = new CompilerEnvirons();
        env.setRecordingComments(true);
        env.setRecordingLocalJsDocComments(true);
        env.setGeneratingSource(true);
        env.setGenerateDebugInfo(true);
        env.setIdeMode(true);
        env.setAllowMemberExprAsFunctionName(true);
        this.compilerEnv = env;

        this.treeBuilder = new TreeBuilder(config);
    }

    /**
     * Parses the JavaScript from the specified input stream.
     *
     * @param stream
     *            The input stream to read the JavaScript from.
     * @throws IOException
     *             When JavaScript stream could not be read.
     */
    public void parseStream(final InputStream stream) throws IOException
    {
        final AstRoot rootNode =
            new Parser(this.compilerEnv).parse(new InputStreamReader(stream),
                "stream", 1);
        parseRootNode(rootNode);
    }

    /**
     * Parses the specified root node.
     *
     * @param rootNode
     *            The root node to parse.
     */
    private void parseRootNode(final AstRoot rootNode)
    {
        rootNode.visit(this.treeBuilder);
    }

    /**
     * Parses the specified JavaScript file or directory. If file is a directory
     * then all JavaScript files and all sub folders are parsed recursively.
     *
     * @param file
     *            The JavaScript file (or directory) to parse.
     * @throws IOException
     *             When JavaScript file could not be read.
     */
    public void parseFile(final File file) throws IOException
    {
        if (file.isDirectory())
        {
            final File[] subs = file.listFiles();
            if (subs == null) return;
            for (final File sub : subs)
            {
                if (sub.getName().toLowerCase().endsWith(".js")
                    || sub.isDirectory()) parseFile(sub);
            }
            return;
        }
        LOG.debug("Parsing " + file + "...");
        final FileReader reader = new FileReader(file);
        try
        {
            final AstRoot rootNode = new Parser(this.compilerEnv).parse(reader,
                file.getAbsolutePath(), 1);

            parseRootNode(rootNode);
        }
        finally
        {
            reader.close();
        }
    }

    /**
     * Returns the global documentation.
     *
     * @return The global documentation.
     */
    public RootNode getTree()
    {
        return this.treeBuilder.getRootNode();
    }
}
