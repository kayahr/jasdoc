/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.tags.Tags;

/**
 * A function node in the symbol tree.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class FuncNode extends Node
{
    /** The function parameters. */
    private final List<Node> parameters = new ArrayList<Node>();

    /**
     * Constructor.
     *
     * @param name
     *            The node name.
     * @param tags
     *            The node tags.
     * @param parentNode
     *            The parent node.
     */
    public FuncNode(final String name, final Tags tags, final Node parentNode)
    {
        super(name, tags, parentNode);
    }

    /**
     * Adds a parameter to the function.
     *
     * @param parameter
     *            The parameter to add
     */
    void addParameter(final Node parameter)
    {
        this.parameters.add(parameter);
    }

    /**
     * Returns the list of parameter nodes.
     *
     * @return The parameter nodes.
     */
    public List<Node> getParameters()
    {
        return Collections.unmodifiableList(this.parameters);
    }
}
