/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tree;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.ailis.jasdoc.tags.ConstructorTag;
import de.ailis.jasdoc.tags.EnumTag;
import de.ailis.jasdoc.tags.InterfaceTag;
import de.ailis.jasdoc.tags.NamespaceTag;
import de.ailis.jasdoc.tags.Tags;

/**
 * A node in the symbol tree.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public class Node
{
    /** The node name. */
    private final String name;

    /** The tags. */
    private Tags tags;

    /** The parent node. */
    private final Node parentNode;

    /** The child nodes. */
    private final Map<String, Node> childNodes = new HashMap<String, Node>();

    /**
     * Constructor.
     *
     * @param name
     *            The node name. Must not be null. Is empty for root.
     * @param tags
     *            The node tags. Must not be null.
     * @param parentNode
     *            The parent node. Null for root.
     */
    public Node(final String name, final Tags tags, final Node parentNode)
    {
        if (name == null) throw new IllegalArgumentException("name is null");
        if (tags == null)
            throw new IllegalArgumentException("tags is null");
        this.name = name;
        this.tags = tags;
        this.parentNode = parentNode;
    }

    /**
     * Returns the child node with the specified name.
     *
     * @param name
     *            The name of the child node.
     * @return The child node or null if not found.
     */
    public final Node getChildNode(final String name)
    {
        return this.childNodes.get(name);
    }

    /**
     * Returns the parent node.
     *
     * @return The parent node or null if this is the root node.
     */
    public final Node getParentNode()
    {
        return this.parentNode;
    }

    /**
     * Returns the name of this node.
     *
     * @return The node name.
     */
    public final String getName()
    {
        return this.name;
    }

    /**
     * Returns the node tags.
     *
     * @return The node tags. Never null.
     */
    public final Tags getTags()
    {
        return this.tags;
    }

    /**
     * Adds the specified child node. If there is already a child node with the
     * same name then the nodes are merged.
     *
     * @param node
     *            The child node to add.
     */
    public final void addChildNode(final Node node)
    {
        final String name = node.getName();
        final Node oldNode = getChildNode(name);
        if (oldNode != null) node.merge(oldNode);
        this.childNodes.put(name, node);
    }

    /**
     * Returns all child nodes of this node.
     *
     * @return All child nodes of this node.
     */
    public final Collection<Node> getChildNodes()
    {
        return Collections.unmodifiableCollection(this.childNodes.values());
    }

    /**
     * Merges this node with the specified node by adding all the child nodes of
     * the other node and by merging the tags.
     *
     * @param other
     *            The other node to merge into this one.
     */
    private void merge(final Node other)
    {
        this.tags =
            Tags.merge(other.tags, this.tags);
        for (final Node childNode: other.getChildNodes())
            addChildNode(childNode);
    }

    /**
     * Checks if this node is a function.
     *
     * @return True if node is a function, false if not.
     */
    public final boolean isFunction()
    {
        return !isClass() && getClass() == FuncNode.class;
    }

    /**
     * Checks if node is a class.
     *
     * @return True if node is a class, false if not.
     */
    public final boolean isClass()
    {
        return this.tags
            .isTagPresent(ConstructorTag.class);
    }

    /**
     * Checks if node is an interface.
     *
     * @return True if node is an interface, false if not.
     */
    public final boolean isInterface()
    {
        return this.tags
            .isTagPresent(InterfaceTag.class);
    }

    /**
     * Checks if node is an enum.
     *
     * @return True if node is a class, false if not.
     */
    public final boolean isEnum()
    {
        return this.tags.isTagPresent(EnumTag.class);
    }

    /**
     * Checks if this node is a class prototype.
     *
     * @return True if class prototype, false if not.
     */
    public final boolean isPrototype()
    {
        if (this.parentNode == null) return false;
        return this.name.equals("prototype");
    }

    /**
     * Checks if node is a namespace.
     *
     * @return True if node is a namespace, false if not.
     */
    public final boolean isNamespace()
    {
        // The root node is always a namespace
        if (getClass() == RootNode.class) return true;

        // When already some other type then it is not a namespace
        if (isClass() || isFunction() || isEnum() || isPrototype())
            return false;

        // If namespace tag is set then this is definitely a namespace
        if (this.tags.isTagPresent(NamespaceTag.class))
            return true;

        // Otherwise check if this node contains functions, classes, enums
        // or other namespaces. If this is the case then this is a namespace
        for (final Node childNode: getChildNodes())
        {
            if (childNode.isFunction() || childNode.isClass()
                || childNode.isNamespace() || childNode.isEnum())
                return true;
        }
        return false;
    }

    /**
     * Checks if this node is a class field.
     *
     * @return True if class field, false if not.
     */
    public final boolean isField()
    {
        if (isFunction() || isPrototype() || this.parentNode == null)
            return false;
        return this.parentNode.isPrototype() || this.parentNode.isClass();
    }

    /**
     * Checks if this node is a variable.
     *
     * @return True if variable, false if not.
     */
    public final boolean isVariable()
    {
        return this.parentNode != null && this.parentNode.isNamespace()
            && !isFunction() && !isClass() && !isNamespace() && !isEnum();
    }

    /**
     * Resolves the specified path into a node. Creates missing nodes while
     * doing this.
     *
     * @param path
     *            The node path.
     * @return The node.
     */
    public final Node resolve(final String path)
    {
        final String[] parts = path.split("\\.", 2);
        final String name = parts[0];
        if (name.isEmpty()) return this;
        Node node = getChildNode(name);
        if (node == null)
        {
            node = new Node(name, new Tags(), this);
            addChildNode(node);
        }
        if (parts.length == 1) return node;
        return node.resolve(parts[1]);
    }

    /**
     * Returns the qualified node name.
     *
     * @return The qualified node name.
     */
    public final String getQualifiedName()
    {
        if (this.parentNode == null) return this.name;
        final String parentName = this.parentNode.getQualifiedName();
        if (parentName.isEmpty()) return this.name;
        return parentName + "." + this.name;
    }
}
