/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.tree;

import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.Assignment;
import org.mozilla.javascript.ast.AstNode;
import org.mozilla.javascript.ast.ExpressionStatement;
import org.mozilla.javascript.ast.FunctionNode;
import org.mozilla.javascript.ast.Name;
import org.mozilla.javascript.ast.NodeVisitor;
import org.mozilla.javascript.ast.ObjectLiteral;
import org.mozilla.javascript.ast.ObjectProperty;
import org.mozilla.javascript.ast.PropertyGet;
import org.mozilla.javascript.ast.VariableDeclaration;
import org.mozilla.javascript.ast.VariableInitializer;

import de.ailis.jasdoc.doc.Configuration;
import de.ailis.jasdoc.tags.ConstructorTag;
import de.ailis.jasdoc.tags.ExtendsTag;
import de.ailis.jasdoc.tags.ImplementsTag;
import de.ailis.jasdoc.tags.InterfaceTag;
import de.ailis.jasdoc.tags.TagParser;
import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.ClassType;
import de.ailis.jasdoc.types.TypeFactory;

/**
 * Builds a symbol tree. Let it visit all the parsed JavaScript root nodes.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class TreeBuilder implements NodeVisitor
{
    /** The type factory. */
    private final TypeFactory typeFactory;

    /** The tag parser. */
    private final TagParser annoParser;

    /** The root node of the tree. */
    private final RootNode root = new RootNode();

    /**
     * Constructor.
     *
     * @param config
     *            The configuration.
     */
    public TreeBuilder(final Configuration config)
    {
        this.typeFactory = new TypeFactory();
        this.annoParser = new TagParser(config, this.typeFactory);
    }

    /**
     * @see NodeVisitor#visit(AstNode)
     */
    @Override
    public boolean visit(final AstNode node)
    {
        switch (node.getType())
        {
            case Token.SCRIPT:
                return true;

            case Token.FUNCTION:
                return visitFunctionNode((FunctionNode) node, null, null,
                    this.root);

            case Token.EXPR_RESULT:
                return visitExpressionStatement((ExpressionStatement) node,
                    this.root);

            case Token.VAR:
                return visitVariableDeclaration((VariableDeclaration) node,
                    this.root);

            default:
                return false;
        }
    }

    /**
     * Visits a variable declaration.
     *
     * @param node
     *            The variable declaration node.
     * @param scope
     *            The current scope.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitVariableDeclaration(final VariableDeclaration node,
        final Node scope)
    {
        String varJsDoc = node.getJsDoc();
        for (final VariableInitializer variable : node.getVariables())
        {
            // Get variable name. Ignore variable if it can't be parsed.
            final AstNode target = variable.getTarget();
            if (target.getType() != Token.NAME) continue;
            final String name = ((Name) target).getIdentifier();

            // Get JsDoc
            final String jsDoc = getJsDoc(variable, varJsDoc);

            // Process the variable value
            final AstNode initializer = variable.getInitializer();
            if (initializer == null)
                visitPropertyName((Name) target, jsDoc, scope);
            else if (initializer.getType() == Token.FUNCTION)
                visitFunctionNode((FunctionNode) initializer, name, jsDoc,
                    scope);
            else if (initializer.getType() == Token.OBJECTLIT)
                visitObjectLiteral((ObjectLiteral) initializer, name, jsDoc,
                    scope);
            else
                visitPropertyValue(initializer, name, jsDoc, scope);

            // Resetting variable declaration JsDoc because it is only usable
            // for first declared variable.
            varJsDoc = null;
        }
        return false;
    }

    /**
     * Visits an expression statement.
     *
     * @param node
     *            The expression statement node.
     * @param scope
     *            The current scope.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitExpressionStatement(final ExpressionStatement node,
        final Node scope)
    {
        final AstNode expression = node.getExpression();
        if (expression.getType() == Token.ASSIGN)
            return visitAssignment((Assignment) expression, scope);
        else if (expression.getType() == Token.NAME)
            return visitPropertyName((Name) expression, null, scope);
        return false;
    }

    /**
     * Returns the JavaScript documentation string from the specified node. If
     * it doesn't have one then the specified fallback is returned instead
     *
     * @param node
     *            The node to read the documentation string from.
     * @param fallback
     *            The fallback documentation string.
     * @return The documentation string to use.
     */
    private String getJsDoc(final AstNode node, final String fallback)
    {
        final String jsDoc = node.getJsDoc();
        if (jsDoc != null) return jsDoc;
        return fallback;
    }

    /**
     * Visits an assignment.
     *
     * @param node
     *            The assignment node.
     * @param scope
     *            The current scope.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitAssignment(final Assignment node,
        final Node scope)
    {
        final AstNode left = node.getLeft();
        String jsDoc = node.getJsDoc();
        String name;
        Node parentScope;

        // If left is a NAME then it is a direct assignment
        if (left.getType() == Token.NAME)
        {
            name = ((Name) left).getIdentifier();
            parentScope = scope;
            jsDoc = getJsDoc(left, jsDoc);
        }

        // If left is a GEPROP then it is a scoped assignment
        else if (left.getType() == Token.GETPROP)
        {
            final PropertyGet propertyGet = (PropertyGet) left;
            name = propertyGet.getProperty().getIdentifier();
            jsDoc = getJsDoc(left, jsDoc);
            parentScope = getScope(propertyGet.getTarget(), scope);
        }

        // Otherwise we don't know what to do with it.
        else
            return false;

        // Process the assignment value
        final AstNode right = node.getRight();
        if (right.getType() == Token.FUNCTION)
        {
            return visitFunctionNode((FunctionNode) right, name, jsDoc,
                parentScope);
        }
        else if (right.getType() == Token.OBJECTLIT)
        {
            return visitObjectLiteral((ObjectLiteral) right, name, jsDoc,
                parentScope);
        }
        else
        {
            return visitPropertyValue(right, name, jsDoc, parentScope);
        }
    }

    /**
     * Visits a function node.
     *
     * @param node
     *            The function node to visit.
     * @param name
     *            The function name. If null then name is read from function
     *            itself (Which only works when function is not anonymous)
     * @param jsDoc
     *            JavaScript documentation string from a possible parent node
     *            location. Null if none.
     * @param scope
     *            The parent scope of the function.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitFunctionNode(final FunctionNode node,
        final String name, final String jsDoc, final Node scope)
    {
        // Get the function name. Ignore function node if not found.
        final String funcName = name == null ? node.getName() : name;
        if (funcName == null) return false;

        // Get the function tags
        final Tags tags =
            this.annoParser.parse(getJsDoc(node, jsDoc), node);

        // Build the function documentation.
        final FuncNode func = new FuncNode(funcName, tags, scope);

        // Add parameters to function documentation.
        for (final AstNode paramNode : node.getParams())
            func.addParameter(processParam(paramNode, func));

        scope.addChildNode(func);

        // Create interface nodes for implemented interfaces
        final ImplementsTag implement =
            tags.getTag(ImplementsTag.class);
        if (implement != null)
        {
            for (final ClassType type : implement.getTypes())
                createInterfaceNode(type.getName());
        }

        // Create interface nodes for implemented interfaces
        final ExtendsTag extend =
            tags.getTag(ExtendsTag.class);
        if (extend != null)
        {
            for (final ClassType type : extend.getTypes())
            {
                if (func.isInterface())
                    createInterfaceNode(type.getName());
                else
                    createClassNode(type.getName());
            }
        }

        return false;
    }

    /**
     * Creates an interface node at the specified path.
     *
     * @param path
     *            The node path.
     */
    private void createInterfaceNode(final String path)
    {
        final Node node = this.root.resolve(path);
        final Node parent = node.getParentNode();
        parent.addChildNode(new FuncNode(node.getName(),
            new Tags(new InterfaceTag()), parent));
    }

    /**
     * Creates a class node at the specified path.
     *
     * @param path
     *            The node path.
     */
    private void createClassNode(final String path)
    {
        final Node node = this.root.resolve(path);
        final Node parent = node.getParentNode();
        parent.addChildNode(new FuncNode(node.getName(),
            new Tags(new ConstructorTag()), parent));
    }

    /**
     * Visits a object literal node.
     *
     * @param node
     *            The object literal node to visit.
     * @param name
     *            The object literal name.
     * @param jsDoc
     *            JavaScript documentation string from a possible parent node
     *            location. Null if none.
     * @param parentScope
     *            The parent scope of the object literal.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitObjectLiteral(final ObjectLiteral node,
        final String name, final String jsDoc, final Node parentScope)
    {
        final Tags tags =
            this.annoParser.parse(getJsDoc(node, jsDoc), node);

        // Create the scope
        final Node scope = new Node(name, tags, parentScope);
        parentScope.addChildNode(scope);

        // Process all object properties
        for (final ObjectProperty property : node.getElements())
        {
            String propJsDoc = property.getJsDoc();

            // Get the property name
            final AstNode left = property.getLeft();
            if (left.getType() != Token.NAME) continue;
            final String propName = ((Name) left).getIdentifier();
            propJsDoc = getJsDoc(left, propJsDoc);

            // Process the property value
            final AstNode right = property.getRight();
            propJsDoc = getJsDoc(right, propJsDoc);
            if (right.getType() == Token.OBJECTLIT)
            {
                visitObjectLiteral((ObjectLiteral) right, propName, propJsDoc,
                    scope);
            }
            else if (right.getType() == Token.FUNCTION)
            {
                visitFunctionNode((FunctionNode) right, propName, propJsDoc,
                    scope);
            }
            else
            {
                visitPropertyValue(right, propName, propJsDoc, scope);
            }
        }

        return false;
    }

    /**
     * Visits a property name node.
     *
     * @param node
     *            The property name node to visit.
     * @param jsDoc
     *            JavaScript documentation string from a possible parent node
     *            location. Null if none.
     * @param parentScope
     *            The parent scope of the property value.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitPropertyName(final Name node, final String jsDoc,
        final Node parentScope)
    {
        parentScope.addChildNode(new Node(node.getIdentifier(),
            this.annoParser.parse(getJsDoc(node, jsDoc), node), parentScope));
        return false;
    }

    /**
     * Visits a property value node.
     *
     * @param node
     *            The property value node to visit.
     * @param name
     *            The property name.
     * @param jsDoc
     *            JavaScript documentation string from a possible parent node
     *            location. Null if none.
     * @param parentScope
     *            The parent scope of the property value.
     * @return True if child nodes must be visited, false if not.
     */
    private boolean visitPropertyValue(final AstNode node,
        final String name, final String jsDoc, final Node parentScope)
    {
        parentScope.addChildNode(new Node(name, this.annoParser.parse(
            getJsDoc(node, jsDoc), node), parentScope));
        return false;
    }

    /**
     * Creates and returns the scope described by the specified node.
     *
     * @param node
     *            The scope node.
     * @param parentScope
     *            The parent scope.
     * @return The newly created scope.
     */
    private Node getScope(final AstNode node, final Node parentScope)
    {
        if (node.getType() == Token.NAME)
        {
            final String name = ((Name) node).getIdentifier();
            final Node symbol = parentScope.getChildNode(name);
            if (symbol != null) return symbol;
            final Node scope = new Node(name, new Tags(),
                parentScope);
            parentScope.addChildNode(scope);
            return scope;
        }
        else if (node.getType() == Token.GETPROP)
        {
            final PropertyGet getprop = (PropertyGet) node;
            final Node scope1 = getScope(getprop.getLeft(), parentScope);
            final Node scope2 = getScope(getprop.getRight(), scope1);
            return scope2;
        }
        else if (node.getType() == Token.THIS)
        {
            return parentScope;
        }
        else
        {
            System.out.println(node.debugPrint());
            throw new RuntimeException("No idea what to do ");
        }
    }

    /**
     * Returns the root node of the parsed tree.
     *
     * @return The root node of the parsed tree.
     */
    public RootNode getRootNode()
    {
        return this.root;
    }

    /**
     * Processes a function parameter.
     *
     * @param node
     *            The parameter node.
     * @param function
     *            The function the parameter belongs to.
     * @return The parameter documentation.
     */
    private Node processParam(final AstNode node,
        final FuncNode function)
    {
        final Name name = (Name) node;
        final Tags tags =
            this.annoParser.parse(node.getJsDoc(), node);
        return new Node(name.getIdentifier(), tags, function);
    }
}
