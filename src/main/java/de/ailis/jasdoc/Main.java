/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import java.io.File;
import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.ailis.jasdoc.doc.Configuration;
import de.ailis.jasdoc.doc.DocParser;
import de.ailis.jasdoc.doc.GlobalDoc;
import de.ailis.jasdoc.html.HtmlWriter;
import de.ailis.jasdoc.logging.LogFormatter;
import de.ailis.jasdoc.logging.LogHandler;
import de.ailis.jasdoc.tree.TreeParser;
import de.ailis.jasdoc.util.IOUtils;

/**
 * Main class.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class Main
{
    /** The logger. */
    private static final Log LOG = LogFactory.getLog(Main.class);

    /** The configuration. */
    private final Configuration config = new Configuration();

    /** Debug mode. */
    private boolean debug;

    /** Quiet mode. */
    private boolean quiet;

    /**
     * Display command line help.
     *
     * @throws IOException
     *             When help.txt could not be read.
     */
    private void help() throws IOException
    {
        System.out.println(IOUtils.readTextResource(Main.class, "help.txt",
            "UTF-8"));
        System.exit(0);
    }

    /**
     * Display version information.
     *
     * @throws IOException
     *             When version.txt could not be read.
     */
    private void version() throws IOException
    {
        System.out.println(IOUtils.readTextResource(Main.class, "version.txt",
            "UTF-8"));
        System.exit(0);
    }

    /**
     * Outputs a usage error message.
     *
     * @param message
     *            The error message.
     */
    private void wrongUsage(final String message)
    {
        System.err.println(message);
        System.err.println("Try 'jasdoc --help' for more information.");
        System.exit(2);
    }

    /**
     * Process command line options.
     *
     * @param args
     *            Command line arguments
     * @return The parameters which are left after processing the options
     * @throws IOException
     *             If an IO error occurred.
     */
    private String[] processOptions(final String[] args) throws IOException
    {
        // Setup long options
        final LongOpt[] allLongOpts = new LongOpt[]
        {
            new LongOpt("destination", LongOpt.REQUIRED_ARGUMENT, null, 'd'),
            new LongOpt("theme", LongOpt.REQUIRED_ARGUMENT, null, 't'),
            new LongOpt("title", LongOpt.REQUIRED_ARGUMENT, null, 'T'),
            new LongOpt("private", LongOpt.NO_ARGUMENT, null, 'p'),
            new LongOpt("public", LongOpt.NO_ARGUMENT, null, 'P'),
            new LongOpt("exclude", LongOpt.REQUIRED_ARGUMENT, null, 'E'),
            new LongOpt("include", LongOpt.REQUIRED_ARGUMENT, null, 'I'),
            new LongOpt("author", LongOpt.NO_ARGUMENT, null, 'A'),
            new LongOpt("quiet", LongOpt.NO_ARGUMENT, null, 'q'),
            new LongOpt("debug", LongOpt.NO_ARGUMENT, null, 'X'),
            new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h'),
            new LongOpt("version", LongOpt.NO_ARGUMENT, null, 'V')
        };

        // Build the shortOpts
        final StringBuilder shortOpts = new StringBuilder();
        for (final LongOpt longOpt: allLongOpts)
        {
            shortOpts.append((char) longOpt.getVal());
            if (longOpt.getHasArg() == LongOpt.REQUIRED_ARGUMENT)
            {
                shortOpts.append(':');
            }
        }

        // Setup short options
        final Getopt getopt = new Getopt("jasdoc", args, shortOpts.toString(),
            allLongOpts);

        // Process options
        int c;
        while ((c = getopt.getopt()) != -1)
        {
            switch (c)
            {
                case 'd':
                    this.config.setOutputDirectory(
                        new File(getopt.getOptarg()));
                    break;
                case 't':
                    this.config.setTheme(getopt.getOptarg());
                    break;
                case 'T':
                    this.config.setTitle(getopt.getOptarg());
                    break;
                case 'p':
                    this.config.setPrivateIncluded(true);
                    break;
                case 'P':
                    this.config.setPrivateIncluded(false);
                    this.config.setProtectedIncluded(false);
                    break;
                case 'E':
                    this.config.setExludes(getopt.getOptarg());
                    break;
                case 'I':
                    this.config.setIncludes(getopt.getOptarg());
                    break;
                case 'A':
                    this.config.setAuthorIncluded(true);
                    break;
                case 'X':
                    this.debug = true;
                    break;
                case 'q':
                    this.quiet = true;
                    break;
                case 'h':
                    help();
                    break;
                case 'V':
                    version();
                    break;

                default:
                    throw new RuntimeException("Unhandled option: " + c);
            }
        }

        // Generate and return parameters
        final String[] params = new String[args.length - getopt.getOptind()];
        int p = 0;
        for (int i = getopt.getOptind(); i < args.length; i++)
        {
            params[p] = args[i];
            p++;
        }
        return params;
    }

    /**
     * Initializes logging.
     */
    private void initLogging()
    {
        final Logger logger = Logger.getLogger("de.ailis.jasdoc");
        logger.setUseParentHandlers(false);
        final Handler handler = new LogHandler();
        handler.setFilter(null);
        handler.setFormatter(new LogFormatter(this.debug));
        if (this.debug)
            handler.setLevel(Level.ALL);
        else if (this.quiet)
            handler.setLevel(Level.WARNING);
        else
            handler.setLevel(Level.INFO);
        logger.setFilter(null);
        logger.addHandler(handler);
        Logger.getLogger("").setLevel(handler.getLevel());
    }

    /**
     * Runs the program.
     *
     * @param args
     *            The command line arguments.
     * @throws Exception
     *             If something went wrong.
     */
    public void run(final String[] args) throws Exception
    {
        final String[] params = processOptions(args);
        initLogging();
        if (params.length == 0) wrongUsage("No input files specified.");

        try
        {
            LOG.info("Loading source files...");
            final TreeParser treeParser = new TreeParser(this.config);
            for (final String param: params)
                treeParser.parseFile(new File(param));

            LOG.info("Parsing documentation from sources...");
            final DocParser docParser = new DocParser(this.config);
            final GlobalDoc global = docParser.parse(treeParser.getTree());

            LOG.info("Generating HTML output...");
            final HtmlWriter writer = new HtmlWriter(this.config);
            writer.writeDoc(global);
        }
        catch (final Exception e)
        {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Main method.
     *
     * @param args
     *            The command line arguments.
     * @throws Exception
     *             If something went wrong.
     */
    public static void main(final String[] args) throws Exception
    {
        new Main().run(args);
    }
}
