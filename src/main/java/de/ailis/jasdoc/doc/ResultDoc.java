/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.AbstractType;

/**
 * Function result documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ResultDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The return type. */
    private final AbstractType type;

    /** The executable this result belongs to. */
    private final AbstractExecutableDoc executable;

    /**
     * Constructor.
     *
     * @param type
     *            The return type. Must not be null.
     * @param description
     *            The result description. Must not be null. May be empty.
     * @param executable
     *            The executable this result belongs to.
     * @param tags
     *            The tags. Must not be null.
     */
    public ResultDoc(final AbstractType type, final String description,
        final AbstractExecutableDoc executable, final Tags tags)
    {
        super("", description, executable, tags);
        if (type == null)
            throw new IllegalArgumentException("type is null");
        if (executable == null)
            throw new IllegalArgumentException("executable is null");
        this.type = type;
        this.executable = executable;
    }

    /**
     * Returns the result type.
     *
     * @return The result type. Never null.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.executable.getUrl() + "=";
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return this.executable.getQualifiedName() + "=";
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.executable.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.RESULT;
    }
}
