/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Comparator;

/**
 * Comparator which compares the qualified names of qualified documentation
 * elements.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class QualifiedNameComparator implements Comparator<QualifiedDoc>
{
    /** The singleton instance. */
    private static final QualifiedNameComparator INSTANCE =
        new QualifiedNameComparator();

    /**
     * Private constructor to prevent instantiation.
     */
    private QualifiedNameComparator()
    {
        // Empty
    }

    /**
     * Returns the singleton instance of this comparator.
     *
     * @return The singleton instance.
     */
    public static QualifiedNameComparator getInstance()
    {
        return INSTANCE;
    }

    /**
     * @see Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final QualifiedDoc o1, final QualifiedDoc o2)
    {
        return o1.getQualifiedName().compareTo(o2.getQualifiedName());
    }
}
