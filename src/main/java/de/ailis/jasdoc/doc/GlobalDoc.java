/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.Tags;

/**
 * The global namespace documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class GlobalDoc extends NamespaceDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The global documentation title. */
    private final String title;

    /** The deprecated namespaces. */
    private final Set<NamespaceDoc> deprecatedNamespaces =
        new TreeSet<NamespaceDoc>(QualifiedNameComparator.getInstance());

    /** The deprecated interfaces. */
    private final Set<InterfaceDoc> deprecatedInterfaces =
        new TreeSet<InterfaceDoc>(QualifiedNameComparator.getInstance());

    /** The deprecated classes. */
    private final Set<ClassDoc> deprecatedClasses =
        new TreeSet<ClassDoc>(QualifiedNameComparator.getInstance());

    /** The deprecated enums. */
    private final Set<EnumDoc> deprecatedEnums =
        new TreeSet<EnumDoc>(QualifiedNameComparator.getInstance());

    /** The deprecated functions. */
    private final Set<FunctionDoc> deprecatedFunctions =
        new TreeSet<FunctionDoc>(QualifiedNameComparator.getInstance());

    /** The deprecated variables. */
    private final Set<VariableDoc> deprecatedVariables =
        new TreeSet<VariableDoc>(QualifiedNameComparator.getInstance());

    /** The deprecated methods. */
    private final Set<MethodDoc> deprecatedMethods = new TreeSet<MethodDoc>(
        QualifiedNameComparator.getInstance());

    /** The deprecated fields. */
    private final Set<FieldDoc> deprecatedFields = new TreeSet<FieldDoc>(
        QualifiedNameComparator.getInstance());

    /** The index. */
    private final IndexDoc index = new IndexDoc(this);

    /**
     * Constructor.
     *
     * @param title
     *            The global documentation title.
     * @param description
     *            The global documentation description.
     */
    public GlobalDoc(final String title, final String description)
    {
        super("", description, null, new Tags());
        this.title = title;
    }

    /**
     * Returns the global documentation title.
     *
     * @return The global documentation title.
     */
    public String getTitle()
    {
        return this.title;
    }

    /**
     * Adds a method to the index.
     *
     * @param method
     *            The deprecated method to add.
     */
    void indexMethod(final MethodDoc method)
    {
        if (method.isDeprecated()) this.deprecatedMethods.add(method);
        this.index.addSymbol(method);
    }

    /**
     * Returns the deprecated methods.
     *
     * @return The deprecated methods.
     */
    public Set<MethodDoc> getDeprecatedMethods()
    {
        return Collections.unmodifiableSet(this.deprecatedMethods);
    }

    /**
     * Adds an interface to the index.
     *
     * @param intf
     *            The interface to add.
     */
    void indexInterface(final InterfaceDoc intf)
    {
        if (intf.isDeprecated()) this.deprecatedInterfaces.add(intf);
        this.index.addSymbol(intf);
    }

    /**
     * Returns the deprecated interfaces.
     *
     * @return The deprecated interfaces.
     */
    public Set<InterfaceDoc> getDeprecatedInterfaces()
    {
        return Collections.unmodifiableSet(this.deprecatedInterfaces);
    }

    /**
     * Adds a function to the index.
     *
     * @param function
     *            The function to add.
     */
    void indexFunction(final FunctionDoc function)
    {
        if (function.isDeprecated()) this.deprecatedFunctions.add(function);
        this.index.addSymbol(function);
    }

    /**
     * Returns the deprecated functions.
     *
     * @return The deprecated functions.
     */
    public Set<FunctionDoc> getDeprecatedFunctions()
    {
        return Collections.unmodifiableSet(this.deprecatedFunctions);
    }

    /**
     * Adds a variable to the index.
     *
     * @param variable
     *            The variable to add.
     */
    void indexVariable(final VariableDoc variable)
    {
        if (variable.isDeprecated()) this.deprecatedVariables.add(variable);
        this.index.addSymbol(variable);
    }

    /**
     * Returns the deprecated variables.
     *
     * @return The deprecated variables.
     */
    public Set<VariableDoc> getDeprecatedVariables()
    {
        return Collections.unmodifiableSet(this.deprecatedVariables);
    }

    /**
     * Adds a class to the index.
     *
     * @param cls
     *            The class to add.
     */
    void indexClass(final ClassDoc cls)
    {
        if (cls.isDeprecated()) this.deprecatedClasses.add(cls);
        this.index.addSymbol(cls);
    }

    /**
     * Returns the deprecated classes.
     *
     * @return The deprecated classes.
     */
    public Set<ClassDoc> getDeprecatedClasses()
    {
        return Collections.unmodifiableSet(this.deprecatedClasses);
    }

    /**
     * Adds an num to the index.
     *
     * @param e
     *            The enum to add.
     */
    void indexEnum(final EnumDoc e)
    {
        if (e.isDeprecated()) this.deprecatedEnums.add(e);
        this.index.addSymbol(e);
    }

    /**
     * Returns the deprecated enums.
     *
     * @return The deprecated enums.
     */
    public Set<EnumDoc> getDeprecatedEnums()
    {
        return Collections.unmodifiableSet(this.deprecatedEnums);
    }

    /**
     * Adds a field to the index.
     *
     * @param field
     *            The field to add.
     */
    void indexField(final FieldDoc field)
    {
        if (field.isDeprecated()) this.deprecatedFields.add(field);
        this.index.addSymbol(field);
    }

    /**
     * Returns the deprecated fields.
     *
     * @return The deprecated fields.
     */
    public Set<FieldDoc> getDeprecatedFields()
    {
        return Collections.unmodifiableSet(this.deprecatedFields);
    }

    /**
     * Adds a namespace to the index.
     *
     * @param namespace
     *            The namespace to add.
     */
    void indexNamespace(final NamespaceDoc namespace)
    {
        if (namespace.isDeprecated()) this.deprecatedNamespaces.add(namespace);
        this.index.addSymbol(namespace);
    }

    /**
     * Returns the deprecated namespaces.
     *
     * @return The deprecated namespaces.
     */
    public Set<NamespaceDoc> getDeprecatedNamespaces()
    {
        return Collections.unmodifiableSet(this.deprecatedNamespaces);
    }

    /**
     * Checks if at least one deprecated symbol is present.
     *
     * @return True When a deprecated symbol is present, false if not.
     */
    public boolean isDeprecatedPresent()
    {
        return !this.deprecatedClasses.isEmpty()
            || !this.deprecatedEnums.isEmpty()
            || !this.deprecatedFields.isEmpty()
            || !this.deprecatedFunctions.isEmpty()
            || !this.deprecatedInterfaces.isEmpty()
            || !this.deprecatedMethods.isEmpty()
            || !this.deprecatedNamespaces.isEmpty()
            || !this.deprecatedVariables.isEmpty();
    }

    /**
     * Returns the index.
     *
     * @return The index.
     */
    public IndexDoc getIndex()
    {
        return this.index;
    }
}
