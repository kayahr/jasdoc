/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Index doc.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class IndexDoc implements ModelDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The index letter. */
    private final Map<Character, SymbolsDoc> index =
        new TreeMap<Character, SymbolsDoc>();

    /** The generation time. */
    private final Date generationTime = new Date();

    /** The global documentation. */
    private final GlobalDoc global;

    /**
     * Constructor.
     *
     * @param global
     *            The global documentation.
     */
    public IndexDoc(final GlobalDoc global)
    {
        this.global = global;
    }

    /**
     * Adds a symbol to this index.
     *
     * @param symbol
     *            The symbol to add.
     */
    void addSymbol(final ModelDoc symbol)
    {
        final char c = Character.toLowerCase(symbol.getName().charAt(0));
        SymbolsDoc symbols = this.index.get(c);
        if (symbols == null)
        {
            symbols = new SymbolsDoc(this, c);
            this.index.put(c, symbols);
        }
        symbols.addSymbol(symbol);
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        if (isPresent()) return this.index.values().iterator().next().getUrl();
        throw new IllegalStateException(
            "Can't return an index URL when no index is present");
    }

    /**
     * @see de.ailis.jasdoc.doc.NamedDoc#getName()
     */
    @Override
    public String getName()
    {
        if (isPresent())
            return this.index.values().iterator().next().getName();
        throw new IllegalStateException(
            "Can't return an index URL when no index is present");
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return getName();
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final ModelDoc o)
    {
        return this.getName().compareTo(o.getName());
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return "../";
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getGlobal()
     */
    @Override
    public GlobalDoc getGlobal()
    {
        return this.global;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getGenerationTime()
     */
    @Override
    public Date getGenerationTime()
    {
        return this.generationTime;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getThis()
     */
    @Override
    public ModelDoc getThis()
    {
        return this;
    }

    /**
     * Returns the symbols which start with the specified letter.
     *
     * @param letter
     *            The index letter.
     * @return The symbols or null if there are no symbols starting with the
     *         specified letter.
     */
    public SymbolsDoc getSymbols(final char letter)
    {
        return this.index.get(letter);
    }

    /**
     * Returns the symbols in the index.
     *
     * @return The symbols in the index.
     */
    public Collection<SymbolsDoc> getSymbols()
    {
        return Collections.unmodifiableCollection(this.index.values());
    }

    /**
     * Returns the index letters.
     *
     * @return The index letters.
     */
    public Set<Character> getLetters()
    {
        return Collections.unmodifiableSet(this.index.keySet());
    }

    /**
     * Checks if the index is present.
     *
     * @return True if index is present, false if not.
     */
    public boolean isPresent()
    {
        return !this.index.isEmpty();
    }
}
