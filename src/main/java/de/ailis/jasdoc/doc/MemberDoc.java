/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */
package de.ailis.jasdoc.doc;

/**
 * Interface for documentation elements which represent type members.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface MemberDoc extends ModelDoc
{
    /**
     * Returns the method modifier flags.
     *
     * @return The method modifier flags. Never null.
     */
    Modifier getModifier();
}
