/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.ailis.jasdoc.tags.AuthorTag;
import de.ailis.jasdoc.tags.DeprecatedTag;
import de.ailis.jasdoc.tags.SeeTag;
import de.ailis.jasdoc.tags.SinceTag;
import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.tags.VersionTag;
import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Base class for all documentation element classes.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractElementDoc extends AbstractDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** Pattern to match an inline link tag. */
    private static final Pattern LINK_TAG_PATTERN = Pattern
        .compile("\\{@(link|linkplain)\\s+([^\\s}]+)\\s*([^}]*)\\s*\\}");

    /** The name. */
    private final String name;

    /** The short description as plain text. */
    private final String shortDescription;

    /** The detailed description as HTML. */
    private final String description;

    /** The generation time. */
    private final Date generationTime = new Date();

    /** The parent documentation model. */
    private final ModelDoc parentDoc;

    /** The tags. */
    private final Tags tags;

    /**
     * Constructor.
     *
     * @param name
     *            The name. Must not be null but may be empty for name-less
     *            elements like return values.
     * @param description
     *            The description as HTML. Must not be null.
     * @param parentDoc
     *            The parent documentation model. Null if none (Only valid for
     *            global doc)
     * @param tags
     *            The tags.
     */
    public AbstractElementDoc(final String name, final String description,
        final ModelDoc parentDoc, final Tags tags)
    {
        if (name == null)
            throw new IllegalArgumentException("name is null");
        if (description == null)
            throw new IllegalArgumentException("description is null");
        this.parentDoc = parentDoc;
        this.name = name;
        this.description = description;
        this.shortDescription =
            HTMLUtils.simplify(HTMLUtils.getFirstSentence(description));
        this.tags = tags;
    }

    /**
     * Returns the detailed HTML description.
     *
     * @return The detailed HTML description. Never null. May be empty.
     */
    public final String getDescription()
    {
        return resolveInlineTags(this.description);
    }

    /**
     * Resolves inline tags in the description.
     *
     * @param description
     *            The description to resolve.
     * @return The resolved description.
     */
    private String resolveInlineTags(final String description)
    {
        final GlobalDoc global = getGlobal();
        final String baseUrl = getBaseUrl();
        final StringBuilder builder = new StringBuilder();
        int start = 0;
        final Matcher linkTagMatcher = LINK_TAG_PATTERN.matcher(description);
        while (linkTagMatcher.find())
        {
            builder
                .append(description.substring(start, linkTagMatcher.start()));
            final String linkType = linkTagMatcher.group(1);
            final String typeName = linkTagMatcher.group(2);
            final String title = linkTagMatcher.group(3);
            final ModelDoc doc = global.resolve(typeName);
            if (doc != null)
            {
                builder.append("<a href=\"");
                builder.append(baseUrl);
                builder.append(HTMLUtils.escape(doc.getUrl()));
                builder.append('"');
                if ("link".equals(linkType))
                    builder.append(" class=\"code\"");
                builder.append(">");
            }
            if (title.isEmpty())
                builder.append(typeName);
            else
                builder.append(title);
            if (doc != null)
                builder.append("</a>");
            start = linkTagMatcher.end();
        }
        builder.append(description.substring(start));
        return builder.toString();
    }

    /**
     * Returns the short description.
     *
     * @return The short description. Never null. May be empty.
     */
    public final String getShortDescription()
    {
        return resolveInlineTags(this.shortDescription);
    }

    /**
     * Returns the simple unqualified name.
     *
     * @return The simple unqualified name. Never null.
     */
    @Override
    public final String getName()
    {
        return this.name;
    }

    /**
     * @see Comparable#compareTo(java.lang.Object)
     */
    @Override
    public final int compareTo(final ModelDoc other)
    {
        int c = getName().toLowerCase().compareTo(
            other.getName().toLowerCase());
        if (c == 0) c = getName().compareTo(other.getName());
        if (c == 0) c = getQualifiedName().compareTo(other.getQualifiedName());
        return c;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public final String toString()
    {
        return getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getGenerationTime()
     */
    @Override
    public final Date getGenerationTime()
    {
        return this.generationTime;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getThis()
     */
    @Override
    public final ModelDoc getThis()
    {
        return this;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getGlobal()
     */
    @Override
    public final GlobalDoc getGlobal()
    {
        if (this instanceof GlobalDoc) return (GlobalDoc) this;
        return this.parentDoc.getGlobal();
    }

    /**
     * Returns the tags for this model.
     *
     * @return The tags.
     */
    public final Tags getTags()
    {
        return this.tags;
    }

    /**
     * Returns the authors.
     *
     * @return The authors. Never null. May be empty.
     */
    public final List<String> getAuthors()
    {
        final AuthorTag tag = this.tags.getTag(AuthorTag.class);
        if (tag == null) return Collections.EMPTY_LIST;
        return tag.getAuthors();
    }

    /**
     * Returns the since texts.
     *
     * @return The since texts. Never null. May be empty.
     */
    public final List<String> getSince()
    {
        final SinceTag tag = this.tags.getTag(SinceTag.class);
        if (tag == null) return Collections.EMPTY_LIST;
        return tag.getSince();
    }

    /**
     * Returns the versions.
     *
     * @return The versions. Never null. May be empty.
     */
    public final List<String> getVersions()
    {
        final VersionTag tag = this.tags.getTag(VersionTag.class);
        if (tag == null) return Collections.EMPTY_LIST;
        return tag.getVersions();
    }

    /**
     * Returns the references.
     *
     * @return The references. Never null. May be empty.
     */
    public final List<String> getReferences()
    {
        final SeeTag tag = this.tags.getTag(SeeTag.class);
        if (tag == null) return Collections.EMPTY_LIST;
        List<String> references = new ArrayList<String>();
        GlobalDoc global = getGlobal();
        for (String reference: tag.getReferences())
        {
            String[] parts = reference.split("\\s+", 2);
            String ref = parts[0];
            String label = parts.length == 2 ? parts[1] : ref;
            ModelDoc doc = global.resolve(ref);
            if (doc != null)
            {
                references.add("<a href=\"" + getBaseUrl() + doc.getUrl()
                    + "\">" + label + "</a>");
            }
            else
                references.add(reference);
        }
        return references;
    }
    
    /**
     * Checks if the documented element is deprecated.
     *
     * @return True if documented element is deprecated, false if not.
     */
    public final boolean isDeprecated()
    {
        return this.tags.isTagPresent(DeprecatedTag.class);
    }

    /**
     * Returns the detailed deprecation description.
     *
     * @return The detailed deprecation description. Empty if not deprecated.
     */
    public final String getDeprecation()
    {
        final DeprecatedTag tag = this.tags.getTag(DeprecatedTag.class);
        if (tag == null) return "";
        return resolveInlineTags(tag.getDescription());
    }

    /**
     * Returns the short deprecation description.
     *
     * @return The short deprecation description. Empty if not deprecated.
     */
    public final String getShortDeprecation()
    {
        final DeprecatedTag tag = this.tags.getTag(DeprecatedTag.class);
        if (tag == null) return "";
        return resolveInlineTags(tag.getShortDescription());
    }

    /**
     * Returns the element type.
     *
     * @return The element type.
     */
    public abstract ElementType getElementType();
}
