/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

/**
 * Interface for documentation elements which is linkable.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface LinkableDoc extends NamedDoc
{
    /**
     * Returns the link to this document element. The link is relative to the
     * documentation root directory.
     *
     * @return The link to this document element.
     */
    String getUrl();
}
