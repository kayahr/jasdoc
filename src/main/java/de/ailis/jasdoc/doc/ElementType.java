/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */
package de.ailis.jasdoc.doc;

/**
 * Element type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public enum ElementType
{
    /** Interface type. */
    INTERFACE,

    /** Class type. */
    CLASS,

    /** Namespace type. */
    NAMESPACE,

    /** Method type. */
    METHOD,

    /** Function type. */
    FUNCTION,

    /** Variable type. */
    VARIABLE,

    /** Enum type. */
    ENUM,

    /** Constructor type. */
    CONSTRUCTOR,

    /** Enum constant type. */
    ENUM_CONSTANT,

    /** Field type. */
    FIELD,

    /** Parameter type. */
    PARAMETER,

    /** Result type. */
    RESULT
}
