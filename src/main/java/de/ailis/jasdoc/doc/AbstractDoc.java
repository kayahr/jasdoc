/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

/**
 * Base class for all documentation element classes.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractDoc implements ModelDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;
}
