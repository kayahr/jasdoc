/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;


/**
 * Constructor documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ConstructorDoc extends AbstractExecutableDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The constructor modifier flags. */
    private final Modifier modifier;

    /** The class this constructor belongs to. */
    private final ClassDoc cls;

    /**
     * @param name
     *            The constructor name.
     * @param description
     *            The constructor description.
     * @param modifier
     *            The constructor modifier flags. Must not be null.
     * @param cls
     *            The class this constructor belongs to.
     * @param tags
     *            The tags. Must not be null.
     */
    public ConstructorDoc(final String name, final String description,
        final Modifier modifier, final ClassDoc cls, final Tags tags)
    {
        super(name, description, cls, tags);
        if (modifier == null)
            throw new IllegalArgumentException("modifier is null");
        if (cls == null)
            throw new IllegalArgumentException("cls is null");
        this.modifier = modifier;
        this.cls = cls;
    }

    /**
     * Returns the constructor modifier flags.
     *
     * @return The constructor modifier flags. Never null.
     */
    public Modifier getModifier()
    {
        return this.modifier;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.cls.getUrl() + "#constructor";
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return this.cls.getQualifiedName() + "#constructor";
    }


    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.cls.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.CONSTRUCTOR;
    }
}

