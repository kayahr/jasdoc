/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */
package de.ailis.jasdoc.doc;

/**
 * Interface for documentation elements which provide a qualified name.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface QualifiedDoc
{
    /**
     * Returns the qualified name.
     *
     * @return The qualified name.
     */
    String getQualifiedName();
}
