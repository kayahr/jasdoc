/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */
package de.ailis.jasdoc.doc;

/**
 * Interface for documentation elements which provide a simple name.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface NamedDoc
{
    /**
     * Returns the simple name.
     *
     * @return The simple name.
     */
    String getName();
}
