/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.Tags;

/**
 * Interface documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class InterfaceDoc extends AbstractTypeDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The super interfaces of this interface. */
    private final Set<InterfaceDoc> superInterfaces;

    /** The implementing classes of this interface. */
    private final Set<ClassDoc> implementingClasses = new TreeSet<ClassDoc>();

    /** Read-only access to the implementing classes. */
    private final Set<ClassDoc> immutableImplementingClasses;

    /** The sub interfaces of this interface. */
    private final Set<InterfaceDoc> subInterfaces = new TreeSet<InterfaceDoc>();

    /** Read-only access to the sub interfaces. */
    private final Set<InterfaceDoc> immutableSubInterfaces;

    /**
     * @param name
     *            The interface name. Must not be null.
     * @param description
     *            The interface description. Must not be null.
     * @param namespace
     *            The namespace this interface belongs to. Must not be null.
     * @param superInterfaces
     *            The super interfaces.
     * @param tags
     *            The tags. Must not be null.
     */
    public InterfaceDoc(final String name, final String description,
        final NamespaceDoc namespace,
        final Collection<InterfaceDoc> superInterfaces, final Tags tags)
    {
        super(name, description, namespace, tags);
        this.superInterfaces =
            Collections.unmodifiableSet(new TreeSet<InterfaceDoc>(
                superInterfaces));
        this.immutableImplementingClasses =
            Collections.unmodifiableSet(this.implementingClasses);
        this.immutableSubInterfaces =
            Collections.unmodifiableSet(this.subInterfaces);
    }

    /**
     * Returns the super interfaces of this interface.
     *
     * @return The super interfaces. Never null. May be empty.
     */
    public Set<InterfaceDoc> getSuperInterfaces()
    {
        return this.superInterfaces;
    }

    /**
     * Adds an implementing class.
     *
     * @param cls
     *            The implementing class to add.
     */
    void addImplementingClass(final ClassDoc cls)
    {
        this.implementingClasses.add(cls);
    }

    /**
     * Returns the implementing classes of this interface.
     *
     * @return The implementing classes of this interface.
     */
    public Set<ClassDoc> getImplementingClasses()
    {
        return this.immutableImplementingClasses;
    }

    /**
     * Adds a sub interface.
     *
     * @param intf
     *            The sub interface to add.
     */
    void addSubInterface(final InterfaceDoc intf)
    {
        this.subInterfaces.add(intf);
    }

    /**
     * Returns the sub interfaces of this interface.
     *
     * @return The sub interfaces of this interface.
     */
    public Set<InterfaceDoc> getSubInterfaces()
    {
        return this.immutableSubInterfaces;
    }

    /**
     * Returns a list of all super interfaces.
     *
     * @return the list of all super interfaces.
     */
    public List<InterfaceDoc> getAllSuperInterfaces()
    {
        final List<InterfaceDoc> result = new ArrayList<InterfaceDoc>();
        for (final InterfaceDoc superInterface : this.superInterfaces)
        {
            result.add(superInterface);
        }
        for (final InterfaceDoc superClass : this.superInterfaces)
        {
            result.addAll(superClass.getAllSuperInterfaces());
        }
        return result;
    }

    /**
     * Return a list of super classes with additional methods they provide.
     *
     * @return The list of super classes and the additional methods they
     *         provide.
     */
    public List<Map.Entry<InterfaceDoc, Set<MethodDoc>>> getInheritedMethods()
    {
        final List<Map.Entry<InterfaceDoc, Set<MethodDoc>>> result =
            new ArrayList<Map.Entry<InterfaceDoc, Set<MethodDoc>>>();

        // Initialize the list of already seen methods
        final Set<String> seenMethods = new HashSet<String>();
        for (final MethodDoc method : this.getMethods())
            seenMethods.add(method.getName());

        for (final InterfaceDoc superInterface : getAllSuperInterfaces())
        {
            final Set<MethodDoc> methods = new TreeSet<MethodDoc>();
            for (final MethodDoc method : superInterface.getMethods())
            {
                if (seenMethods.contains(method.getName())) continue;
                methods.add(method);
                seenMethods.add(method.getName());
            }
            if (methods.isEmpty()) continue;
            result.add(
                new AbstractMap.SimpleEntry<InterfaceDoc, Set<MethodDoc>>(
                    superInterface, methods));
        }

        return result;
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractTypeDoc#getSuperTypes()
     */
    @Override
    Set<? extends AbstractTypeDoc> getSuperTypes()
    {
        return this.superInterfaces;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.getNamespace().getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.INTERFACE;
    }
}
