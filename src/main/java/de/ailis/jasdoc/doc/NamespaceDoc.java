/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.Tags;

/**
 * Namespace documentation.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */
public class NamespaceDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The parent namespace. Null if this is the global namespace. */
    private final NamespaceDoc superNamespace;

    /** The classes in this namespace. */
    private final Set<ClassDoc> classes = new TreeSet<ClassDoc>();

    /** The immutable view on the set of classes in this namespace. */
    private final Set<ClassDoc> immutableClasses;

    /** The interfaces in this namespace. */
    private final Set<InterfaceDoc> interfaces = new TreeSet<InterfaceDoc>();

    /** The immutable view on the set of interfaces in this namespace. */
    private final Set<InterfaceDoc> immutableInterfaces;

    /** The functions in this namespace. */
    private final Set<FunctionDoc> functions = new TreeSet<FunctionDoc>();

    /** The immutable view on the set of functions in this namespace. */
    private final Set<FunctionDoc> immutableFunctions;

    /** The variables in this namespace. */
    private final Set<VariableDoc> variables = new TreeSet<VariableDoc>();

    /** The immutable view on the set of variables in this namespace. */
    private final Set<VariableDoc> immutableVariables;

    /** The enums in this namespace. */
    private final Set<EnumDoc> enums = new TreeSet<EnumDoc>();

    /** The immutable view on the set of enums in this namespace. */
    private final Set<EnumDoc> immutableEnums;

    /** The sub namespaces in this namespace. */
    private final Set<NamespaceDoc> namespaces = new TreeSet<NamespaceDoc>();

    /** The immutable view on the set of sub namespaces in this namespace. */
    private final Set<NamespaceDoc> immutableNamespaces;

    /** Indexed child elements. */
    private final Map<String, AbstractElementDoc> elements =
        new HashMap<String, AbstractElementDoc>();

    /**
     * @param name
     *            The simple unqualified namespace name.
     * @param description
     *            The namespace description.
     * @param superNamespace
     *            The super namespace or null if global namespace.
     * @param tags
     *            The tags. Must not be null.
     */
    public NamespaceDoc(final String name, final String description,
        final NamespaceDoc superNamespace, final Tags tags)
    {
        super(name, description, superNamespace, tags);
        this.superNamespace = superNamespace;
        this.immutableClasses = Collections.unmodifiableSet(this.classes);
        this.immutableInterfaces = Collections.unmodifiableSet(this.interfaces);
        this.immutableFunctions = Collections.unmodifiableSet(this.functions);
        this.immutableVariables = Collections.unmodifiableSet(this.variables);
        this.immutableEnums = Collections.unmodifiableSet(this.enums);
        this.immutableNamespaces = Collections.unmodifiableSet(this.namespaces);
    }

    /**
     * Adds the specified class to this namespace.
     * 
     * @param cls
     *            The class to add.
     */
    final void addClass(final ClassDoc cls)
    {
        this.classes.add(cls);
        this.elements.put(cls.getName(), cls);

        for (final ClassDoc superClass : cls.getSuperClasses())
            superClass.addSubClass(cls);

        for (final InterfaceDoc intf : cls.getInterfaces())
            intf.addImplementingClass(cls);

        getGlobal().indexClass(cls);
    }

    /**
     * Adds the specified interface to this namespace.
     * 
     * @param intf
     *            The interface to add.
     */
    final void addInterface(final InterfaceDoc intf)
    {
        this.interfaces.add(intf);
        this.elements.put(intf.getName(), intf);

        for (final InterfaceDoc superInterface : intf.getSuperInterfaces())
            superInterface.addSubInterface(intf);

        getGlobal().indexInterface(intf);
    }

    /**
     * Adds the specified function to this namespace.
     * 
     * @param function
     *            The function to add.
     */
    final void addFunction(final FunctionDoc function)
    {
        this.functions.add(function);
        this.elements.put(function.getName(), function);
        getGlobal().indexFunction(function);
    }

    /**
     * Adds the specified variable to this namespace.
     * 
     * @param variable
     *            The variable to add.
     */
    final void addVariable(final VariableDoc variable)
    {
        this.variables.add(variable);
        this.elements.put(variable.getName(), variable);
        getGlobal().indexVariable(variable);
    }

    /**
     * Adds the specified sub namespace to this namespace.
     * 
     * @param namespace
     *            The sub namespace to add.
     */
    final void addNamespace(final NamespaceDoc namespace)
    {
        this.namespaces.add(namespace);
        this.elements.put(namespace.getName(), namespace);
        getGlobal().indexNamespace(namespace);
    }

    /**
     * Adds the specified enum to this namespace.
     * 
     * @param e
     *            The enum to add.
     */
    final void addEnum(final EnumDoc e)
    {
        this.enums.add(e);
        this.elements.put(e.getName(), e);
        getGlobal().indexEnum(e);
    }

    /**
     * Returns the super namespace or null if this is the global namespace.
     * 
     * @return The super namespace or null if global namespace.
     */
    public final NamespaceDoc getSuperNamespace()
    {
        return this.superNamespace;
    }

    /**
     * Returns the classes in this namespace.
     * 
     * @return The classes in this namespace. Never null. May be empty.
     */
    public final Set<ClassDoc> getClasses()
    {
        return this.immutableClasses;
    }

    /**
     * Returns the interfaces in this namespace.
     * 
     * @return The interfaces in this namespace. Never null. May be empty.
     */
    public final Set<InterfaceDoc> getInterfaces()
    {
        return this.immutableInterfaces;
    }

    /**
     * Returns the functions in this namespace.
     * 
     * @return The functions in this namespace. Never null. May be empty.
     */
    public final Set<FunctionDoc> getFunctions()
    {
        return this.immutableFunctions;
    }

    /**
     * Returns the variables in this namespace.
     * 
     * @return The variables in this namespace. Never null. May be empty.
     */
    public final Set<VariableDoc> getVariables()
    {
        return this.immutableVariables;
    }

    /**
     * Returns the enums in this namespace.
     * 
     * @return The enums in this namespace. Never null. May be empty.
     */
    public final Set<EnumDoc> getEnums()
    {
        return this.immutableEnums;
    }

    /**
     * Returns the sub namespaces in this namespace.
     * 
     * @return The sub namespaces in this namespace. Never null. May be empty.
     */
    public final Set<NamespaceDoc> getNamespaces()
    {
        return this.immutableNamespaces;
    }

    /**
     * Returns all sub namespaces of this namespaces and recursibely of all its
     * sub namespaces.
     * 
     * @return All sub namespaces with infinit depth. Never null. May be empty.
     */
    public final Set<NamespaceDoc> getAllNamespaces()
    {
        final Set<NamespaceDoc> namespaces =
            new TreeSet<NamespaceDoc>(QualifiedNameComparator.getInstance());
        namespaces.addAll(this.namespaces);
        for (final NamespaceDoc namespace : this.namespaces)
            namespaces.addAll(namespace.getAllNamespaces());
        return namespaces;
    }

    /**
     * Recursively resolves a type.
     * 
     * @param name
     *            The type name.
     * @return The resolved type or null if not found.
     */
    public final ModelDoc resolve(final String name)
    {
        String[] parts;
        boolean member;
        if (name.indexOf('.') >= 0)
        {
            parts = name.split("\\.", 2);
            member = false;
        }
        else
        {
            parts = name.split("#", 2);
            member = true;
        }
        final AbstractElementDoc element = this.elements.get(parts[0]);
        if (parts.length > 1)
        {
            if (element instanceof NamespaceDoc)
                return ((NamespaceDoc) element).resolve(parts[1]);
            else if (element instanceof AbstractTypeDoc)
            {
                if (member)
                    return ((AbstractTypeDoc) element).getMember(parts[1]);
                else
                    return ((AbstractTypeDoc) element)
                        .getStaticMember(parts[1]);
            }
        }
        return element;
    }

    /**
     * Returns the namespace path relative to the root. The root namespace just
     * returns an empty string, all other namespaces return their qualified name
     * with dots replaced with slashes and with a trailing additional slash.
     * 
     * @return The namespace path.
     */
    public final String getPath()
    {
        final String path = getQualifiedName().replace('.', '/');
        if (path.isEmpty()) return path;
        return path + "/";
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public final String getUrl()
    {
        return getPath() + "index.html";
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public final String getQualifiedName()
    {
        if (this.superNamespace == null) return getName();
        final String parentName = this.superNamespace.getQualifiedName();
        if (parentName.isEmpty()) return getName();
        return parentName + "." + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public final String getBaseUrl()
    {
        if (this.superNamespace == null) return "";
        return this.superNamespace.getBaseUrl() + "../";
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public final ElementType getElementType()
    {
        return ElementType.NAMESPACE;
    }
}
