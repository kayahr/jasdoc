/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.AbstractType;

/**
 * Field documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class FieldDoc extends AbstractElementDoc implements MemberDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The field type. */
    private final AbstractType type;

    /** The constructor modifier flags. */
    private final Modifier modifier;

    /** The class this field belongs to. */
    private final ClassDoc cls;

    /**
     * @param type
     *            The field type. Must not be null.
     * @param name
     *            The field name. Must not be null.
     * @param description
     *            The field description. Must not be null.
     * @param modifier
     *            The field modifier flags. Must not be null.
     * @param cls
     *            The class this field belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public FieldDoc(final AbstractType type, final String name,
        final String description, final Modifier modifier, final ClassDoc cls,
        final Tags tags)
    {
        super(name, description, cls, tags);
        if (type == null) throw new IllegalArgumentException("type is null");
        if (modifier == null)
            throw new IllegalArgumentException("modifier is null");
        if (cls == null)
            throw new IllegalArgumentException("cls is null");
        this.type = type;
        this.modifier = modifier;
        this.cls = cls;
    }

    /**
     * Returns the field type.
     *
     * @return The field type.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * Returns the field modifier flags.
     *
     * @return The field modifier flags. Never null.
     */
    @Override
    public Modifier getModifier()
    {
        return this.modifier;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.cls.getUrl() + "#" + getId();
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return this.cls.getQualifiedName() + "#" + getId();
    }

    /**
     * Returns the field ID. THis is the name for normal member fields and the
     * name with a prepended "-" character for static fields. This ID is used in
     * link anchors.
     *
     * @return The field ID.
     */
    public String getId()
    {
        if (this.modifier.isStatic())
            return "-" + getName();
        else
            return getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.cls.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.FIELD;
    }

    /**
     * The type where this field is declared in.
     *
     * @return The declaring type.
     */
    public ClassDoc getDeclaringType()
    {
        return this.cls;
    }
}
