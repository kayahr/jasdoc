/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.AbstractType;

/**
 * Enum documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class EnumConstantDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The enum type. */
    private final AbstractType type;

    /** The modifier flags. */
    private final Modifier modifier;

    /** The enum this constant belongs to. */
    private final EnumDoc enm;

    /**
     * @param type
     *            The enum type. Must not be null.
     * @param name
     *            The enum constant name. Must not be null.
     * @param description
     *            The enum constant description. Must not be null.
     * @param modifier
     *            The enum modifier flags. Must not be null.
     * @param enm
     *            The enum this constant belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public EnumConstantDoc(final AbstractType type, final String name,
        final String description, final Modifier modifier, final EnumDoc enm,
        final Tags tags)
    {
        super(name, description, enm, tags);
        if (type == null) throw new IllegalArgumentException("type is null");
        if (modifier == null)
            throw new IllegalArgumentException("modifier is null");
        if (enm == null) throw new IllegalArgumentException("enm is null");
        this.type = type;
        this.modifier = modifier;
        this.enm = enm;
    }

    /**
     * Returns the enum type.
     *
     * @return The enum type.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * Returns the enum modifier.
     *
     * @return The enum modifier.
     */
    public Modifier getModifier()
    {
        return this.modifier;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.enm.getUrl() + "#" + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return this.enm.getQualifiedName() + "#" + getName();
    }


    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.enm.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.ENUM_CONSTANT;
    }
}
