/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.AbstractType;

/**
 * Variable documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class VariableDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The variable type. */
    private final AbstractType type;

    /** The namespace this variable belongs to. */
    private final NamespaceDoc namespace;

    /** The variable modifier flags. */
    private final Modifier modifier;

    /**
     * @param type
     *            The variable type. Must not be null.
     * @param name
     *            The variable name. Must not be null.
     * @param description
     *            The variable description. Must not be null.
     * @param modifier
     *            The variable modifier flags. Must not be null.
     * @param namespace
     *            The namespace this variable belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public VariableDoc(final AbstractType type, final String name,
        final String description, final Modifier modifier,
        final NamespaceDoc namespace, final Tags tags)
    {
        super(name, description, namespace, tags);
        if (type == null) throw new IllegalArgumentException("type is null");
        if (modifier == null)
            throw new IllegalArgumentException("modifier is null");
        if (namespace == null)
            throw new IllegalArgumentException("namespace is null");
        this.type = type;
        this.namespace = namespace;
        this.modifier = modifier;
    }

    /**
     * Returns the variable type.
     *
     * @return The variable type.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * Returns the namespace this variable belongs to.
     *
     * @return The namespace this variable belongs to. Never null.
     */
    public NamespaceDoc getNamespace()
    {
        return this.namespace;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.namespace.getUrl() + "#" + getName();
    }

    /**
     * Returns the variable modifier flags.
     *
     * @return The modifier flags.
     */
    public Modifier getModifier()
    {
        return this.modifier;
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        final String namespace = this.namespace.getQualifiedName();
        if (namespace.isEmpty()) return getName();
        return namespace + "." + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.namespace.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.VARIABLE;
    }
}
