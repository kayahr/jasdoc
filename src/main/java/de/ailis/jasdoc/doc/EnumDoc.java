/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.AbstractType;

/**
 * Enum documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class EnumDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The enum type. */
    private final AbstractType type;

    /** The enum constants. */
    private Set<EnumConstantDoc> constants;

    /** The namespace this enum belongs to. */
    private final NamespaceDoc namespace;

    /**
     * @param name
     *            The enum name. Must not be null.
     * @param description
     *            The enum description. Must not be null.
     * @param namespace
     *            The namespace this enum belongs to. Must not be null.
     * @param type
     *            The enum type. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public EnumDoc(final String name, final String description,
        final NamespaceDoc namespace, final AbstractType type, final Tags tags)
    {
        super(name, description, namespace, tags);
        if (type == null) throw new IllegalArgumentException("type is null");
        if (namespace == null)
            throw new IllegalArgumentException("namespace is null");
        this.namespace = namespace;
        this.type = type;
    }

    /**
     * Sets the constants.
     *
     * @param constants
     *            The enum constants. Must not be null. May be empty.
     */
    void setConstants(final Collection<EnumConstantDoc> constants)
    {
        if (constants == null)
            throw new IllegalArgumentException("constants is null");
        this.constants = Collections.unmodifiableSet(
            new TreeSet<EnumConstantDoc>(constants));
    }

    /**
     * Returns the enum type.
     *
     * @return The enum type. Never null. May be empty.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * Returns the enum constants.
     *
     * @return The enum constants. Never null. May be empty.
     */
    public Set<EnumConstantDoc> getConstants()
    {
        return this.constants;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return getNamespace().getPath() + getName() + ".html";
    }

    /**
     * Return the namespace this enum belongs to.
     *
     * @return The namespace this enum belongs to
     */
    public NamespaceDoc getNamespace()
    {
        return this.namespace;
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        final String namespace = this.namespace.getQualifiedName();
        if (namespace.isEmpty()) return getName();
        return namespace + "." + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.namespace.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.ENUM;
    }
}
