/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

/**
 * Modifier flags.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class Modifier
{
    /** Constant for public modifier flag. */
    public static final int PUBLIC = 1;

    /** Constant for private modifier flag. */
    public static final int PRIVATE = 2;

    /** Constant for protected modifier flag. */
    public static final int PROTECTED = 4;

    /** Constant for static modifier flag. */
    public static final int STATIC = 8;

    /** Constant for final modifier flag. */
    public static final int FINAL = 16;

    /** Constant for const modifier flag. */
    public static final int CONST = 0x40000000;

    /** The modifier flags. */
    private final int flags;

    /**
     * Constructor.
     *
     * @param flags
     *            The modifier flags.
     */
    public Modifier(final int flags)
    {
        this.flags = flags;
    }

    /**
     * Constructor.
     *
     * @param isPrivate
     *            Private modifier flag.
     * @param isProtected
     *            Protected modifier flag.
     * @param isStatic
     *            Static modifier flag.
     * @param isFinal
     *            Final modifier flag.
     * @param isConst
     *            Const modifier flag.
     */
    public Modifier(final boolean isPrivate,
        final boolean isProtected, final boolean isStatic,
        final boolean isFinal, final boolean isConst)
    {
        this(((!isPrivate && !isProtected) ? PUBLIC : 0)
            | (isPrivate ? PRIVATE : 0)
            | (isProtected ? PROTECTED : 0)
            | (isStatic ? STATIC : 0)
            | (isFinal ? FINAL : 0)
            | (isConst ? CONST : 0));
    }

    /**
     * Checks if public flag is set.
     *
     * @return True if public, false if not.
     */
    public boolean isPublic()
    {
        return (this.flags & PUBLIC) != 0;
    }

    /**
     * Checks if private flag is set.
     *
     * @return True if private, false if not.
     */
    public boolean isPrivate()
    {
        return (this.flags & PRIVATE) != 0;
    }

    /**
     * Checks if protected flag is set.
     *
     * @return True if protected, false if not.
     */
    public boolean isProtected()
    {
        return (this.flags & PROTECTED) != 0;
    }

    /**
     * Checks if static flag is set.
     *
     * @return True if static, false if not.
     */
    public boolean isStatic()
    {
        return (this.flags & STATIC) != 0;
    }

    /**
     * Checks if final flag is set.
     *
     * @return True if final, false if not.
     */
    public boolean isFinal()
    {
        return (this.flags & FINAL) != 0;
    }

    /**
     * Checks if const flag is set.
     *
     * @return True if const, false if not.
     */
    public boolean isConst()
    {
        return (this.flags & CONST) != 0;
    }
}
