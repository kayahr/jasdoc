/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */
package de.ailis.jasdoc.doc;

import java.io.Serializable;
import java.util.Date;

/**
 * Interface for documentation elements which are used as a model for
 * rendering a view.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public interface ModelDoc extends Serializable, LinkableDoc, QualifiedDoc,
    Comparable<ModelDoc>
{
    /**
     * Returns the base URL to reach the documentation root from this
     * element. The global namespace returns an empty string here. All other
     * elements return their path relative to the root with a trailing
     * slash.
     *
     * @return The base URL. Never null. Empty for global namespace.
     */
    String getBaseUrl();

    /**
     * Returns the global namespace.
     *
     * @return The global namespace.
     */
    GlobalDoc getGlobal();

    /**
     * Returns the generation time.
     *
     * @return The generation time.
     */
    Date getGenerationTime();

    /**
     * Returns the reference to this model itself.
     *
     * @return This model.
     */
    ModelDoc getThis();
}
