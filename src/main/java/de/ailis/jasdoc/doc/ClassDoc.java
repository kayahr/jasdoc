/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.Tags;

/**
 * Class documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ClassDoc extends AbstractTypeDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The constructor or null if none. */
    private ConstructorDoc constructor;

    /** The fields in this class. */
    private Set<FieldDoc> fields;

    /** The super classes of this class. */
    private final Set<ClassDoc> superClasses;

    /** The implemented interfaces. */
    private final Set<InterfaceDoc> interfaces;

    /** The sub classes of this class. */
    private final Set<ClassDoc> subClasses = new TreeSet<ClassDoc>();

    /** Read-only access to the sub classes. */
    private final Set<ClassDoc> immutableSubClasses;

    /**
     * @param name
     *            The class name. Must not be null.
     * @param description
     *            The class description. Must not be null.
     * @param namespace
     *            The namespace this class belongs to. Must not be null.
     * @param superClasses
     *            The super class. Must not be null. May be empty.
     * @param interfaces
     *            The implemented interfaces. Must not be null. May be empty.
     * @param tags
     *            The tags. Must not be null.
     */
    public ClassDoc(final String name, final String description,
        final NamespaceDoc namespace, final Collection<ClassDoc> superClasses,
        final Collection<InterfaceDoc> interfaces, final Tags tags)
    {
        super(name, description, namespace, tags);
        this.superClasses =
            Collections.unmodifiableSet(new TreeSet<ClassDoc>(superClasses));
        this.interfaces =
            Collections.unmodifiableSet(new TreeSet<InterfaceDoc>(interfaces));
        this.immutableSubClasses = Collections.unmodifiableSet(this.subClasses);
    }

    /**
     * Sets the constructor.
     *
     * @param constructor
     *            The constructor of this class. Null if none.
     */
    void setConstructor(final ConstructorDoc constructor)
    {
        this.constructor = constructor;
    }

    /**
     * Sets the fields.
     *
     * @param fields
     *            The fields of this class. Must not be null. May be empty.
     */
    void setFields(final Collection<FieldDoc> fields)
    {
        if (fields == null)
            throw new IllegalArgumentException("fields is null");
        this.fields =
            Collections.unmodifiableSet(new TreeSet<FieldDoc>(fields));
        final GlobalDoc global = getGlobal();
        for (final FieldDoc field: fields)
        {
            if (field.getModifier().isStatic())
                addStaticMember(field);
            else
                addMember(field);

            global.indexField(field);
        }
    }

    /**
     * Returns the fields of this class.
     *
     * @return The fields of this class. Never null. May be empty.
     */
    public Set<FieldDoc> getFields()
    {
        return this.fields;
    }

    /**
     * Returns the constructor.
     *
     * @return The constructor or null if class has no constructor.
     */
    public ConstructorDoc getConstructor()
    {
        return this.constructor;
    }

    /**
     * Returns the super classes.
     *
     * @return The super classes. Never null. May be empty.
     */
    public Set<ClassDoc> getSuperClasses()
    {
        return this.superClasses;
    }

    /**
     * Returns the interfaces implemented by this class.
     *
     * @return The interfaces this class implements. Never null. May be empty.
     */
    public Set<InterfaceDoc> getInterfaces()
    {
        return this.interfaces;
    }

    /**
     * Adds a sub class.
     *
     * @param cls
     *            The sub class to add.
     */
    void addSubClass(final ClassDoc cls)
    {
        this.subClasses.add(cls);
    }

    /**
     * Returns the sub classes of this class.
     *
     * @return The sub classes of this class.
     */
    public Set<ClassDoc> getSubClasses()
    {
        return this.immutableSubClasses;
    }

    /**
     * Returns a list of all super classes.
     *
     * @return the list of all super classes.
     */
    public List<ClassDoc> getAllSuperClasses()
    {
        final List<ClassDoc> result = new ArrayList<ClassDoc>();
        for (final ClassDoc superClass: this.superClasses)
        {
            result.add(superClass);
        }
        for (final ClassDoc superClass: this.superClasses)
        {
            result.addAll(superClass.getAllSuperClasses());
        }
        return result;
    }

    /**
     * Returns the first super class of this class. Calling this method only
     * makes sense when the class does not use multiple inheritance.
     *
     * @return The first super class of this class.
     */
    public ClassDoc getSuperClass()
    {
        final Iterator<ClassDoc> iterator = this.superClasses.iterator();
        if (iterator.hasNext()) return iterator.next();
        return null;
    }

    /**
     * Checks if this class (or one of its parent classes) uses multiple
     * inheritance.
     *
     * @return True if multiple inheritance is used, false if not.
     */
    public boolean isMultipleInheritance()
    {
        final int size = this.superClasses.size();
        if (size > 1) return true;
        if (size == 0) return false;
        return this.superClasses.iterator().next().isMultipleInheritance();
    }

    /**
     * Return a list of super classes with additional methods they provide.
     *
     * @return The list of super classes and the additional methods they
     *         provide.
     */
    public List<Map.Entry<ClassDoc, Set<MethodDoc>>> getInheritedMethods()
    {
        final List<Map.Entry<ClassDoc, Set<MethodDoc>>> result =
            new ArrayList<Map.Entry<ClassDoc, Set<MethodDoc>>>();

        // Initialize the list of already seen methods
        final Set<String> seenMethods = new HashSet<String>();
        for (final MethodDoc method: this.getMethods())
            seenMethods.add(method.getName());

        for (final ClassDoc superClass: getAllSuperClasses())
        {
            final Set<MethodDoc> methods = new TreeSet<MethodDoc>();
            for (final MethodDoc method: superClass.getMethods())
            {
                if (seenMethods.contains(method.getName())) continue;
                methods.add(method);
                seenMethods.add(method.getName());
            }
            if (methods.isEmpty()) continue;
            result.add(new AbstractMap.SimpleEntry<ClassDoc, Set<MethodDoc>>(
                superClass, methods));
        }

        return result;
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractTypeDoc#getSuperTypes()
     */
    @Override
    Set<? extends AbstractTypeDoc> getSuperTypes()
    {
        return this.superClasses;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.getNamespace().getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.CLASS;
    }
}
