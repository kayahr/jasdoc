/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Comparator;

/**
 * Comparator which compares the simple names of named documentation element.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class SimpleNameComparator implements Comparator<NamedDoc>
{
    /** The singleton instance. */
    private static final SimpleNameComparator INSTANCE =
        new SimpleNameComparator();

    /**
     * Private constructor to prevent instantiation.
     */
    private SimpleNameComparator()
    {
        // Empty
    }

    /**
     * Returns the singleton instance of this comparator.
     *
     * @return The singleton instance.
     */
    public static SimpleNameComparator getInstance()
    {
        return INSTANCE;
    }

    /**
     * @see Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final NamedDoc o1, final NamedDoc o2)
    {
        return o1.getName().compareTo(o2.getName());
    }
}
