/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.Tags;

/**
 * Base class for class and interface documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractTypeDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The methods. */
    private Set<MethodDoc> methods;

    /** The namespace this type belongs to. */
    private final NamespaceDoc namespace;

    /** Internal map of all members of this type. */
    private final Map<String, MemberDoc> members =
        new HashMap<String, MemberDoc>();

    /** Internal map of all static members of this type. */
    private final Map<String, MemberDoc> staticMembers =
        new HashMap<String, MemberDoc>();

    /**
     * @param name
     *            The type name. Must not be null.
     * @param description
     *            The type description. Must not be null.
     * @param namespace
     *            The namespace this type belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public AbstractTypeDoc(final String name, final String description,
        final NamespaceDoc namespace, final Tags tags)
    {
        super(name, description, namespace, tags);
        if (namespace == null)
            throw new IllegalArgumentException("namespace is null");
        this.namespace = namespace;
    }

    /**
     * Adds the specified methods.
     *
     * @param methods
     *            The methods to add.
     */
    final void setMethods(final Collection<MethodDoc> methods)
    {
        this.methods =
            Collections.unmodifiableSet(new TreeSet<MethodDoc>(methods));
        final GlobalDoc global = getGlobal();
        for (final MethodDoc method: methods)
        {
            if (method.getModifier().isStatic())
                this.staticMembers.put(method.getName(), method);
            else
                this.members.put(method.getName(), method);

            global.indexMethod(method);
        }
    }

    /**
     * Returns the namespace this type belongs to.
     *
     * @return The namespace this type belongs to.
     */
    public final NamespaceDoc getNamespace()
    {
        return this.namespace;
    }

    /**
     * Returns the methods of this type.
     *
     * @return The methods. Never null. May be empty.
     */
    public final Set<MethodDoc> getMethods()
    {
        return this.methods;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public final String getUrl()
    {
        return this.namespace.getPath() + getName() + ".html";
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public final String getQualifiedName()
    {
        final String namespace = this.namespace.getQualifiedName();
        if (namespace.isEmpty()) return getName();
        return namespace + "." + getName();
    }

    /**
     * Adds a member.
     *
     * @param member
     *            The member to add.
     */
    final void addMember(final MemberDoc member)
    {
        this.members.put(member.getName(), member);
    }

    /**
     * Adds a static member.
     *
     * @param member
     *            The static member to add.
     */
    final void addStaticMember(final MemberDoc member)
    {
        this.staticMembers.put(member.getName(), member);
    }

    /**
     * Returns the member with the specified name.
     *
     * @param name
     *            The member name.
     * @return The member or null if not found.
     */
    final MemberDoc getMember(final String name)
    {
        return this.members.get(name);
    }

    /**
     * Returns the static member with the specified name.
     *
     * @param name
     *            The static member name.
     * @return The static member or null if not found.
     */
    final MemberDoc getStaticMember(final String name)
    {
        return this.staticMembers.get(name);
    }

    /**
     * Returns the super types of this type.
     *
     * @return The super types.
     */
    abstract Set<? extends AbstractTypeDoc> getSuperTypes();

    /**
     * Returns the super method of the specified method.
     *
     * @param name
     *            The method name.
     * @param staticMethod
     *            True if static method, false if not.
     * @return The super method. Null if not found.
     */
    public final MethodDoc getSuperMethod(final String name,
        final boolean staticMethod)
    {
        final MemberDoc member =
            staticMethod ? getStaticMember(name) : getMember(name);
        if (member != null && (member instanceof MethodDoc))
            return (MethodDoc) member;

        if (this instanceof ClassDoc)
        {
            for (final InterfaceDoc intf: ((ClassDoc) this).getInterfaces())
            {
                final MethodDoc superMethod =
                    intf.getSuperMethod(name, staticMethod);
                if (superMethod != null) return superMethod;
            }
        }

        for (final AbstractTypeDoc superType: getSuperTypes())
        {
            final MethodDoc superMethod =
                superType.getSuperMethod(name, staticMethod);
            if (superMethod != null) return superMethod;
        }
        return null;
    }

    /**
     * Returns the first super type of this type.
     *
     * @return The first super type of this type or null if there is no super
     *         type.
     */
    public final AbstractTypeDoc getSuperType()
    {
        final Iterator<? extends AbstractTypeDoc> iterator =
            getSuperTypes().iterator();
        if (iterator.hasNext()) return iterator.next();
        return null;
    }
}
