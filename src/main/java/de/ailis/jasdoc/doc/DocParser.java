/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.ailis.jasdoc.tags.ClassTag;
import de.ailis.jasdoc.tags.ConstTag;
import de.ailis.jasdoc.tags.DescriptionTag;
import de.ailis.jasdoc.tags.EnumTag;
import de.ailis.jasdoc.tags.ExtendsTag;
import de.ailis.jasdoc.tags.FinalTag;
import de.ailis.jasdoc.tags.ImplementsTag;
import de.ailis.jasdoc.tags.InheritDocTag;
import de.ailis.jasdoc.tags.NamespaceTag;
import de.ailis.jasdoc.tags.ParamTag;
import de.ailis.jasdoc.tags.PrivateTag;
import de.ailis.jasdoc.tags.ProtectedTag;
import de.ailis.jasdoc.tags.ReturnTag;
import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.tags.TypeTag;
import de.ailis.jasdoc.tree.FuncNode;
import de.ailis.jasdoc.tree.Node;
import de.ailis.jasdoc.tree.RootNode;
import de.ailis.jasdoc.types.AnyType;
import de.ailis.jasdoc.types.ClassType;
import de.ailis.jasdoc.types.AbstractType;
import de.ailis.jasdoc.types.NumberType;

/**
 * Parses a JavaScript root tree node into a documentation model.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class DocParser
{
    /** Cache of already created documentation elements. */
    private final Map<String, AbstractElementDoc> cache =
        new HashMap<String, AbstractElementDoc>();

    /** The documentation parser configuration. */
    private final Configuration config;

    /** The root node. */
    private RootNode rootNode;

    /**
     * Constructor.
     *
     * @param config
     *            The documentation parser configuration.
     */
    public DocParser(final Configuration config)
    {
        this.config = config;
    }

    /**
     * Parses the specified root node into a documentation model.
     *
     * @param rootNode
     *            The root node of the parsed JavaScript tree.
     * @return The documentation model.
     */
    public GlobalDoc parse(final RootNode rootNode)
    {
        final GlobalDoc global = new GlobalDoc(this.config.getTitle(), "");
        this.rootNode = rootNode;
        this.cache.put("", global);
        parse(rootNode, global);
        return global;
    }

    /**
     * Parses the specified javascript tree node into the specified
     * documentation model namespace.
     *
     * @param node
     *            The JavaScript tree node.
     * @param namespace
     *            The documentation model namespace.
     */
    private void parse(final Node node, final NamespaceDoc namespace)
    {
        for (final Node childNode: node.getChildNodes())
        {
            if (this.config.isExcluded(childNode.getQualifiedName())) continue;

            if (childNode.isNamespace())
            {
                final NamespaceDoc childNamespace = createNamespace(childNode);
                namespace.addNamespace(childNamespace);
                parse(childNode, childNamespace);
            }
            else if (childNode.isClass())
            {
                namespace.addClass(createClass(childNode));
            }
            else if (childNode.isInterface())
            {
                namespace.addInterface(createInterface(childNode));
            }
            else if (childNode.isEnum())
            {
                namespace.addEnum(createEnum(childNode));
            }
            else if (childNode.isFunction())
            {
                namespace.addFunction(createFunction((FuncNode) childNode));
            }
            else if (childNode.isVariable())
            {
                namespace.addVariable(createVariable(childNode));
            }
            else
            {
                throw new RuntimeException("Don't know what to do with "
                    + childNode.getQualifiedName());
            }
        }
    }

    /**
     * Returns the description from the specified JavaScript tree node.
     *
     * @param node
     *            The JavaScript tree node.
     * @return The description. Never null. May be empty.
     */
    private String getDescription(final Node node)
    {
        return getDescription(node.getTags());
    }

    /**
     * Returns the description for the specified tags.
     *
     * @param tags
     *            The tags.
     * @return The description. Never null. May be empty.
     */
    private String getDescription(final Tags tags)
    {
        final DescriptionTag tag =
            tags.getTag(DescriptionTag.class);
        if (tag != null) return tag.getDescription();
        return "";
    }

    /**
     * Returns the namespace description from the specified node.
     *
     * @param node
     *            The JavaScript tree node.
     * @return The namespace description. Never null. May be empty.
     */
    private String getNamespaceDescription(final Node node)
    {
        final NamespaceTag tag =
            node.getTags().getTag(NamespaceTag.class);        
        if (tag != null)
        {
            final String description = tag.getDescription();
            if (!description.isEmpty()) return description;
        }
        return getDescription(node);
    }

    /**
     * Returns the class description from the specified node.
     *
     * @param node
     *            The JavaScript tree node.
     * @return The class description.
     */
    private String getClassDescription(final Node node)
    {
        final ClassTag tag =
            node.getTags().getTag(ClassTag.class);
        if (tag == null) return "";
        return tag.getDescription();
    }

    /**
     * Returns the return value description from the specified tags.
     *
     * @param tags
     *            The tags.
     * @return The return value description. Null if no return tag was found on
     *         the node.
     */
    private String getReturnDescription(final Tags tags)
    {
        final ReturnTag tag = tags.getTag(ReturnTag.class);
        if (tag != null) return tag.getDescription();
        return null;
    }

    /**
     * Returns the description for the specified parameter JavaScript tree node.
     *
     * @param node
     *            The parameter JavaScript tree node.
     * @param executableTags
     *            The method/function/constructor tags.
     * @return The description. Never null. May be empty.
     */
    private String getParamDescription(final Node node,
        final Tags executableTags)
    {
        final String paramName = node.getName();
        final ParamTag tag = executableTags.getParamTag(paramName);
        if (tag != null) return tag.getDescription();
        return getDescription(node);
    }

    /**
     * Returns the modifier for the specified constructor node.
     *
     * @param node
     *            The JavaScript tree node of the constructor.
     * @return The constructor modifier.
     */
    private Modifier getConstructorModifier(final Node node)
    {
        final Tags tags = node.getTags();
        final boolean isProtected =
            tags.isTagPresent(ProtectedTag.class);
        final boolean isPrivate =
            tags.isTagPresent(PrivateTag.class);
        return new Modifier(isPrivate, isProtected, false, false, false);
    }

    /**
     * Returns the modifier for the specified member node.
     *
     * @param node
     *            The JavaScript tree node of the member.
     * @return The member modifier.
     */
    private Modifier getFieldModifier(final Node node)
    {
        final Tags tags = node.getTags();
        final boolean isProtected =
            tags.isTagPresent(ProtectedTag.class);
        final boolean isPrivate =
            tags.isTagPresent(PrivateTag.class);
        final boolean isStatic =
            !node.getParentNode().isPrototype();
        final boolean isFinal =
            tags.isTagPresent(FinalTag.class);
        final boolean isConst =
            tags.isTagPresent(ConstTag.class);
        return new Modifier(isPrivate, isProtected, isStatic, isFinal, isConst);
    }

    /**
     * Returns the modifier for the specified member node.
     *
     * @param node
     *            The JavaScript tree node of the member.
     * @param tags
     *            The method tags.
     * @return The member modifier.
     */
    private Modifier getMethodModifier(final Node node, final Tags tags)
    {
        final boolean isProtected =
            tags.isTagPresent(ProtectedTag.class);
        final boolean isPrivate =
            tags.isTagPresent(PrivateTag.class);
        final boolean isStatic =
            !node.getParentNode().isPrototype();
        final boolean isFinal =
            tags.isTagPresent(FinalTag.class);
        final boolean isConst =
            tags.isTagPresent(ConstTag.class);
        return new Modifier(isPrivate, isProtected, isStatic, isFinal, isConst);
    }

    /**
     * Returns the modifier for the specified variable node.
     *
     * @param node
     *            The JavaScript tree node of the variable.
     * @return The variable modifier.
     */
    private Modifier getVariableModifier(final Node node)
    {
        final Tags tags = node.getTags();
        final boolean isConst =
            tags.isTagPresent(ConstTag.class);
        return new Modifier(false, false, false, false, isConst);
    }

    /**
     * Returns the modifier for the specified function node.
     *
     * @param node
     *            The JavaScript tree node of the function.
     * @return The function modifier.
     */
    private Modifier getFunctionModifier(final Node node)
    {
        return new Modifier(false, false, false, false, false);
    }

    /**
     * Returns the modifier for the specified enum constant node.
     *
     * @param node
     *            The JavaScript tree node of the enum constant.
     * @return The enum constant modifier.
     */
    private Modifier getEnumConstantModifier(final Node node)
    {
        return new Modifier(false, false, true, true, true);
    }

    /**
     * Returns the parameter type of the specified parameter JavaScript tree
     * node.
     *
     * @param node
     *            The parameter JavaScript tree node.
     * @param tags
     *            The tags.
     * @return The type. Never null. May be the ANY type if no type was found.
     */
    private AbstractType getParamType(final Node node, final Tags tags)
    {
        final String paramName = node.getName();
        final ParamTag tag = tags.getParamTag(paramName);
        if (tag != null) return tag.getType();
        return new AnyType();
    }

    /**
     * Returns the enum type of the specified enum JavaScript tree node.
     *
     * @param node
     *            The enum JavaScript tree node.
     * @return The type. Never null. May be the number type if no type was
     *         found.
     */
    private AbstractType getEnumType(final Node node)
    {
        final EnumTag tag =
            node.getTags().getTag(EnumTag.class);
        if (tag != null) return tag.getType();
        return new NumberType();
    }

    /**
     * Returns the variable type of the specified variable JavaScript tree node.
     *
     * @param node
     *            The variable JavaScript tree node.
     * @return The type. Never null. May be the ANY type if no type was found.
     */
    private AbstractType getVariableType(final Node node)
    {
        final TypeTag tag =
            node.getTags().getTag(TypeTag.class);
        if (tag != null) return tag.getType();
        return new AnyType();
    }

    /**
     * Returns the enum constant type of the specified enum constant JavaScript
     * tree node.
     *
     * @param node
     *            The enum constant JavaScript tree node.
     * @return The enum constant type. Never null. May be the number type if no
     *         type was found.
     */
    private AbstractType getEnumConstantType(final Node node)
    {
        return getEnumType(node.getParentNode());
    }

    /**
     * Returns the field type from the specified parameter JavaScript tree node.
     *
     * @param node
     *            The parameter JavaScript tree node.
     * @return The field type. Never null. May be the ANY type if no type was
     *         found.
     */
    private AbstractType getFieldType(final Node node)
    {
        final TypeTag tag =
            node.getTags().getTag(TypeTag.class);
        if (tag != null) return tag.getType();
        return new AnyType();
    }

    /**
     * Returns the result type of the specified function/method JavaScript tree
     * node.
     *
     * @param tags
     *            The tags.
     * @return The type. Null if no return tag was found on the node.
     */
    private AbstractType getReturnType(final Tags tags)
    {
        final ReturnTag tag = tags.getTag(ReturnTag.class);
        if (tag != null) return tag.getType();
        return null;
    }

    /**
     * Creates a new class documentation element.
     *
     * @param node
     *            The JavaScript tree node of the class.
     * @return The created class documentation element.
     */
    private ClassDoc createClass(final Node node)
    {
        ClassDoc cls = getCached(node, ClassDoc.class);
        if (cls == null)
        {
            final String description = getClassDescription(node);
            final String name = node.getName();
            final NamespaceDoc namespace =
                createNamespace(node.getParentNode());
            final Set<ClassDoc> superClasses = createSuperClasses(node);
            final Set<InterfaceDoc> interfaces =
                createImplementedInterfaces(node);
            cls = new ClassDoc(name, description, namespace, superClasses,
                interfaces, node.getTags());
            cls.setConstructor(createConstructor((FuncNode) node, cls));
            cls.setMethods(createMethods(node, cls));
            cls.setFields(createFields(node, cls));
            cache(node, cls);
        }
        return cls;
    }

    /**
     * Creates a new enum documentation element.
     *
     * @param node
     *            The JavaScript tree node of the enum.
     * @return The created enum documentation element.
     */
    private EnumDoc createEnum(final Node node)
    {
        EnumDoc cls = getCached(node, EnumDoc.class);
        if (cls == null)
        {
            final String description = getDescription(node);
            final AbstractType type = getEnumType(node);
            final String name = node.getName();
            final NamespaceDoc namespace =
                createNamespace(node.getParentNode());
            cls = new EnumDoc(name, description, namespace, type,
                node.getTags());
            cls.setConstants(createEnumConstants(node, cls));
            cache(node, cls);
        }
        return cls;
    }

    /**
     * Creates a new variable documentation element.
     *
     * @param node
     *            The JavaScript tree node of the variable.
     * @return The created variable documentation element.
     */
    private VariableDoc createVariable(final Node node)
    {
        VariableDoc variable = getCached(node, VariableDoc.class);
        if (variable == null)
        {
            final String description = getDescription(node);
            final AbstractType type = getVariableType(node);
            final String name = node.getName();
            final Modifier modifier = getVariableModifier(node);
            final NamespaceDoc namespace =
                createNamespace(node.getParentNode());
            variable =
                new VariableDoc(type, name, description, modifier,
                    namespace, node.getTags());
            cache(node, variable);
        }
        return variable;
    }

    /**
     * Creates the list of enum constants from the specified JavaScript enum
     * tree node.
     *
     * @param node
     *            The enum JavaScript tree node.
     * @param enm
     *            The enum the constants belongs to.
     * @return The list of enum constants.
     */
    private Collection<EnumConstantDoc> createEnumConstants(final Node node,
        final EnumDoc enm)
    {
        final Collection<EnumConstantDoc> constants =
            new ArrayList<EnumConstantDoc>();
        for (final Node child: node.getChildNodes())
        {
            constants.add(createEnumConstant(child, enm));
        }
        return constants;
    }

    /**
     * Creates an enum constant.
     *
     * @param node
     *            The JavaScript enum constant tree node.
     * @param enm
     *            The enum this constant belongs to.
     * @return The created enum constant.
     */
    private EnumConstantDoc createEnumConstant(final Node node,
        final EnumDoc enm)
    {
        final String name = node.getName();
        final String description = getDescription(node);
        final AbstractType type = getEnumConstantType(node);
        final Modifier modifier = getEnumConstantModifier(node);
        return new EnumConstantDoc(type, name, description, modifier, enm,
            node.getTags());
    }

    /**
     * Returns the interfaces implemented by the specified JavaScript class tree
     * node.
     *
     * @param node
     *            The JavaScript class tree node.
     * @return The implemented interfaces.
     */
    private Set<InterfaceDoc> createImplementedInterfaces(final Node node)
    {
        final Set<InterfaceDoc> interfaces = new TreeSet<InterfaceDoc>();
        final ImplementsTag tag =
            node.getTags().getTag(ImplementsTag.class);
        if (tag != null)
        {
            for (final ClassType type: tag.getTypes())
            {
                final Node intfNode = this.rootNode.resolve(type.getName());
                interfaces.add(createInterface(intfNode));
            }
        }
        return interfaces;
    }

    /**
     * Returns the documentation element cached for the specified node.
     *
     * @param node
     *            The JavaScript tree node.
     * @param c
     *            The class name of the expected element.
     * @return The cached documentation element or null if there is no matching
     *         element.
     * @param <T>
     *            The requested type.
     */
    @SuppressWarnings("unchecked")
    private <T> T getCached(final Node node, final Class<?> c)
    {
        final AbstractElementDoc cached = this.cache.get(node.getQualifiedName());
        if (cached == null) return null;
        if (!c.isAssignableFrom(cached.getClass()))
            throw new RuntimeException(
                "Unexpected element type in cache for path "
                    + node.getQualifiedName() + ". Expected class: " + c
                    + ", Found class: " + cached.getClass());
        return (T) cached;
    }

    /**
     * Caches an created element for the specified node.
     *
     * @param node
     *            The JavaScript tree node.
     * @param element
     *            The documentation element created for this node.
     */
    private void cache(final Node node, final AbstractElementDoc element)
    {
        this.cache.put(node.getQualifiedName(), element);
    }

    /**
     * Creates a namespace documentation element.
     *
     * @param node
     *            The JavaScript namespace tree node.
     * @return The created namespace documentaiton element.
     */
    private NamespaceDoc createNamespace(final Node node)
    {
        NamespaceDoc namespace = getCached(node, NamespaceDoc.class);
        if (namespace == null)
        {
            final String name = node.getName();
            final String description = getNamespaceDescription(node);            
            final NamespaceDoc parent = createNamespace(node.getParentNode());
            namespace = new NamespaceDoc(name, description, parent,
                node.getTags());
            cache(node, namespace);
        }
        return namespace;
    }

    /**
     * Returns the interfaces extended by the specified JavaScript interface
     * tree node.
     *
     * @param node
     *            The JavaScript interface tree node.
     * @return The extended interfaces.
     */
    private Set<InterfaceDoc> createExtendedInterfaces(final Node node)
    {
        final Set<InterfaceDoc> interfaces = new TreeSet<InterfaceDoc>();
        final ExtendsTag tag =
            node.getTags().getTag(ExtendsTag.class);
        if (tag != null)
        {
            for (final ClassType type: tag.getTypes())
            {
                final Node intfNode = this.rootNode.resolve(type.getName());
                interfaces.add(createInterface(intfNode));
            }
        }
        return interfaces;
    }

    /**
     * Creates and returns an interface documentation element.
     *
     * @param node
     *            The JavaScript tree node of the interface.
     * @return The interface documentation element.
     */
    private InterfaceDoc createInterface(final Node node)
    {
        InterfaceDoc intf = getCached(node, InterfaceDoc.class);
        if (intf == null)
        {

            final String name = node.getName();
            final NamespaceDoc namespace =
                createNamespace(node.getParentNode());
            final String description = getDescription(node);
            final Collection<InterfaceDoc> superInterfaces =
                createExtendedInterfaces(node);
            intf = new InterfaceDoc(name, description, namespace,
                superInterfaces, node.getTags());
            intf.setMethods(createMethods(node, intf));
            cache(node, intf);
        }
        return intf;
    }

    /**
     * Returns all fields from the specified JavaScript class tree node.
     *
     * @param node
     *            The JavaScript class tree node.
     * @param cls
     *            The class the fields belongs to.
     * @return The fields.
     */
    private Set<FieldDoc> createFields(final Node node, final ClassDoc cls)
    {
        final Set<FieldDoc> fields = new TreeSet<FieldDoc>();
        for (final Node childNode: node.getChildNodes())
        {
            if (childNode.isField())
            {
                final FieldDoc methodDoc = createField(childNode, cls);
                if (methodDoc != null) fields.add(methodDoc);
            }
        }
        final Node prototype = node.getChildNode("prototype");
        if (prototype != null)
        {
            for (final Node childNode: prototype.getChildNodes())
            {
                if (childNode.isField())
                {
                    final FieldDoc fieldDoc = createField(childNode, cls);
                    if (fieldDoc != null) fields.add(fieldDoc);
                }
            }
        }
        return fields;
    }

    /**
     * Returns all super classes of the specified JavaScript class tree node.
     *
     * @param node
     *            The JavaScript class tree node.
     * @return The super classes.
     */
    private Set<ClassDoc> createSuperClasses(final Node node)
    {
        final Set<ClassDoc> superClasses = new TreeSet<ClassDoc>();
        final ExtendsTag tag =
            node.getTags().getTag(ExtendsTag.class);
        if (tag != null)
        {
            for (final ClassType type: tag.getTypes())
            {
                final Node classNode = this.rootNode.resolve(type.getName());
                superClasses.add(createClass(classNode));
            }
        }
        return superClasses;
    }

    /**
     * Creates the list of methods in the specified JavaScript class tree node.
     *
     * @param node
     *            The JavaScript class tree node.
     * @param declaringType
     *            The declaring type.
     * @return The list of methods.
     */
    private Set<MethodDoc> createMethods(final Node node,
        final AbstractTypeDoc declaringType)
    {
        final Set<MethodDoc> methods = new TreeSet<MethodDoc>();
        for (final Node childNode: node.getChildNodes())
        {
            if (childNode.isFunction())
            {
                final MethodDoc methodDoc =
                    createMethod((FuncNode) childNode, declaringType);
                if (methodDoc != null) methods.add(methodDoc);
            }
        }
        final Node prototype = node.getChildNode("prototype");
        if (prototype != null)
        {
            for (final Node childNode: prototype.getChildNodes())
            {
                if (childNode.isFunction())
                {
                    final MethodDoc methodDoc =
                        createMethod((FuncNode) childNode, declaringType);
                    if (methodDoc != null) methods.add(methodDoc);
                }
            }
        }
        return methods;
    }

    /**
     * Returns the tags for a method.
     *
     * @param node
     *            The method node.
     * @param declaringType
     *            The type declaring the method.
     * @return The tags.
     */
    private Tags getMethodTags(final FuncNode node,
        final AbstractTypeDoc declaringType)
    {
        // If InheritDoc tag is not present then simply return the node tags.
        final Tags tags = node.getTags();
        if (!tags.isTagPresent(InheritDocTag.class)) return tags;

        // Merge tags with the super type if there is a super type.
        final String name = node.getName();
        final boolean staticMethod = !node.getParentNode().isPrototype();
        final MethodDoc superMethod =
            declaringType.getSuperMethod(name, staticMethod);
        if (superMethod == null) return tags;
        return Tags.merge(tags, superMethod.getTags());
    }

    /**
     * Creates the method from the specified JavaScript tree node.
     *
     * @param node
     *            The method JavaScript tree node.
     * @param declaringType
     *            The declaring type.
     * @return The method. May be null if method is not to be documented because
     *         it has been annotated to be ignored or doesn't have the necessary
     *         visibility.
     */
    private MethodDoc createMethod(final FuncNode node,
        final AbstractTypeDoc declaringType)
    {
        final Tags tags = getMethodTags(node, declaringType);
        final Modifier modifier = getMethodModifier(node, tags);
        if (!this.config.isPrivateIncluded() && modifier.isPrivate())
            return null;
        if (!this.config.isProtectedIncluded() && modifier.isProtected())
            return null;
        final String description = getDescription(tags);
        final String name = node.getName();
        final MethodDoc method = new MethodDoc(name, description, modifier,
            declaringType, node.getTags());
        method.setResult(createResult(method, tags));
        method.setParameters(createParameters(node, method, tags));
        return method;
    }

    /**
     * Creates the method from the specified JavaScript tree node.
     *
     * @param node
     *            The method JavaScript tree node.
     * @return The method. May be null if method is not to be documented because
     *         it has been annotated to be ignored or doesn't have the necessary
     *         visibility.
     */
    private FunctionDoc createFunction(final FuncNode node)
    {
        final String description = getDescription(node);
        final String name = node.getName();
        final NamespaceDoc namespace = createNamespace(node.getParentNode());
        final Modifier modifier = getFunctionModifier(node);
        final FunctionDoc function = new FunctionDoc(name, description,
            modifier, namespace, node.getTags());
        function.setResult(createResult(function, node.getTags()));
        function.setParameters(createParameters(node, function,
            node.getTags()));
        return function;
    }

    /**
     * Creates a field from the specified JavaScript tree node.
     *
     * @param node
     *            The field JavaScript tree node.
     * @param cls
     *            The class the field belongs to.
     * @return The field. May be null if field is not to be documented because
     *         it has been annotated to be ignored or doesn't have the necessary
     *         visibility.
     */
    private FieldDoc createField(final Node node, final ClassDoc cls)
    {
        final Modifier modifier = getFieldModifier(node);
        if (!this.config.isPrivateIncluded() && modifier.isPrivate())
            return null;
        if (!this.config.isProtectedIncluded() && modifier.isProtected())
            return null;
        final String description = getDescription(node);
        final String name = node.getName();
        final AbstractType type = getFieldType(node);
        return new FieldDoc(type, name, description, modifier, cls,
            node.getTags());
    }

    /**
     * Creates the constructor for the specified JavaScript tree node.
     *
     * @param node
     *            The constructor/class JavaScript tree node.
     * @param cls
     *            The class this constructor belongs to.
     * @return The constructor. May be null if constructor is not to be
     *         documented because it has been annotated to be ignored or doesn't
     *         have the necessary visibility.
     */
    private ConstructorDoc createConstructor(final FuncNode node,
        final ClassDoc cls)
    {
        final Modifier modifier = getConstructorModifier(node);
        if (!this.config.isPrivateIncluded() && modifier.isPrivate())
            return null;
        if (!this.config.isProtectedIncluded() && modifier.isProtected())
            return null;
        final String description = getDescription(node);
        final String name = node.getName();
        final ConstructorDoc constructor =
            new ConstructorDoc(name, description,
                modifier, cls, node.getTags());
        constructor.setParameters(createParameters(node, constructor,
            node.getTags()));
        return constructor;
    }

    /**
     * Creates the list of parameters for the specified function node.
     *
     * @param node
     *            The function JavaScript tree node.
     * @param executable
     *            The executable the parameters belongs to.
     * @param methodTags
     *            The method tags.
     * @return The parameter list. Never null. May be empty.
     */
    private List<ParameterDoc> createParameters(final FuncNode node,
        final AbstractExecutableDoc executable, final Tags methodTags)
    {
        final List<Node> paramNodes = node.getParameters();
        final List<ParameterDoc> parameters =
            new ArrayList<ParameterDoc>(paramNodes.size());
        for (final Node paramNode: paramNodes)
            parameters.add(createParameter(paramNode, executable, methodTags));
        return parameters;
    }

    /**
     * Creates a parameter.
     *
     * @param node
     *            The parameter JavaScript tree node.
     * @param executable
     *            The executable this parameter belongs to.
     * @param methodTags
     *            The method tags.
     * @return The parameter. Never null.
     */
    private ParameterDoc createParameter(final Node node,
        final AbstractExecutableDoc executable, final Tags methodTags)
    {
        final AbstractType type = getParamType(node, methodTags);
        final String name = node.getName();
        final String description = getParamDescription(node, methodTags);
        return new ParameterDoc(type, name, description, executable,
            node.getTags());
    }

    /**
     * Creates a function/method result.
     *
     * @param executable
     *            The executable This result belongs to.
     * @param tags
     *            The tags.
     * @return The function/method result. Never null.
     */
    private ResultDoc createResult(final AbstractExecutableDoc executable,
        final Tags tags)
    {
        final AbstractType type = getReturnType(tags);
        if (type == null) return null;
        final String description = getReturnDescription(tags);
        if (description == null) return null;
        return new ResultDoc(type, description, executable, tags);
    }
}
