/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;
import de.ailis.jasdoc.types.AbstractType;

/**
 * Parameter documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ParameterDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The parameter type. */
    private final AbstractType type;

    /** The executable this parameter belongs to. */
    private final AbstractExecutableDoc executable;

    /**
     * Constructor.
     *
     * @param type
     *            The parameter type. Must not be null.
     * @param name
     *            The parameter name. Must not be null or empty.
     * @param description
     *            The parameter description. Must not be null. May be empty.
     * @param executable
     *            The executable this parameter belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public ParameterDoc(final AbstractType type, final String name,
        final String description, final AbstractExecutableDoc executable,
        final Tags tags)
    {
        super(name, description, executable, tags);
        if (name.isEmpty())
            throw new IllegalArgumentException("name is empty");
        if (type == null)
            throw new IllegalArgumentException("type is null");
        if (executable == null)
            throw new IllegalArgumentException("executable is null");
        this.executable = executable;
        this.type = type;
    }

    /**
     * Returns the parameter type.
     *
     * @return The parameter type. Never null.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.executable.getUrl() + ";" + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return this.executable.getQualifiedName() + ";" + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.executable.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.PARAMETER;
    }
}
