/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

/**
 * Symbols doc.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class SymbolsDoc implements ModelDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The index letter. */
    private final char letter;

    /** The generation time. */
    private final Date generationTime = new Date();

    /** The index documentation. */
    private final IndexDoc index;

    /** The symbols in this index. */
    private final Set<ModelDoc> symbols = new TreeSet<ModelDoc>();

    /**
     * Constructor.
     *
     * @param index
     *            The index documentation.
     * @param letter
     *            The index letter (lower-case).
     */
    public SymbolsDoc(final IndexDoc index, final char letter)
    {
        this.letter = letter;
        this.index = index;
    }

    /**
     * Adds a symbol to this index.
     *
     * @param symbol
     *            The symbol to add.
     */
    void addSymbol(final ModelDoc symbol)
    {
        this.symbols.add(symbol);
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return "index-files/" + this.letter + ".html";
    }

    /**
     * @see de.ailis.jasdoc.doc.NamedDoc#getName()
     */
    @Override
    public String getName()
    {
        return "" + Character.toUpperCase(this.letter);
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return getName();
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final ModelDoc o)
    {
        return this.getName().compareTo(o.getName());
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return "../";
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getGlobal()
     */
    @Override
    public GlobalDoc getGlobal()
    {
        return this.index.getGlobal();
    }

    /**
     * Returns the index.
     *
     * @return The index.
     */
    public IndexDoc getIndex()
    {
        return this.index;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getGenerationTime()
     */
    @Override
    public Date getGenerationTime()
    {
        return this.generationTime;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getThis()
     */
    @Override
    public ModelDoc getThis()
    {
        return this;
    }

    /**
     * Returns the index letter.
     *
     * @return The index letter.
     */
    public char getLetter()
    {
        return this.letter;
    }

    /**
     * Returns the symbols.
     *
     * @return The symbols.
     */
    public Set<ModelDoc> getSymbols()
    {
        return Collections.unmodifiableSet(this.symbols);
    }
}
