/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;

/**
 * Method documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class MethodDoc extends AbstractExecutableDoc implements MemberDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The constructor modifier flags. */
    private final Modifier modifier;

    /** The class this method belongs to. */
    private final AbstractTypeDoc declaringType;

    /**
     * @param name
     *            The method name.
     * @param description
     *            The method description.
     * @param modifier
     *            The method modifier flags. Must not be null.
     * @param declaringType
     *            The class this method belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public MethodDoc(final String name, final String description,
        final Modifier modifier, final AbstractTypeDoc declaringType,
        final Tags tags)
    {
        super(name, description, declaringType, tags);
        if (modifier == null)
            throw new IllegalArgumentException("modifier is null");
        if (declaringType == null)
            throw new IllegalArgumentException("modifier is null");
        this.modifier = modifier;
        this.declaringType = declaringType;
    }

    /**
     * Returns the method modifier flags.
     *
     * @return The method modifier flags. Never null.
     */
    @Override
    public Modifier getModifier()
    {
        return this.modifier;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.declaringType.getUrl() + "#" + getId();
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        return this.declaringType.getQualifiedName()
            + (getModifier().isStatic() ? "." : "#") + getName();
    }

    /**
     * Returns the method ID. This is the name for normal member methods and the
     * name with a prepended "-" character for static method. This ID is used in
     * link anchors.
     *
     * @return The method ID.
     */
    public String getId()
    {
        if (this.modifier.isStatic())
            return "-" + getName();
        else
            return getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.declaringType.getBaseUrl();
    }

    /**
     * Returns the type this method is declared in.
     *
     * @return The declaring type.
     */
    public AbstractTypeDoc getDeclaringType()
    {
        return this.declaringType;
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.METHOD;
    }
}
