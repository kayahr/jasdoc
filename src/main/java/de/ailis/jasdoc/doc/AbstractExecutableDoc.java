/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.tags.Tags;

/**
 * Base class for function, method and constructor documentations.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractExecutableDoc extends AbstractElementDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The parameters. */
    private List<ParameterDoc> parameters;

    /** The result. Null if none. */
    private ResultDoc result;

    /**
     * @param name
     *            The name.
     * @param description
     *            The description.
     * @param parentDoc
     *            The parent documentation model. Null if none.
     * @param tags
     *            The tags. Must not be null.
     */
    public AbstractExecutableDoc(final String name, final String description,
        final ModelDoc parentDoc, final Tags tags)
    {
        super(name, description, parentDoc, tags);
    }

    /**
     * Sets the result.
     *
     * @param result
     *            The result or null if function has no result.
     */
    final void setResult(final ResultDoc result)
    {
        this.result = result;
    }

    /**
     * Sets the parameters.
     *
     * @param parameters
     *            The parameters. Must not be null, may be empty.
     */
    final void setParameters(final Collection<ParameterDoc> parameters)
    {
        if (parameters == null)
            throw new IllegalArgumentException("parameters is null");
        this.parameters = Collections.unmodifiableList(
            new ArrayList<ParameterDoc>(parameters));
    }

    /**
     * Returns the parameters.
     *
     * @return The parameters. Never null. May be empty.
     */
    public final List<ParameterDoc> getParameters()
    {
        return this.parameters;
    }

    /**
     * Returns the result or null if none.
     *
     * @return The result or null if none.
     */
    public final ResultDoc getResult()
    {
        return this.result;
    }
}
