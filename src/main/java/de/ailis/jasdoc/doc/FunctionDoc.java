/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc;

import de.ailis.jasdoc.tags.Tags;

/**
 * Function documentation.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class FunctionDoc extends AbstractExecutableDoc
{
    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The namespace this function belongs to. */
    private final NamespaceDoc namespace;

    /** The modifier flags. */
    private final Modifier modifier;

    /**
     * @param name
     *            The function name.
     * @param description
     *            The function description.
     * @param modifier
     *            The function modifiers. Must not be null.
     * @param namespace
     *            The namespace this function belongs to. Must not be null.
     * @param tags
     *            The tags. Must not be null.
     */
    public FunctionDoc(final String name, final String description,
        final Modifier modifier, final NamespaceDoc namespace, final Tags tags)
    {
        super(name, description, namespace, tags);
        if (namespace == null)
            throw new IllegalArgumentException("namespace is null");
        if (modifier == null)
            throw new IllegalArgumentException("modifier is null");
        this.namespace = namespace;
        this.modifier = modifier;
    }

    /**
     * Returns the namespace this function belongs to.
     *
     * @return The namespace of this function.
     */
    public NamespaceDoc getNamespace()
    {
        return this.namespace;
    }

    /**
     * @see de.ailis.jasdoc.doc.LinkableDoc#getUrl()
     */
    @Override
    public String getUrl()
    {
        return this.namespace.getUrl() + "#" + getName();
    }

    /**
     * @see de.ailis.jasdoc.doc.QualifiedDoc#getQualifiedName()
     */
    @Override
    public String getQualifiedName()
    {
        final String namespace = this.namespace.getQualifiedName();
        if (namespace.isEmpty()) return getName();
        return namespace + "." + getName();
    }

    /**
     * Returns the modifier flags.
     *
     * @return The modifier flags.
     */
    public Modifier getModifier()
    {
        return this.modifier;
    }

    /**
     * @see de.ailis.jasdoc.doc.ModelDoc#getBaseUrl()
     */
    @Override
    public String getBaseUrl()
    {
        return this.namespace.getBaseUrl();
    }

    /**
     * @see de.ailis.jasdoc.doc.AbstractElementDoc#getElementType()
     */
    @Override
    public ElementType getElementType()
    {
        return ElementType.FUNCTION;
    }
}
