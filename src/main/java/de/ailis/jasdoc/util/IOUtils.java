/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;

/**
 * IO utility methods.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class IOUtils
{
    /**
     * Private constructor to prevent instantiation.
     */
    private IOUtils()
    {
        // Empty
    }

    /**
     * Reads a text from the specified file and returns it.
     *
     * @param file
     *            The file.
     * @param charset
     *            The character set.
     * @return The read text.
     * @throws IOException
     *             If reading the text failed.
     */
    public static String readText(final File file, final String charset)
        throws IOException
    {
        final InputStream input = new FileInputStream(file);
        try
        {
            return readText(input, charset);
        }
        finally
        {
            input.close();
        }
    }

    /**
     * Reads a text from the specified URL and returns it.
     *
     * @param url
     *            The URL.
     * @param charset
     *            The character set.
     * @return The read text.
     * @throws IOException
     *             If reading the text failed.
     */
    public static String readText(final URL url, final String charset)
        throws IOException
    {
        final InputStream input = url.openStream();
        try
        {
            return readText(input, charset);
        }
        finally
        {
            input.close();
        }
    }

    /**
     * Reads a text from the specified resource and returns it.
     *
     * @param base
     *            The base class for resolving relative URLs.
     * @param name
     *            The resource name.
     * @param charset
     *            The character set.
     * @return The read text.
     * @throws IOException
     *             If reading the text failed.
     */
    public static String readTextResource(final Class<?> base,
        final String name, final String charset) throws IOException
    {
        final URL url = base.getResource(name);
        if (url == null)
            throw new FileNotFoundException("Resource " + name
                + " not found for base class " + base.getName());
        return readText(url, charset);
    }

    /**
     * Reads a text from the specified input stream and returns it.
     *
     * @param input
     *            The input stream.
     * @param charset
     *            The character set.
     * @return The read text.
     * @throws IOException
     *             If reading the text failed.
     */
    public static String readText(final InputStream input, final String charset)
        throws IOException
    {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        try
        {
            copy(input, output);
        }
        finally
        {
            output.close();
        }
        return output.toString(charset);
    }

    /**
     * Copies the specified input stream to the specified output stream.
     *
     * @param input
     *            The input stream.
     * @param output
     *            The output stream.
     * @throws IOException
     *             If copying failed.
     */
    public static void copy(final InputStream input, final OutputStream output)
        throws IOException
    {
        final byte[] buffer = new byte[8192];
        int read;
        while ((read = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, read);
        }
    }

    /**
     * Copies the specified input stream to the specified output stream.
     *
     * @param input
     *            The input stream.
     * @param output
     *            The output stream.
     * @throws IOException
     *             If copying failed.
     */
    public static void copy(final Reader input, final Writer output)
        throws IOException
    {
        final char[] buffer = new char[8192];
        int read;
        while ((read = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, read);
        }
    }

    /**
     * Copies the source file to the destination file.
     *
     * @param source
     *            The source file.
     * @param destination
     *            The destination file.
     * @throws IOException
     *             If copy fails.
     */
    public static void copy(final ExtFile source, final File destination)
        throws IOException
    {
        final InputStream input = source.openInputStream();
        try
        {
            final FileOutputStream output = new FileOutputStream(destination);
            try
            {
                copy(input, output);
            }
            finally
            {
                output.close();
            }
        }
        finally
        {
            input.close();
        }
    }

    /**
     * Writes a text to the specified file.
     *
     * @param file
     *            The file.
     * @param text
     *            The text to write.
     * @param charset
     *            The character set.
     * @throws IOException
     *             If writing the text failed.
     */
    public static void writeText(final File file, final String text,
        final String charset) throws IOException
    {
        final OutputStream output = new FileOutputStream(file);
        try
        {
            writeText(output, text, charset);
        }
        finally
        {
            output.close();
        }
    }

    /**
     * Writes a text to the specified stream.
     *
     * @param stream
     *            The output stream.
     * @param text
     *            The text to write.
     * @param charset
     *            The character set.
     * @throws IOException
     *             If writing the text failed.
     */
    public static void writeText(final OutputStream stream, final String text,
        final String charset) throws IOException
    {
        final Writer output = new OutputStreamWriter(stream, charset);
        try
        {
            output.write(text);
        }
        finally
        {
            output.close();
        }
    }
}
