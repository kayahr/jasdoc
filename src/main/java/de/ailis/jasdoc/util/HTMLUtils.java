/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;

/**
 * HTML utility methods.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */
public final class HTMLUtils
{
    /** The simple text white list which allows links. */
    private static final Whitelist SIMPLE_TEXT_WITH_LINKS =
        Whitelist.simpleText().addTags("a").addAttributes("a", "href");

    /**
     * Private constructor to prevent instantiation.
     */
    private HTMLUtils()
    {
        // Empty
    }

    /**
     * Parses the specified html code.
     * 
     * @param html
     *            The HTML code to parse.
     * @return The parsed document.
     */
    public static Document parse(final String html)
    {
        Document doc = Jsoup.parseBodyFragment(html);
        doc.outputSettings().prettyPrint(false);
        return doc;
    }

    /**
     * Cleans the specified HTML with the specified white list.
     * 
     * @param html
     *            The HTML code to clean.
     * @param whitelist
     *            The whitelist.
     * @return The cleaned HTML.
     */
    public static String clean(String html, Whitelist whitelist)
    {
        Document doc = parse(mask(html));
        Cleaner cleaner = new Cleaner(whitelist);
        Document clean = cleaner.clean(doc);
        clean.outputSettings().prettyPrint(false);
        return unmask(normalizeWhitespaces(clean).body().html());
    }

    /**
     * Normalizes the whitespaces in text nodes of the specified document.
     * Normally this is done by pretty printing but I disabled it because
     * indentation done by Jsoup is pretty buggy. So I have to normalize the
     * whitespaces manually here.
     * 
     * @param doc
     *            The document to normalise whitespaces in.
     * @return The normalized document.
     */
    private static Document normalizeWhitespaces(Document doc)
    {
        for (TextNode node : doc.body().textNodes())
        {
            node.text(node.text());
        }
        return doc;
    }

    /**
     * Cleans the specified HTML code so it only contains valid and allowed
     * tags.
     * 
     * @param html
     *            The HTML to clean.
     * @return The cleaned HTML.
     */
    public static String clean(final String html)
    {
        return clean(html, Whitelist.basic());
    }

    /**
     * Masks problematic code which Jsoup doesn't handle as we need it. After
     * Jsoup did its work the string must be piped through unmask() to restore
     * the original code.
     * 
     * @param html
     *            The HTML text to mask.
     * @return The masked HTML text.
     */
    private static String mask(final String html)
    {
        return html.replace("&#125;", "@jasdoc.unicode#125;");
    }

    /**
     * Unmasks previously masked html text to restore masked code it.
     * 
     * @param html
     *            The previously masked html text.
     * @return The unmasked HTML text.
     */
    private static String unmask(final String html)
    {
        return html.replace("@jasdoc.unicode", "&");
    }

    /**
     * Strips all HTML tags from the specified string.
     * 
     * @param html
     *            The string to strip HTML tags from.
     * @return The plain text.
     */
    public static String strip(final String html)
    {
        return Jsoup.parse(html).text();
    }

    /**
     * Cleans up the specified HTML so it only contains simple text and links.
     * 
     * @param html
     *            The HTML to simplify.
     * @return The simplified HTML.
     */
    public static String simplify(final String html)
    {
        return clean(html, SIMPLE_TEXT_WITH_LINKS);
    }

    /**
     * Escapes all special HTML characters from the specified string.
     * 
     * @param s
     *            The string to escape.
     * @return The escaped string.
     */
    public static String escape(final String s)
    {
        return s.replace("<", "&lt;").replace(">", "&gt;")
            .replace("&", "&amp;").replace("\"", "&quot;");
    }

    /**
     * Returns the first sentence of the specified HTML text.
     * 
     * @param html
     *            The HTML text.
     * @return The first sentence.
     */
    public static String getFirstSentence(final String html)
    {
        final Document newDoc = Document.createShell("");
        final Element newBody = newDoc.body();
        final Document document = parse(html);
        final Element body = document.body();
        for (final Node node : body.childNodes())
        {
            if (node instanceof TextNode)
            {
                final String text = ((TextNode) node).text();
                final String[] parts = text.split("\\.(\\s+|$)", 2);
                if (parts.length == 2)
                {
                    newBody.appendText(parts[0] + ".");
                    break;
                }
            }
            newBody.appendChild(node.clone());
        }
        return newDoc.body().html().trim();
    }

    /**
     * Converts a textual email link to a real HTML link.
     * 
     * @param html
     *            The text to scan for textual email links.
     * @return The HTML code with containing the real HTML link.
     */
    public static String convertEMail(final String html)
    {
        return html.replaceAll("<([\\w.-_]+@[\\w.-_]+)>",
            "&lt;<a href=\"mailto:$1\">$1</a>&gt;");
    }
}
