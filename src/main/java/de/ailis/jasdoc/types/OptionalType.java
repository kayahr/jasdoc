/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Optional type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class OptionalType extends AbstractType
{
    /** The optional type. */
    private final AbstractType type;

    /**
     * Constructor.
     *
     * @param type
     *            The optional type. Must not be null.
     */
    public OptionalType(final AbstractType type)
    {
        if (type == null) throw new IllegalArgumentException("type is null");
        this.type = type;
    }

    /**
     * Returns the optional type.
     *
     * @return The optional type. Never null.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return this.type.toExpression() + "=";
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        return this.type.toHtml(doc) + "=";
    }
}
