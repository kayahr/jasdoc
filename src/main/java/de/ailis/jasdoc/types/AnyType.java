/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.ModelDoc;


/**
 * The "any" type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class AnyType extends AbstractType
{
    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return "*";
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        return toExpression();
    }
}
