/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import org.apache.commons.lang.builder.EqualsBuilder;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Base type for all types and factory to convert a type expression into a
 * concrete type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractType
{
    /**
     * Returns the expression of this type.
     *
     * @return The type expression.
     */
    public abstract String toExpression();

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString()
    {
        return toExpression();
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj)
    {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != getClass()) return false;
        final AbstractType other = (AbstractType) obj;
        return new EqualsBuilder().append(toExpression(),
            other.toExpression())
            .isEquals();
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public final int hashCode()
    {
        return toExpression().hashCode();
    }

    /**
     * Returns the expression of this type in HTML code. All types within the
     * expression are linked to the corresponded documentation page if possible.
     *
     * @param doc
     *            The documentation model needed to resolve the type.
     * @return The link.
     */
    public abstract String toHtml(final ModelDoc doc);
}
