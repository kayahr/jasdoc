/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.GlobalDoc;
import de.ailis.jasdoc.doc.ModelDoc;
import de.ailis.jasdoc.util.HTMLUtils;

/**
 * Class type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ClassType extends AbstractType
{
    /** The type name. */
    private final String name;

    /**
     * Constructor.
     *
     * @param name
     *            The type name. Must not be null.
     */
    public ClassType(final String name)
    {
        if (name == null) throw new IllegalArgumentException("name is null");
        this.name = name;
    }

    /**
     * Returns the type name.
     *
     * @return The type name. Never null.
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return this.name;
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        final GlobalDoc global = doc.getGlobal();
        final String baseUrl = doc.getBaseUrl();
        final ModelDoc type = global.resolve(this.name);
        if (type == null)
            return HTMLUtils.escape(getName());
        else
            return "<a href=" + baseUrl + "/" + type.getUrl() + ">"
                + HTMLUtils.escape(type.getName()) + "</a>";
    }
}
