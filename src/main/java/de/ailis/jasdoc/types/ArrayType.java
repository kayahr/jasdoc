/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Array type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class ArrayType extends AbstractType
{
    /** The array type. */
    private final AbstractType type;

    /**
     * Constructor.
     *
     * @param type
     *            The array type. Must not be null.
     */
    public ArrayType(final AbstractType type)
    {
        this.type = type;
    }

    /**
     * Returns the array type.
     *
     * @return The array type. Never null.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return "Array.<" + this.type.toExpression() + ">";
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        return "Array.&lt;" + this.type.toHtml(doc) + "&gt;";
    }
}
