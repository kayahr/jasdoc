/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;


/**
 * Primitive boolean type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class BooleanType extends AbstractPrimitiveType
{
    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return "boolean";
    }
}
