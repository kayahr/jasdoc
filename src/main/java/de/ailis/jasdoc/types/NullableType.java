/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Nullable type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class NullableType extends AbstractType
{
    /** The nullable type. */
    private final AbstractType type;

    /**
     * Constructor.
     *
     * @param type
     *            The nullable type. Must not be null.
     */
    public NullableType(final AbstractType type)
    {
        if (type == null) throw new IllegalArgumentException("type is null");
        this.type = type;
    }

    /**
     * Returns the nullable type.
     *
     * @return The nullable. Never null.
     */
    public AbstractType getType()
    {
        return this.type;
    }

    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return "?" + this.type.toExpression();
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        return "?" + this.type.toHtml(doc);
    }
}
