/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Union type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class UnionType extends AbstractType
{
    /** The types in this union. */
    private final List<AbstractType> types;

    /**
     * Constructor.
     *
     * @param types
     *            The union types. Must not be null.
     */
    public UnionType(final Collection<AbstractType> types)
    {
        if (types == null)
            throw new IllegalArgumentException("types is null");
        this.types = Collections.unmodifiableList(
            new ArrayList<AbstractType>(types));
    }

    /**
     * Returns the union types.
     *
     * @return The union types. Never null.
     */
    public List<AbstractType> getTypes()
    {
        return this.types;
    }

    /**
     * @see de.ailis.jasdoc.types.AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        final StringBuilder builder = new StringBuilder();
        for (final AbstractType type: this.types)
        {
            if (builder.length() != 0) builder.append('|');
            builder.append(type.toExpression());
        }
        return builder.toString();
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        final StringBuilder builder = new StringBuilder();
        for (final AbstractType type: this.types)
        {
            if (builder.length() != 0) builder.append('|');
            builder.append(type.toHtml(doc));
        }
        return builder.toString();
    }
}
