/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Map type.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class MapType extends AbstractType
{
    /** The key type. */
    private final AbstractType keyType;

    /** The value type. */
    private final AbstractType valueType;

    /**
     * Constructor.
     *
     * @param keyType
     *            The key type. Must not be null.
     * @param valueType
     *            The value type. Must not be null.
     */
    public MapType(final AbstractType keyType, final AbstractType valueType)
    {
        if (keyType == null)
            throw new IllegalArgumentException("keyType is null");
        if (valueType == null)
            throw new IllegalArgumentException("valueType is null");
        this.keyType = keyType;
        this.valueType = valueType;
    }

    /**
     * Returns the key type.
     *
     * @return The key type. Never null.
     */
    public AbstractType getKeyType()
    {
        return this.keyType;
    }

    /**
     * Returns the value type.
     *
     * @return The value type. Never null.
     */
    public AbstractType getValueType()
    {
        return this.valueType;
    }

    /**
     * @see AbstractType#toExpression()
     */
    @Override
    public String toExpression()
    {
        return "Object.<" + this.keyType.toExpression() + ","
            + this.valueType.toExpression() + ">";
    }

    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public String toHtml(final ModelDoc doc)
    {
        return "Object.&lt;" + this.keyType.toHtml(doc) + ","
            + this.valueType.toHtml(doc) + "&gt;";
    }
}
