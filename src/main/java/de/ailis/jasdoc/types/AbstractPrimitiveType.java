/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.types;

import de.ailis.jasdoc.doc.ModelDoc;

/**
 * Base class for primitive types (string, number, boolean).
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractPrimitiveType extends AbstractType
{
    /**
     * @see AbstractType#toHtml(ModelDoc)
     */
    @Override
    public final String toHtml(final ModelDoc doc)
    {
        return toExpression();
    }
}
