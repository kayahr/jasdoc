/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.html;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import de.ailis.jasdoc.Main;
import de.ailis.jasdoc.doc.ClassDoc;
import de.ailis.jasdoc.doc.Configuration;
import de.ailis.jasdoc.doc.EnumDoc;
import de.ailis.jasdoc.doc.GlobalDoc;
import de.ailis.jasdoc.doc.IndexDoc;
import de.ailis.jasdoc.doc.InterfaceDoc;
import de.ailis.jasdoc.doc.ModelDoc;
import de.ailis.jasdoc.doc.NamespaceDoc;
import de.ailis.jasdoc.doc.SymbolsDoc;
import de.ailis.jasdoc.freemarker.TemplateFactory;
import de.ailis.jasdoc.util.ExtFile;
import de.ailis.jasdoc.util.IOUtils;
import freemarker.template.Template;

/**
 * Writes the HTML output.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public class HtmlWriter
{
    /** The configuration. */
    private final Configuration config;

    /** The template factory. */
    private final TemplateFactory templateFactory;

    /**
     * Constructor.
     *
     * @param configuration
     *            The configuration.
     */
    public HtmlWriter(final Configuration configuration)
    {
        if (configuration == null)
            throw new IllegalArgumentException("configuration is null");
        this.config = configuration;
        this.templateFactory = new TemplateFactory(this.config.getTheme());
    }

    /**
     * Writes the HTML output for the specified global namespace.
     *
     * @param global
     *            The global namespace to output as HTML.
     * @throws IOException
     *             If writing the HTML output fails.
     */
    public final void writeDoc(final GlobalDoc global) throws IOException
    {
        // Copy all static files to output directory
        final String theme = this.config.getTheme();
        URL url = Main.class.getResource("themes/" + theme);
        if (url == null)
            url = Main.class.getResource("/themes/" + theme);
        if (url == null)
            throw new IOException("Theme '" + theme + "' not found");
        final ExtFile themeDir = new ExtFile(url);
        copyStaticFiles(themeDir, this.config.getOutputDirectory());

        // Generate global namespace doc
        writeNamespace(global);

        // Generate deprecation page
        if (global.isDeprecatedPresent())
            write("deprecated-list.ftl", global, "deprecated-list.html");

        // Write the index.
        writeIndex(global);
    }

    /**
     * Writes the index files.
     *
     * @param global
     *            The global documentation.
     * @throws IOException
     *             If writing the HTML output fails.
     */
    private void writeIndex(final GlobalDoc global) throws IOException
    {
        final IndexDoc index = global.getIndex();
        for (final Character c : index.getLetters())
        {
            final SymbolsDoc symbols = index.getSymbols(c);
            write("index-files.ftl", symbols);
        }
    }

    /**
     * Recursively writes all the files for the specified namespace.
     *
     * @param namespace
     *            The namespace to write.
     * @throws IOException
     *             If writing the HTML output fails.
     */
    private void writeNamespace(final NamespaceDoc namespace)
        throws IOException
    {
        // Write the HTML file for the current namespace
        write("namespace.ftl", namespace);

        // Write the HTML files for the interfaces in the current namespace
        for (final InterfaceDoc intf : namespace.getInterfaces())
            write("interface.ftl", intf);

        // Write the HTML files for the classes in the current namespace
        for (final ClassDoc cls : namespace.getClasses())
            write("class.ftl", cls);

        // Write the HTML files for the enums in the current namespace
        for (final EnumDoc e : namespace.getEnums())
            write("enum.ftl", e);

        // Process all sub namespace
        for (final NamespaceDoc subNamespace : namespace.getNamespaces())
            writeNamespace(subNamespace);
    }

    /**
     * Copies all static files from the specified source directory to the
     * specified destination directory. This is done recursively.
     *
     * @param source
     *            The source directory.
     * @param dest
     *            The destination directory
     * @throws IOException
     *             If copy fails.
     */
    private void copyStaticFiles(final ExtFile source, final File dest)
        throws IOException
    {
        dest.mkdirs();
        for (final ExtFile file : source.listFiles())
        {
            if (file.isDirectory())
                copyStaticFiles(file, new File(dest, file.getName()));
            else
            {
                // Ignore freemarker templates
                if (file.getName().toLowerCase().endsWith(".ftl")) continue;

                // Copy static file
                IOUtils.copy(file, new File(dest, file.getName()));
            }
        }
    }

    /**
     * Writes a single HTML file.
     *
     * @param templateName
     *            The template file to use.
     * @param model
     *            The model.
     * @throws IOException
     *             If writing the HTML output fails.
     */
    private void write(final String templateName, final ModelDoc model)
        throws IOException
    {
        write(templateName, model, model.getUrl());
    }

    /**
     * Writes a single HTML file.
     *
     * @param templateName
     *            The template file to use.
     * @param model
     *            The model.
     * @param destFilename
     *            The destination filename relative to the output directory.
     * @throws IOException
     *             If writing the HTML output fails.
     */
    private void write(final String templateName, final ModelDoc model,
        final String destFilename) throws IOException
    {
        final File output = new File(this.config.getOutputDirectory(),
            destFilename);
        final Template template =
            this.templateFactory.getTemplate(templateName);
        final String content =
            this.templateFactory.processTemplate(template, model);
        output.getParentFile().mkdirs();
        IOUtils.writeText(output, content, "UTF-8");
    }

}
