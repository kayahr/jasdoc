/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.logging;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Log handler which dumps logs up to INFO level to STDOUT and everything else
 * to STDERR.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public class LogHandler extends Handler
{
    /** The number of warnings occurred. */
    private int warnings;

    /** The number of errors occurred. */
    private int errors;

    /**
     * Constructor.
     */
    public LogHandler()
    {
        super();
        setFilter(null);
    }

    /**
     * @see Handler#publish(LogRecord)
     */
    @Override
    public final void publish(final LogRecord record)
    {
        if (record.getLevel().intValue() < getLevel().intValue()) return;

        final int level = record.getLevel().intValue();
        final String message = this.getFormatter().format(record);
        if (level > Level.INFO.intValue())
        {
            if (level > Level.WARNING.intValue())
                this.errors += 1;
            else
                this.warnings += 1;
            System.err.print(message);
        }
        else
            System.out.print(message);
    }

    /**
     * @see Handler#flush()
     */
    @Override
    public final void flush()
    {
        System.out.flush();
        System.err.flush();
    }

    /**
     * @see Handler#close()
     */
    @Override
    public final void close()
    {
        flush();
        if (this.errors == 1)
            System.err.println("1 error");
        else if (this.errors > 0) System.err.println(this.errors + " errors");
        if (this.warnings == 1)
            System.err.println("1 warning");
        else if (this.warnings > 0)
            System.err.println(this.warnings + " warnings");
    }
}
