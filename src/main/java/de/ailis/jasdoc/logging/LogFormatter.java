/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * A log formatter with nice readable output optimized for use in a command line
 * program.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public class LogFormatter extends Formatter
{
    /** Debug mode. */
    private final boolean debug;

    /**
     * Constructor.
     *
     * @param debug
     *            True to enable debug mode, false to disable it.
     */
    public LogFormatter(final boolean debug)
    {
        this.debug = debug;
    }

    /**
     * @see Formatter#format(LogRecord)
     */
    @Override
    public final String format(final LogRecord record)
    {
        final StringBuffer buffer = new StringBuffer();

        if (this.debug)
        {
            final Date date = new Date(record.getMillis());
            buffer.append(new SimpleDateFormat("HH:mm:ss.SSS").format(date));
            buffer.append(" ");
            final String level = record.getLevel().getName();
            buffer.append(level);
            final int max = 5 - level.length();
            for (int i = 0; i < max; i++)
            {
                buffer.append(' ');
            }
            buffer.append(" [");
            final String[] parts = record.getLoggerName().split("\\.");
            buffer.append(parts[parts.length - 1]);
            buffer.append("] ");
        }

        if (record.getLevel().intValue() >= Level.SEVERE.intValue())
            buffer.append("ERROR: ");
        else if (record.getLevel().intValue() >= Level.WARNING.intValue())
            buffer.append("Warning: ");

        buffer.append(record.getMessage());

        final Throwable thrown = record.getThrown();
        if (thrown != null && this.debug)
        {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            record.getThrown().printStackTrace(pw);
            pw.close();
            buffer.append("\n");
            buffer.append(sw.toString());
        }
        buffer.append("\n");

        return buffer.toString();
    }
}
