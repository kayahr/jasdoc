/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.freemarker;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

import de.ailis.jasdoc.Main;
import freemarker.cache.TemplateLoader;

/**
 * Template loader which loads templates from a specific theme directory and
 * falls back to the default theme if the template was not found.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public class ThemeTemplateLoader implements TemplateLoader
{
    /** The theme. */
    private final String theme;

    /**
     * Constructor.
     *
     * @param theme
     *            The theme.
     */
    public ThemeTemplateLoader(final String theme)
    {
        this.theme = theme;
    }

    /**
     * @see TemplateLoader#findTemplateSource(String)
     */
    @Override
    public final Object findTemplateSource(final String name) throws IOException
    {
        // Try to load template from built-in theme directory
        URL url = Main.class.getResource("themes/" + this.theme + "/" + name);

        // If not found the try to load it from custom theme directory
        if (url == null)
            url = Main.class.getResource("/themes/" + this.theme + "/" + name);

        // If still not found then try to load it from the default theme
        if (url == null && !"default".equals(this.theme))
            url = Main.class.getResource("themes/default/" + name);

        if (url == null) return null;
        return url.openStream();
    }

    /**
     * @see TemplateLoader#getLastModified(Object)
     */
    @Override
    public final long getLastModified(final Object templateSource)
    {
        return -1;
    }

    /**
     * @see TemplateLoader#getReader(Object, String)
     */
    @Override
    public final Reader getReader(final Object templateSource,
        final String encoding) throws IOException
    {
        return new InputStreamReader((InputStream) templateSource, encoding);
    }

    /**
     * @see TemplateLoader#closeTemplateSource(Object)
     */
    @Override
    public final void closeTemplateSource(final Object templateSource)
        throws IOException
    {
        ((InputStream) templateSource).close();
    }
}
