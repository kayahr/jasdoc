/*
 * Copyright (C) 2010-2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */
package de.ailis.jasdoc.freemarker;

import java.util.Map;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateModelException;

/**
 * Base class for template directives.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public abstract class AbstractDirective implements TemplateDirectiveModel
{
    /**
     * Returns a parameter as string. Returns null if parameter was not found.
     *
     * @param params
     *            The parameters
     * @param name
     *            The parameter name
     * @return The parameter value or null if not set
     */
    protected final String getString(final Map<?, ?> params, final String name)
    {
        final SimpleScalar value = (SimpleScalar) params.get(name);
        if (value == null) return null;
        return value.getAsString();
    }

    /**
     * Returns a parameter as string. Throws exception if parameter is not set
     *
     * @param params
     *            The parameters
     * @param name
     *            The parameter name
     * @return The parameter value
     * @throws TemplateModelException
     *             If parameter is not set
     */
    protected final String getRequiredString(final Map<?, ?> params,
        final String name) throws TemplateModelException
    {
        final String value = getString(params, name);
        if (value == null)
            throw new TemplateModelException("Param " + name + " is not set");
        return value;
    }
}
