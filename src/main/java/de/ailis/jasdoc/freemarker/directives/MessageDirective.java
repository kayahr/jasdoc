/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.freemarker.directives;

import java.io.IOException;
import java.util.Map;

import de.ailis.jasdoc.freemarker.AbstractDirective;
import de.ailis.jasdoc.util.I18N;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * Message directive for freemarker templates.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public class MessageDirective extends AbstractDirective
{
    /**
     * @see freemarker.template.TemplateDirectiveModel#execute(
     *      freemarker.core.Environment, java.util.Map,
     *      freemarker.template.TemplateModel[],
     *      freemarker.template.TemplateDirectiveBody)
     */
    @Override
    public final void execute(final Environment env, final Map params,
        final TemplateModel[] loopVars, final TemplateDirectiveBody body)
        throws TemplateException, IOException
    {
        if (body != null) throw new TemplateModelException("No body allowed");
        env.getOut().append(I18N.getString(getRequiredString(params, "key")));
    }
}
