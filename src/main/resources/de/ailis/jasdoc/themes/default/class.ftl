[#include "common.ftl" /]
[#assign superMethods = inheritedMethods /]
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    [#include "head.ftl" /]
  </head>
  <body>
    [#include "header.ftl" /]
    
    <nav id="subNav">
      <ul>
        [#if constructor?has_content]
          <li><a href="#constructor">Constructor</a></li>
        [/#if]
        [#if fields?has_content]
          <li><a href="#field_summary">Field Summary</a></li>
        [/#if]
        [#if methods?has_content]
          <li><a href="#method_summary">Method Summary</a></li>
        [/#if]
        [#if superMethods?has_content]
          <li><a href="#inherited_methods">Inherited Methods</a></li>
        [/#if]
        [#if fields?has_content]
          <li><a href="#field_detail">Field Detail</a></li>
        [/#if]
        [#if methods?has_content]
          <li><a href="#method_detail">Method Detail</a></li>
        [/#if]
      </ul>
    </nav>
    
    <div id="body">
  
      <div class="namespace">
        [@namespaceLinks namespace /]
      </div>
      
      <h2>Class ${name?html}</h2>
           
      [#if superClasses?has_content]
        [#if multipleInheritance]
          <dl>
            <dt>Superclasses:</dt>
            <dd>
              [#list superClasses as superClass]
                [@docLink superClass]${superClass.name?html}[/@docLink][#if superClass_has_next], [/#if]
              [/#list]          
            </dd>
          </dl>
        [#else]
          <div class="classTree">
            [@classTree this /]
          </div>        
        [/#if]
      [/#if]
      
      [#if interfaces?has_content]
        <dl>
          <dt>Implemented Interfaces:</dt>
          <dd>
            [#list interfaces as interface]
              [@docLink interface]${interface.name?html}[/@docLink][#if interface_has_next], [/#if]
            [/#list]          
          </dd>
        </dl>
      [/#if]
      
      [#if subClasses?has_content]
        <dl>
          <dt>Subclasses:</dt>
          <dd>
            [#list subClasses as subClass]
              [@docLink subClass]${subClass.name?html}[/@docLink][#if subClass_has_next], [/#if]
            [/#list]          
          </dd>
        </dl>
      [/#if]
      
      <hr />
      
      <div class="classSignature">
        <div class="class">
          public class <span class="className">${name?html}</span>
        </div>
        [#if superClasses?has_content]
          <div class="extends">
            extends
            [#list superClasses as superClass]
              [@docLink superClass]${superClass.name?html}[/@docLink][#if superClass_has_next], [/#if]
            [/#list]          
          </div>
        [/#if]
        [#if interfaces?has_content]
          <div class="implements">
            implements
            [#list interfaces as interface]
              [@docLink interface]${interface.name?html}[/@docLink][#if interface_has_next], [/#if]
            [/#list]          
          </div>
        [/#if]
      </div>
      
      <div class="description">
        [@deprecatedDetail this /]
        ${description}
      </div>
            
      [@tags this /]
      
      [#if constructor?has_content]
        <section id="constructor">
          <h3>Constructor</h3>
          [@constructorDetail constructor /] 
        </section>
      [/#if]      

      [#if fields?has_content]
        <section id="field_summary">
          <h3>Field Summary</h3>
          <table summary="A summary of all fields in the current class.">
            <tr>
              <th class="modifier type">Modifier and Type</th>
              <th class="field description">Field and Description</th>
            </tr>
            [#list fields as field]
              [@fieldSummary field /]
            [/#list]
          </table>
        </section>
      [/#if]
      
      [#if methods?has_content]
        <section id="method_summary">
          <h3>Method Summary</h3>
          <table summary="A summary of all methods in the current class.">
            <tr>
              <th class="modifier type">Modifier and Type</th>
              <th class="method description">Method and Description</th>
            </tr>
            [#list methods as method]
              [@methodSummary method /]
            [/#list]
          </table>
        </section>
      [/#if]
      
      [#if superMethods?has_content]
        <section id="inherited_methods">
          <h3>Inherited Methods</h3>
          <div class="inherited_methods">
            [#list superMethods as entry]
              [#assign class = entry.key /]
              <h4>From [@docLink class]${class.qualifiedName}[/@docLink]</h4>
              <p>
                [#list entry.value as method]
                  [@docLink method]${method.name}[/@docLink][#if method_has_next],[/#if]
                [/#list]      
              </p> 
            [/#list]
          </div>
        </section>
      [/#if]
      
      [#if fields?has_content]
        <section id="field_detail">
          <h3>Field Detail</h3>
          [#list fields as field]
            [@fieldDetail field /]
          [/#list]
        </section>
      [/#if]

      [#if methods?has_content]
        <section id="method_detail">
          <h3>Method Detail</h3>
          [#list methods as method]
            [@methodDetail method /]
          [/#list]
        </section>
      [/#if]
          
    </div>

    [#include "footer.ftl" /]    
  </body>
</html>