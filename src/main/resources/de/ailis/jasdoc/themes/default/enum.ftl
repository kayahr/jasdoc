[#include "common.ftl" /]
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    [#include "head.ftl" /]
  </head>
  <body>
    [#include "header.ftl" /]
    
    <nav id="subNav">
      <ul>
        [#if constants?has_content]
          <li><a href="#enum_constant_summary">Enum Constant Summary</a></li>
          <li><a href="#enum_constant_detail">Enum Constant Detail</a></li>
        [/#if]
      </ul>
    </nav>
    
    <div id="body">
  
      <div class="namespace">
        [@namespaceLinks namespace /]
      </div>
      
      <h2>Enum ${name?html}</h2>
      
      <div class="description">
        [@deprecatedDetail this /]
        ${description}
      </div>
            
      [@tags this /]
      
      [#if constants?has_content]
        <section id="enum_constant_summary">
          <h3>Enum Constant Summary</h3>
          <table summary="A summary of all constants in the current enum.">
            <tr>
              <th class="field description">Enum constant and Description</th>
            </tr>
            [#list constants as constant]
              [@enumConstantSummary constant /]
            [/#list]
          </table>
        </section>
        
        <section id="enum_constant_detail">
          <h3>Enum Constant Detail</h3>
          [#list constants as constant]
            [@enumConstantDetail constant /]
          [/#list]
        </section>
      [/#if]

    </div>

    [#include "footer.ftl" /]    
  </body>
</html>