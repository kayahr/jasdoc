[#include "common.ftl" /]
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    [#include "head.ftl" /]
  </head>
  <body>
    [#include "header.ftl" /]
    
    <nav id="subNav">
      <ul>
        [#list index.symbols as symbols]
          <li>[@docLink symbols]${symbols.name}[/@docLink]</li>
        [/#list]
      </ul>
    </nav>
    
    <div id="body">
  
      <h1>${name}</h1>
      
      <dl class="index">
        [#list symbols as symbol]
          <dt>
            <span class="symbol">
              [@docLink symbol]${symbol.name?html}[/@docLink]
            </span>
            -
            [#switch symbol.elementType]
              [#case "NAMESPACE"]
                Namespace
                [#if symbol.superNamespace != global]
                  in namespace
                  [@docLink symbol.superNamespace]${symbol.superNamespace.qualifiedName?html}[/@docLink]
                [/#if]
                [#break]

              [#case "VARIABLE"]
                Variable
                [#if symbol.namespace != global]
                  in namespace
                  [@docLink symbol.namespace]${symbol.namespace.qualifiedName?html}[/@docLink]
                [/#if]
                [#break]

              [#case "FUNCTION"]
                Function
                [#if symbol.namespace != global]
                  in namespace
                  [@docLink symbol.namespace]${symbol.namespace.qualifiedName?html}[/@docLink]
                [/#if]
                [#break]
                
              [#case "CLASS"]
                Class
                [#if symbol.namespace != global]
                  in namespace
                  [@docLink symbol.namespace]${symbol.namespace.qualifiedName?html}[/@docLink]
                [/#if]                
                [#break]
                
              [#case "ENUM"]
                Enum
                [#if symbol.namespace != global]
                  in namespace
                  [@docLink symbol.namespace]${symbol.namespace.qualifiedName?html}[/@docLink]
                [/#if]                
                [#break]
                
              [#case "INTERFACE"]
                Interface
                [#if symbol.namespace != global]
                  in namespace
                  [@docLink symbol.namespace]${symbol.namespace.qualifiedName?html}[/@docLink]
                [/#if]                
                [#break]

              [#case "FIELD"]
                [#if symbol.modifier.static]
                  Static field
                [#else]
                  Field
                [/#if] 
                in class 
                [@docLink symbol.declaringType]${symbol.declaringType.qualifiedName?html}[/@docLink]
                [#break]

              [#case "METHOD"]
                [#if symbol.modifier.static]
                  Static method
                [#else]
                  Method
                [/#if] 
                in
                [#if symbol.declaringType.elementType == "CLASS"]
                  class
                [#else]
                  interface
                [/#if]
                [@docLink symbol.declaringType]${symbol.declaringType.qualifiedName?html}[/@docLink]
                [#break]
                
              [#default]
                TODO for Type ${symbol.elementType}
            [/#switch]
          </dt>
          <dd>
            [@deprecatedSummary symbol /]
            ${symbol.shortDescription}
          </dd>
        [/#list]
      </dl>

    </div>

    [#include "footer.ftl" /]
  </body>
</html>