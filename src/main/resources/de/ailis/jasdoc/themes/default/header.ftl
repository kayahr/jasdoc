<header>
  <nav id="mainNav">
    <ul>
      <li><a href="${baseUrl}index.html">Global</a></li>
      [#--<li><a href="${baseUrl}tree.html">Tree</a></li>--]
      [#if global.deprecatedPresent]
        <li><a href="${baseUrl}deprecated-list.html">Deprecated</a></li>
      [/#if]
      [#if global.index.present]
        <li>[@docLink global.index]Index[/@docLink]</li>
      [/#if]
    </ul>
  </nav>
</header>
