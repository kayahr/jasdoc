[#include "common.ftl" /]
[#assign superMethods = inheritedMethods /]
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    [#include "head.ftl" /]
  </head>
  <body>
    [#include "header.ftl" /]
    
    <nav id="subNav">
      <ul>
        [#if methods?has_content]
          <li><a href="#method_summary">Method Summary</a></li>
        [/#if]
        [#if superMethods?has_content]
          <li><a href="#inherited_methods">Inherited Methods</a></li>
        [/#if]
        [#if methods?has_content]
          <li><a href="#method_detail">Method Detail</a></li>
        [/#if]
      </ul>
    </nav>
    
    <div id="body">
  
      <div class="namespace">
        [@namespaceLinks namespace /]
      </div>
      
      <h2>Interface ${name?html}</h2>
      
      [#if superInterfaces?has_content]
        <dl>
          <dt>Superinterfaces:</dt>
          <dd>
            [#list superInterfaces as superIntf]
              [@docLink superIntf]${superIntf.name?html}[/@docLink][#if superIntf_has_next], [/#if]
            [/#list]          
          </dd>
        </dl>
      [/#if]
      
      [#if subInterfaces?has_content]
        <dl>
          <dt>Subinterfaces:</dt>
          <dd>
            [#list subInterfaces as subClass]
              [@docLink subClass]${subClass.name?html}[/@docLink][#if subClass_has_next], [/#if]
            [/#list]          
          </dd>
        </dl>
      [/#if]
      
      [#if implementingClasses?has_content]
        <dl>
          <dt>Implementing Classes:</dt>
          <dd>
            [#list implementingClasses as implClass]
              [@docLink implClass]${implClass.name?html}[/@docLink][#if implClass_has_next], [/#if]
            [/#list]          
          </dd>
        </dl>
      [/#if]
      
      <hr />
      
      <div class="interfaceSignature">
        <div class="interface">
          public interface <span class="interfaceName">${name?html}</span>
        </div>
        [#if superInterfaces?has_content]
          <div class="extends">
            extends
            [#list superInterfaces as superInterface]
              [@docLink superInterface]${superInterface.name?html}[/@docLink][#if superInterface_has_next], [/#if]
            [/#list]          
          </div>
        [/#if]
      </div>
      
      <div class="description">
        [@deprecatedDetail this /]
        ${description}
      </div>
            
      [@tags this /]
      
      [#if methods?has_content]
        <section id="method_summary">
          <h3>Method Summary</h3>
          <table summary="A summary of all methods in the current class.">
            <tr>
              <th class="modifier type">Modifier and Type</th>
              <th class="method description">Method and Description</th>
            </tr>
            [#list methods as method]
              [@methodSummary method /]
            [/#list]
          </table>
        </section>
      [/#if]
      
      [#if superMethods?has_content]
        <section id="inherited_methods">
          <h3>Inherited Methods</h3>
          <div class="inherited_methods">
            [#list superMethods as entry]
              [#assign interface = entry.key /]
              <h4>From [@docLink interface]${interface.qualifiedName}[/@docLink]</h4>
              <p>
                [#list entry.value as method]
                  [@docLink method]${method.name}[/@docLink][#if method_has_next],[/#if]
                [/#list]      
              </p> 
            [/#list]
          </div>
        </section>
      [/#if]
      
      [#if methods?has_content]
        <section id="method_detail">
          <h3>Method Detail</h3>
          [#list methods as method]
            [@methodDetail method /]
          [/#list]
        </section>
      [/#if]
          
    </div>

    [#include "footer.ftl" /]    
  </body>
</html>