[#include "common.ftl" /]
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    [#include "head.ftl" /]
  </head>
  <body>
    [#include "header.ftl" /]
    
    <nav id="subNav">
      <ul>
        [#if deprecatedNamespaces?has_content]
          <li><a href="#namespaces">Namespaces</a></li>
        [/#if]
        [#if deprecatedInterfaces?has_content]
          <li><a href="#interfaces">Interfaces</a></li>
        [/#if]
        [#if deprecatedClasses?has_content]
          <li><a href="#classes">Classes</a></li>
        [/#if]
        [#if deprecatedEnums?has_content]
          <li><a href="#enums">Enums</a></li>
        [/#if]
        [#if deprecatedVariables?has_content]
          <li><a href="#variables">Variables</a></li>
        [/#if]
        [#if deprecatedFunctions?has_content]
          <li><a href="#functions">Functions</a></li>
        [/#if]
        [#if deprecatedMethods?has_content]
          <li><a href="#methods">Methods</a></li>
        [/#if]
        [#if deprecatedFields?has_content]
          <li><a href="#fields">Fields</a></li>
        [/#if]
      </ul>
    </nav>
    
    <div id="body" class="deprecated-page">
  
      <h1>Deprecated API</h1>
      
      [#if deprecatedNamespaces?has_content]
        <section id="namespaces">
          <h3>Deprecated Namespaces</h3>
          <table>
            <tr>
              <th class="description">Namespace and Description</th>
            </tr>
            [#list deprecatedNamespaces as namespace]
              <tr>
                <td class="description">
                  [@docLink namespace]${namespace.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${namespace.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if deprecatedInterfaces?has_content]
        <section id="interfaces">
          <h3>Deprecated Interfaces</h3>
          <table>
            <tr>
              <th class="description">Interface and Description</th>
            </tr>
            [#list deprecatedInterfaces as interface]
              <tr>
                <td class="description">
                  [@docLink interface]${interface.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${interface.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if deprecatedClasses?has_content]
        <section id="classes">
          <h3>Deprecated Classes</h3>
          <table>
            <tr>
              <th class="description">Class and Description</th>
            </tr>
            [#list deprecatedClasses as class]
              <tr>
                <td class="description">
                  [@docLink class]${class.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${class.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if deprecatedEnums?has_content]
        <section id="enums">
          <h3>Deprecated Enums</h3>
          <table>
            <tr>
              <th class="description">Enum and Description</th>
            </tr>
            [#list deprecatedEnums as enum]
              <tr>
                <td class="description">
                  [@docLink enum]${enum.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${enum.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if deprecatedVariables?has_content]
        <section id="variables">
          <h3>Deprecated Variables</h3>
          <table>
            <tr>
              <th class="description">Variable and Description</th>
            </tr>
            [#list deprecatedVariables as variable]
              <tr>
                <td class="description">
                  [@docLink variable]${variable.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${variable.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if deprecatedFunctions?has_content]
        <section id="functions">
          <h3>Deprecated Functions</h3>
          <table>
            <tr>
              <th class="description">Function and Description</th>
            </tr>
            [#list deprecatedFunctions as function]
              <tr>
                <td class="description">
                  [@docLink function]${function.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${function.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if deprecatedMethods?has_content]
        <section id="methods">
          <h3>Deprecated Methods</h3>
          <table>
            <tr>
              <th class="description">Method and Description</th>
            </tr>
            [#list deprecatedMethods as method]
              <tr>
                <td class="description">
                  [@docLink method]${method.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${method.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]
    
      [#if deprecatedFields?has_content]
        <section id="fields">
          <h3>Deprecated Fields</h3>
          <table>
            <tr>
              <th class="description">Field and Description</th>
            </tr>
            [#list deprecatedFields as field]
              <tr>
                <td class="description">
                  [@docLink field]${field.qualifiedName?html}[/@docLink]
                  <div class="deprecated">
                    ${field.shortDeprecation}
                  </div>
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

    </div>

    [#include "footer.ftl" /]
  </body>
</html>