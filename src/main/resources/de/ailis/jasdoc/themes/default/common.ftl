[#--
  - Common macros used in documentation pages.
  - Copyright (C) 2012 Klaus Reimer <k@ailis.de>
  - See LICENSE.md for licensing information.
  --]
  
[#--
  - Prints the link to a specific documentation page.
  -
  - @param doc
  -            The documentation model to link to. 
  --]
[#macro docLink doc]
  [#compress]
    <a href="${baseUrl}${doc.url?html}">[#nested /]</a>
  [/#compress]
[/#macro]

[#--
  - Prints a fully linked type.
  -
  - @param {JsType} type
  -            The type to print.
  --]
[#macro typeLink type]${type.toHtml(this)}[/#macro]

[#--
  - Prints a list of parameters.
  -
  - @param {Array.<ParamDoc>} params
  -            The parameters to print.
  --]
[#macro params params]
  [#compress]
    [#list params as param]
      [@typeLink param.type /] 
      ${param.name?html}[#if param_has_next], [/#if]
    [/#list]
  [/#compress]
[/#macro]

[#--
  - Prints a qualified namespace where all super namespaces are linked to
  - their documentation pages.
  -
  - @param {NamespaceDoc} namespace
  -            The namespace to print
  --]
[#macro namespaceLinks namespace]
  [#compress]
    [#if namespace.superNamespace?has_content && namespace.superNamespace.name?has_content]
      [@namespaceLinks namespace.superNamespace /].
    [/#if]
  [/#compress]
  [#compress]
    [#if namespace == this ]
      ${namespace.name?html}
    [#else]
      <a href="${baseUrl}${namespace.url?html}">${namespace.name?html}</a>
    [/#if]
  [/#compress]
[/#macro]

[#--
  - Prints the author of the specified model.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro author doc]
  [#if doc.authors?has_content]
    <dt>Author[#if doc.authors?size > 1]s[/#if]</dt>
    <dd>
      [#list doc.authors as author]
        ${author}[#if author_has_next], [/#if]
      [/#list]
    </dd>
  [/#if]
[/#macro]

[#--
  - Prints the since-texts of the specified model.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro since doc]
  [#if doc.since?has_content]
    <dt>Since</dt>
    <dd>
      [#list doc.since as since]
        ${since}[#if since_has_next], [/#if]
      [/#list]
    </dd>
  [/#if]
[/#macro]

[#--
  - Prints the versions of the specified model.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro version doc]
  [#if doc.versions?has_content]
    <dt>Version</dt>
    <dd>
      [#list doc.versions as version]
        ${version}[#if version_has_next], [/#if]
      [/#list]
    </dd>
  [/#if]
[/#macro]

[#--
  - Prints the see also references of the specified model.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro seeAlso doc]
  [#if doc.references?has_content]
    <dt>See Also</dt>
    <dd>
      [#list doc.references as reference]
        ${reference}[#if reference_has_next], [/#if]
      [/#list]
    </dd>
  [/#if]
[/#macro]

[#--
  - Prints the additional tags of the specified documentation model.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro tags doc]
  <dl class="tags">
    [@author doc /]
    [@version doc /]
    [@since doc /]
    [@seeAlso doc /]
  </dl>
[/#macro]

[#--
  - Prints the detailed deprecation description.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro deprecatedDetail doc]
  [#if doc.deprecated]
    <div class="deprecated">${doc.deprecation}</div>
  [/#if]
[/#macro]

[#--
  - Prints the detailed deprecation description.
  -
  - @param {ModelDoc} doc
  -            The documentation model.
  --]
[#macro deprecatedSummary doc]
  [#if doc.deprecated]
    <div class="deprecated">${doc.shortDeprecation}</div>
  [/#if]
[/#macro]

[#--
  - Prints the summary of the specified variable
  -
  - @param {VariableDoc} variable
  -            The variable model.
  --]
[#macro variableSummary variable]
  <tr>
    <td class="type">
      <code>
        [#if variable.modifier.final]final[/#if]
        [#if variable.modifier.const]const[/#if]      
        [@typeLink variable.type /]
      </code>
    </td>
    <td class="variable description">
      <code>
        [@docLink variable]${variable.name?html}[/@docLink]
      </code>
      <br />
      [@deprecatedSummary variable /]
      ${variable.shortDescription}
    </td>
  </tr>
[/#macro]

[#--
  - Prints the detail information of the specified variable
  -
  - @param {VariableDoc} variable
  -            The variable model.
  --]
[#macro variableDetail variable]
  <div id="${variable.name?html}" class="detail">
    <h4>${variable.name?html}</h4>
    <code class="syntax">
      [#if variable.modifier.final]final[/#if]
      [#if variable.modifier.const]const[/#if]
      <span class="type">[@typeLink variable.type /]</span>
      <span class="name">${variable.name?html}</span>
    </code>
    <div class="description">
      [@deprecatedDetail variable /]
      ${variable.description}
    </div>
    [@tags variable /]
  </div>
[/#macro]

[#--
  - Prints the summary of the specified function.
  -
  - @param {FunctionDoc} function
  -            The function model.
  --]
[#macro functionSummary function]
  <tr>
    <td class="modifier type">
      <code>
        [#if function.result?has_content]
          [@typeLink function.result.type /]
        [#else]
          void
        [/#if]
      </code>
    </td>
    <td class="function description">
      <code>
        <a href="#${function.name?html}">${function.name?html}</a>([@params params=function.parameters /])
      </code>
      <br />
      [@deprecatedSummary function /]      
      ${function.shortDescription}
    </td>
  </tr>
[/#macro]

[#--
  - Prints the detail information of the specified function.
  -
  - @param {FunctionDoc} function
  -            The function model.
  --]
[#macro functionDetail function]
  <div id="${function.name?html}" class="detail">
    <h4>${function.name?html}</h4>
    <code class="syntax">
      <span class="returnType">
        [#if function.result?has_content]
          [@typeLink function.result.type /]
        [#else]
          void
        [/#if]
      </span>
      <span class="name">${function.name?html}</span><span class="parameters">([@params params=function.parameters /])</span>
    </code>
    <div class="description">
      [@deprecatedDetail function /]
      ${function.description}
    </div>
    [#if function.parameters?has_content]
      <h5>Parameters</h5>
      <dl class="params">
        [#list function.parameters as param]
          <dt><code>${param.name?html}</code></dt>
          <dd>${param.description}</dd>
        [/#list]
      </dl>              
    [/#if]
    [#if function.result?has_content]
      <h5>Returns</h5>
      <dl class="return">
        <dd>${function.result.description}</dd>
      </dl>              
    [/#if]
    [@tags function /]
  </div>
[/#macro]

[#--
  - Prints the summary of the specified method.
  -
  - @param {MethodDoc} method
  -            The method model.
  --]
[#macro methodSummary method]
  <tr>
    <td class="modifier type">
      <code>
        [#if method.modifier.private]private[/#if]
        [#if method.modifier.protected]protected[/#if]
        [#if method.modifier.final]final[/#if]
        [#if method.modifier.static]static[/#if]
        [#if method.result?has_content]
          [@typeLink method.result.type /]
        [#else]
          void
        [/#if]
      </code>
    </td>
    <td class="method description">
      <code>
        <a href="#${method.name?html}">${method.name?html}</a>([@params params=method.parameters /])
      </code>
      <br />
      [@deprecatedSummary method /]
      ${method.shortDescription}
    </td>
  </tr>
[/#macro]

[#--
  - Prints the detail information of the specified method.
  -
  - @param {MethodDoc} method
  -            The method model.
  --]
[#macro methodDetail method]
  <div id="${method.id?html}" class="detail">
    <h4>${method.name?html}</h4>
    <code class="syntax">
      [#if method.modifier.private]private[/#if]
      [#if method.modifier.protected]protected[/#if]
      [#if method.modifier.public]public[/#if]
      [#if method.modifier.final]final[/#if]
      [#if method.modifier.static]static[/#if]
      <span class="returnType">
        [#if method.result?has_content]
          [@typeLink method.result.type /]
        [#else]
          void
        [/#if]
      </span>
      <span class="name">${method.name?html}</span><span class="parameters">([@params params=method.parameters /])</span>
    </code>
    <div class="description">
      [@deprecatedDetail method /]
      ${method.description}
    </div>
    [#if method.parameters?has_content]
      <h5>Parameters</h5>
      <dl class="params">
        [#list method.parameters as param]
          <dt><code>${param.name?html}</code></dt>
          <dd>${param.description}</dd>
        [/#list]
      </dl>              
    [/#if]
    [#if method.result?has_content]
      <h5>Returns</h5>
      <dl class="return">
        <dd>${method.result.description}</dd>
      </dl>              
    [/#if]
    [@tags method /]
  </div>
[/#macro]

[#--
  - Prints the detail information of the specified constructor.
  -
  - @param {ConstructorDoc} method
  -            The constructor model.
  --]
[#macro constructorDetail constructor]
  <div id="${constructor.name?html}" class="detail">
    <h4>${constructor.name?html}</h4>
    <code class="syntax">
      [#if constructor.modifier.private]private[/#if]
      [#if constructor.modifier.protected]protected[/#if]
      [#if constructor.modifier.public]public[/#if]
      <span class="name">${constructor.name?html}</span><span class="parameters">([@params params=constructor.parameters /])</span>
    </code>
    <div class="description">
      [@deprecatedDetail constructor /]
      ${constructor.description}
    </div>
    [#if constructor.parameters?has_content]
      <h5>Parameters</h5>
      <dl class="params">
        [#list constructor.parameters as param]
          <dt><code>${param.name?html}</code></dt>
          <dd>${param.description}</dd>
        [/#list]
      </dl>              
    [/#if]
    [@tags constructor /]
  </div>
[/#macro]

[#--
  - Prints the summary of the specified field.
  -
  - @param {FieldDoc} method
  -            The field model.
  --]
[#macro fieldSummary field]
  <tr>
    <td class="modifier type">
      <code>
        [#if field.modifier.private]private[/#if]
        [#if field.modifier.protected]protected[/#if]
        [#if field.modifier.final]final[/#if]
        [#if field.modifier.static]static[/#if]
        [@typeLink field.type /]
      </code>
    </td>
    <td class="field description">
      <code>
        <a href="#${field.name?html}">${field.name?html}</a>
      </code>
      <br />
      [@deprecatedSummary field /]
      ${field.shortDescription}
    </td>
  </tr>
[/#macro]

[#--
  - Prints the detail information of the specified field.
  -
  - @param {FieldDoc} field
  -            The field model.
  --]
[#macro fieldDetail field]
  <div id="${field.id?html}" class="detail">
    <h4>${field.name?html}</h4>
    <code class="syntax">
      [#if field.modifier.private]private[/#if]
      [#if field.modifier.protected]protected[/#if]
      [#if field.modifier.public]public[/#if]
      [#if field.modifier.final]final[/#if]
      [#if field.modifier.static]static[/#if]
      [#if field.modifier.const]const[/#if]
      <span class="type">[@typeLink field.type /]</span>
      <span class="name">${field.name?html}</span>
    </code>
    <div class="description">
      [@deprecatedDetail field /]
      ${field.description}
    </div>
    [@tags field /]
  </div>
[/#macro]

[#--
  - Prints the summary of the specified enum constant.
  -
  - @param {EnumConstantDoc} method
  -            The enum constant model.
  --]
[#macro enumConstantSummary constant]
  <tr>
    <td class="enum-constant description">
      <code>
        <a href="#${constant.name?html}">${constant.name?html}</a>
      </code>
      <br />
      [@deprecatedSummary constant /]
      ${constant.shortDescription}
    </td>
  </tr>
[/#macro]

[#--
  - Prints the detail information of the specified enum constant.
  -
  - @param {EnumConstantDoc} constant
  -            The enum constant model.
  --]
[#macro enumConstantDetail constant]
  <div id="${constant.name?html}" class="detail">
    <h4>${constant.name?html}</h4>
    <code class="syntax">
      [#if constant.modifier.private]private[/#if]
      [#if constant.modifier.protected]protected[/#if]
      [#if constant.modifier.public]public[/#if]
      [#if constant.modifier.final]final[/#if]
      [#if constant.modifier.static]static[/#if]
      [#if constant.modifier.const]const[/#if]
      <span class="type">[@typeLink constant.type /]</span>
      <span class="name">${constant.name?html}</span>
    </code>
    <div class="description">
      [@deprecatedDetail constant /]
      ${constant.description}
    </div>
    [@tags constant /]
  </div>
[/#macro]

[#macro classTree class]
  [#if class.superClass?has_content]
    [@classTree class.superClass]
      [@docLink class]${class.qualifiedName?html}[/@docLink]
      <div>
        [#nested /]
      </div>
    [/@classTree]
  [#else]
    [@docLink class]${class.qualifiedName?html}[/@docLink]
    <div>
       [#nested /]
    </div>
  [/#if]
[/#macro]