[#include "common.ftl" /]
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    [#include "head.ftl" /]
  </head>
  <body>
    [#include "header.ftl" /]
    
    <nav id="subNav">
      <ul>
        [#if namespaces?has_content]
          <li><a href="#namespaces">Namespaces</a></li>
        [/#if]
        [#if interfaces?has_content]
          <li><a href="#interfaces">Interfaces</a></li>
        [/#if]
        [#if classes?has_content]
          <li><a href="#classes">Classes</a></li>
        [/#if]
        [#if enums?has_content]
          <li><a href="#enums">Enums</a></li>
        [/#if]
        [#if variables?has_content]
          <li><a href="#variable_summary">Variable Summary</a></li>
        [/#if]
        [#if functions?has_content]
          <li><a href="#function_summary">Function Summary</a></li>
        [/#if]
        [#if variables?has_content]
          <li><a href="#variable_detail">Variable Detail</a></li>
        [/#if]
        [#if functions?has_content]
          <li><a href="#function_detail">Function Detail</a></li>
        [/#if]
      </ul>
    </nav>
    
    <div id="body">
  
      [#if title?has_content]
        <h1>${title?html}</h1>
      [#else]
        <h2>Namespace [@namespaceLinks this /]</h2>
      [/#if]
      
      <div class="description">
        [@deprecatedDetail this /]
        ${description}
      </div>
      
      [@tags this /]
      
      [#if namespaces?has_content]
        <section id="namespaces">
          <h3>Namespaces</h3>
          <table summary="A summary of all sub namespaces in the current namespace.">
            <tr>
              <th class="namespace">Namespace</th>
              <th class="description">Description</th>
            </tr>
            [#list allNamespaces as namespace]
              <tr>
                <td class="namespace">
                  [@docLink namespace]${namespace.qualifiedName?html}[/@docLink]
                </td>
                <td class="description">
                  [@deprecatedSummary namespace /]
                  ${namespace.shortDescription}
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]

      [#if interfaces?has_content]
        <section id="interfaces">
          <h3>Interfaces</h3>
          <table summary="A summary of all interfaces in the current namespace.">
            <tr>
              <th class="interface">Interface</th>
              <th class="description">Description</th>
            </tr>
            [#list interfaces as interface]
              <tr>
                <td class="interface">
                  [@docLink interface]${interface.name?html}[/@docLink]
                </td>
                <td class="description">
                  [@deprecatedSummary interface /]
                  ${interface.shortDescription}
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]
 
      [#if classes?has_content]
        <section id="classes">
          <h3>Classes</h3>
          <table summary="A summary of all classes in the current namespace.">
            <tr>
              <th class="class">Class</th>
              <th class="description">Description</th>
            </tr>
            [#list classes as class]
              <tr>
                <td class="class">
                  [@docLink class]${class.name?html}[/@docLink]
                </td>
                <td class="description">
                  [@deprecatedSummary class /]
                  ${class.shortDescription}
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]
      
      [#if enums?has_content]
        <section id="enums">
          <h3>Enums</h3>
          <table summary="A summary of all enums in the current namespace.">
            <tr>
              <th class="enum">Enum</th>
              <th class="description">Description</th>
            </tr>
            [#list enums as enum]
              <tr>
                <td class="enum">
                  [@docLink enum]${enum.name?html}[/@docLink]
                </td>
                <td class="description">
                  [@deprecatedSummary enum /]
                  ${enum.shortDescription}
                </td>
              </tr>
            [/#list]           
          </table>
        </section>
      [/#if]
      
      [#if variables?has_content]
        <section id="variable_summary">
          <h3>Variable Summary</h3>
          <table summary="A summary of all variables in the current namespace.">
            <tr>
              <th class="modifier type">Modifier and Type</th>
              <th class="variable description">Variable and Description</th>
            </tr>
            [#list variables as variable]
              [@variableSummary variable /]
            [/#list]
          </table>
        </section>
      [/#if]

      [#if functions?has_content]
        <section id="function_summary">
          <h3>Function summary</h3>
          <table summary="A summary of all functions in the current namespace.">
            <tr>
              <th class="modifier type">Modifier and Type</th>
              <th class="function description">Function and Description</th>
            </tr>
            [#list functions as function]
              [@functionSummary function /]
            [/#list]
          </table>
        </section>
      [/#if]
      
      [#if variables?has_content]
        <section id="variable_detail">
          <h3>Variable Detail</h3>
          [#list variables as variable]
            [@variableDetail variable /]
          [/#list]
        </section>
      [/#if]
      
      [#if functions?has_content]
        <section id="function_detail">
          <h3>Function detail</h3>          
          [#list functions as function]
            [@functionDetail function /]
          [/#list]
        </section>
      [/#if]
    
    </div>

    [#include "footer.ftl" /]
  </body>
</html>