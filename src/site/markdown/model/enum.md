## Enum Documentation Model

This documentation model describes a JavaScript enum. It provides the following 
properties (Additional to the [common properties](index.html)):

#### constants
The [Enum Constant documentation model](enumconstant.html)s of all constants 
defines by the enum.

#### namespace
The [Namespace documentation model](namespace.html) of the namespace the enum 
is located in.

#### type
The enum [type](type.html).

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)
* [Enum Constant Documentation Model](enumconstant.html)