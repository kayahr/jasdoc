## Documentation Model

All documentation models provide the following properties:

#### baseUrl

The base URL pointing to the documentation root. This property can be used to 
build links to static files or other documentation pages. Example:

    <img src="${baseUrl}images/logo.png" />


#### deprecated

True if element is marked deprecated by an [@deprecated](../tags/deprecated.html) tag, false if not.


#### deprecation

The detailed deprecation description defined by the [@deprecated](../tags/deprecated.html) tag If element is not deprecated then an this returns an empty string.


#### description

The detailed HTML description of the element described by the model.


#### elementType

The type of the documentation element. Returns `INTERFACE`, `CLASS`, `NAMESPACE`, `METHOD`, `FUNCTION`, `VARIABLE`, `ENUM`, `CONSTRUCTOR`, `ENUM_CONSTANT`, `FIELD`, `PARAMETER` or `RESULT`.


#### generationTime

The generation time of this documentation model in form of a `Date` object. 
The template can use this to print a generation time information in the footer 
of the page for example:

    <span id="generationTime">${generationTime?datetime}</span>


#### global

Reference to the global [Namespace Documentation Model](namespace.html).


#### name

The simple name of the element. For example the name of the class `SomeClass` 
in the namespace `someNamespace` is simply `SomeClass`. If you need the name 
with namespaces included then you must use the `qualifiedName` property 
instead.


#### qualifiedName

The qualified name of the element. This is the simple name plus the qualified 
name of the parent element. For example the qualified name of the class 
`SomeClass` in the namespace `someNamespace` is `someNamespace.SomeClass`. 
If you need the simple name without the namespaces then you must use the 
`name` property instead.


#### references

List of See Also references defined by the  [@see](../tags/see.html) tag.


#### shortDeprecation

The short deprecation description of the element. This is the first sentence from the detailed deprecation description.


#### shortDescription

The short description of the element. This is the first sentence from the 
detailed description.


#### since

List of *since* information defined by the [@since](../tags/since.html) tag.


#### this

A reference to the current element itself.


#### url

Returns the URL of the element relative to the documentation root. So for 
linking to a documentation page of the class `otherClass`for example you can 
use code like this:

    <a href="${baseUrl}${otherClass.url}">${otherClass.name}</a>

Don't forget to prefix all URLs with `${baseUrl}` or otherwise your links are 
broken.


#### versions

List of versions defined by the [@version](../tags/version.html) tag.


### Concrete Documentation Models

Each documentation model also provides some scope-related properties. They are 
listed in the following articles:

* [Namespace Documentation Model](namespace.html) (Root model for `namespace.ftl`)
* [Class Documentation Model](class.html) (Root model for `class.ftl`)
* [Interface Documentation Model](interface.html) (Root model for `interface.ftl`)
* [Enum Documentation Model](enum.html) (Root model for `enum.ftl`)
* [Constructor Documentation Model](constructor.html)
* [Enum Constant Documentation Model](enumconstant.html)
* [Field Documentation Model](field.html)
* [Method Documentation Model](method.html)
* [Function Documentation Model](function.html)
* [Parameter Documentation Model](parameter.html)
* [Result Documentation Model](result.html)
* [Variable Documentation Model](variable.html)


### See Also

* [Themes](../themes.html)
