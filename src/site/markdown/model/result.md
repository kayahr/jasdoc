## Result Documentation Model

This documentation model describes a JavaScript function/method resuult. It 
provides the following properties 
(Additional to the [common properties](index.html)):

#### type
The return [type](type.html).

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)