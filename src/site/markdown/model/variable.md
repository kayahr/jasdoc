## Variable Documentation Model

This documentation model describes a JavaScript variable. It provides the 
following properties (Additional to the [common properties](index.html)):

#### modifier
The [modifier](modifier.html) flags of the variable.

#### namespace
The [Namespace documentation model](namespace.html) of the namespace the class 
is located in.

#### type
The variable [type](type.html).

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)