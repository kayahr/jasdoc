## Interface Documentation Model

This documentation model describes a JavaScript interface. It provides the 
following properties (Additional to the [common properties](index.html)):

#### allSuperInterfaces

A list of the [Interface documentation models](interface.html) of all super 
interfaces and all the super interfaces of the super interfaces and so on. It 
is recommended to assign the returned value to a variable when it is used 
multiple times because this is an expensive operation.

#### inheritedMethods

Returns a list of all super interfaces and super super interfaces (and so on)
and all the additional methods they provide. It is recommended to assign the
returned value to a variable when it is used multiple times because this
is an expensive operation.

    [#assign entries = inheritedMethods /]
    [#list entries as entry]
      [#assign interface = entry.key /]
      [#assign methods = entry.value /]
      <h3>${interface.name?html}:</h3>
      <ul>
        [#list methods as method]
          <li>${method.name}</li>
        [/#list 
      </ul>
    [/#list] 
    
#### implementingClasses
The [Class documentation models](class.html) of all classes implementing
this interface.

#### subInterfaces
The [Interface documentation models](interface.html) of all interfaces
extending this interface.

#### superInterfaces
The [Interface documentation models](interface.html) of all interfaces 
extended by the interface.

#### methods
The [Method documentation models](method.html) of all methods defined by the 
interface.

#### namespace
The [Namespace documentation model](namespace.html) of the namespace the 
interface is located in.

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)