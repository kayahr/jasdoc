## Type

JaSDoc supports all of the 
[JavaScript type expressions](https://developers.google.com/closure/compiler/docs/js-for-compiler#types) 
used by [Google's Closure Compiler](https://developers.google.com/closure/compiler/). 
Even if you don't use Google's Closure Compiler it is still recommended to 
use their type expressions so JaSDoc correctly links all the sub types in 
these expressions.

Example:

    /**
     * Returns the list of users.
     * 
     * @return {Array.<User>}
     *             The list of all users.
     */
    UserRegistry.prototype.getUsers = function()
    {
        return this.users;
    };

When JaSDoc creates the API documentation for the code above then the `User` 
type is correctly linked to the corresponding documentation page.

### Usage in theme templates

When printing a type expression in a theme template then you can simply use it 
in a FreeMarker expression if you want the plain text representation of the 
type:

    ${type}

But you most likely want the fully linked type expression instead. This works 
like this:

    ${type.toHtml(this)}

### See Also

* [Themes](../themes.html)
