## Enum Constant Documentation Model

This documentation model describes an enum constant. It provides the following 
properties (Additional to the [common properties](index.html)):

#### modifier
The [modifier](modifier.html) flags of the enum.

#### type
The enum [type](type.html).

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)
* [Enum Documentation Model](enum.html)