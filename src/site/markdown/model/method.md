## Method Documentation Model

This documentation model describes a JavaScript method. It provides the 
following properties (Additional to the [common properties](index.html)):

#### modifier
The [modifier](modifier.html) flags of the method.

#### parameters
The [Parameter documentation model](parameter.html)s of all method parameters.

#### result
The [Result documentation model](result.html) of the method result. Not set if 
method doesn't have a result.

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)