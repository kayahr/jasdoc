## Field Documentation Model

This documentation model describes a JavaScript field. It provides the 
following properties (Additional to the [common properties](index.html)):

#### modifier
The [modifier](modifier.html) flags of the field.

#### type
The field [type](type.html).

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)