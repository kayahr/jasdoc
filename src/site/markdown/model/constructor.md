## Constructor Documentation Model

This documentation model describes a JavaScript constructor. It provides the 
following properties (Additional to the [common properties](index.html)):

#### modifier
The [modifier](modifier.html) flags of the constructor.

#### parameters
The [Parameter documentation model](parameter.html)s of all constructor 
parameters.

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)