## Modifier

The modifier flags container provides the following properties:

#### public
True if element is public, false if not. Only applicable to methods and fields.

#### private
True if element is private, false if not. Only applicable to methods and fields.

#### protected
True if element is protected, false if not. Only applicable to methods and fields.

#### static
True if element is static, false if not. Only applicable to methods and fields.

#### final
True if element is final, false if not.

#### const
True if element is const, false if not. Only applicable to variables.


### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)