## Function Documentation Model

This documentation model describes a JavaScript function. It provides the 
following properties (Additional to the [common properties](index.html)):

#### modifier
The [modifier](modifier.html) flags of the function.

#### namespace
The [Namespace documentation model](namespace.html) of the namespace the 
function is located in.

#### parameters
The [Parameter documentation model](parameter.html)s of the function parameters.

#### result
The [Result documentation model](result.html) of the function result. Not set 
if function does not have a result.

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)
