## Namespace Documentation Model

This documentation model describes a JavaScript namespace. It provides the 
following properties (Additional to the [common properties](index.html)):

#### allNamespaces

A set of all sub namespace documentation models (Direct and indirect) of this 
namespace sorted by their qualified names. Example:

    [#list allNamespaces as namespace]
      ${namespace.qualifiedName}
    [#/list]

Example output:

    jascfx
    jascfx.scene
    jascfx.scene.chart
    jascfx.scene.effect
    jascfx.util


#### superNamespace

Returns the documentation model of the super namespace. For the global 
namespace this is not set because the global namespace can't have a super namespace.

#### classes / enums / functions / interfaces / variables / namespaces

Returns the documentation models for all [classes](class.html), 
[enums](enum.html), [functions](function.html), [interfaces](interface.html),
[variables](variable.html) or direct sub namespaces in this namespace sorted by 
their names. Example:

    [#list classes as class]
      ${class.name}
    [/#list]

Example output:

    Camera
    Node
    Scene

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)