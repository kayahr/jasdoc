## Parameter Documentation Model

This documentation model describes a JavaScript function/method/constructor 
parameter. It provides the following properties (Additional to the 
[common properties](index.html)):

#### type
The parameter [type](type.html).

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)