## Class Documentation Model

This documentation model describes a JavaScript class. It provides the 
following properties (Additional to the [common properties](index.html)):

#### allSuperClasses

A list of the [Class documentation models](class.html) of all super classes
and all the super classes of the super classes and so on. It is recommended
to assign the returned value to a variable when it is used multiple times
because this is an expensive operation. 

#### constructor
The [Constructor documentation model](constructor.html) of the constructor.

#### fields
The [Field documentation models](field.html) of all fields of the class.

#### inheritedMethods

Returns a list of all super classes and super super classes (and so on) and 
all the additional methods they provide. It is recommended to assign the
returned value to a variable when it is used multiple times because this
is an expensive operation.

    [#assign entries = inheritedMethods /]
    [#list entries as entry]
      [#assign class = entry.key /]
      [#assign methods = entry.value /]
      <h3>${class.name?html}:</h3>
      <ul>
        [#list methods as method]
          <li>${method.name}</li>
        [/#list 
      </ul>
    [/#list] 

#### interfaces
The [Interface documentation models](interface.html) of all interfaces 
implemented by the class.

#### methods
The [Method documentation models](method.html) of all methods of the class.

#### multipleInheritance
True if this class or one of its super classes uses multiple inheritance.
False if this call and all its super classes uses single inheritance. A
template can use this flag to decide if it makes sense to display an
inheritance tree or not.

#### namespace
The [Namespace documentation model](namespace.html) of the namespace the class 
is located in.

#### subClasses
The [Class documentation models](class.html) of all classes extending this
class.

#### superClass
The [Class documentation model](class.html) of the super class extended by
this class. This property only makes sense when the class uses single
inheritance.

#### superClasses
The [Class documentation models](class.html) of all super classes extended by 
this class.

### See Also

* [Themes](../themes.html)
* [Documentation Model](index.html)