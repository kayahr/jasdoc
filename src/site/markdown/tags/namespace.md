## @namespace

This tag defines a namespace and provides a description for it.

### Syntax

    @namespace description
    
### Example

    /**
     * @namespace 
     * The main namespace of the jascfx library. 
     */
    var jascfx = {};
     
### See Also

* [Tags](../tags.html)
