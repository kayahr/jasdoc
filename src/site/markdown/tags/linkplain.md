## {@linkplain}

The inline `{@linkplain}` tag links to some other documented element. The link is printed with the normal text font.

### Syntax

    {@linkplain namespace.symbol#member label}

The hash character must only be used for non-static class members. Static class members are handled liked namespaced symbols (dot character).

The label is optional. If not specified then the link itself is used as a label.
    
### Example

     /**
      * @deprecated Use {@linkplain Person#getEmail} instead.
      */
     Person.prototype.getEMail = function()
     {
         return this.name;
     };

### See Also

* [Tags](../tags.html)
* [{@link}](link.html)
* [@see](see.html)
