## @author

The `@author` defines an author of a documented element. It can be used multiple times if the element has multiple authors. In this case all authors are printed separated with a comma. But you also can specify multiple authors in one tag. 

The argument is simply treated as normal text with one exception: E-Mail addresses enclosed in `<>` brackets are converted into E-Mail links.

Author information is ignored by default. You have to specify the `-A` or `--author` command line argument to enable author information in JaSDoc.

### Syntax

    @author Name
    
### Example

    /**
     * Constructs a new button.
     *
     * @constructor
     * @class A button component.
     * @author Klaus Reimer <k@ailis.de>
     */
    function Button()
    {
       ...
    }
     
### See Also

* [Tags](../tags.html)
