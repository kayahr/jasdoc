## @protected

The `@protected` tag marks a method, constructor or field as protected so it
can only be accessed from within the same class or by sub classes.

### Syntax

    @protected
    
### Example

    /**
     * Internally called to click the button.
     *
     * @protected
     */
    Button.prototype.doClick = function()
    {
       ...
    };
     
### See Also

* [Tags](../tags.html)
* [@protected](../protected.html)
