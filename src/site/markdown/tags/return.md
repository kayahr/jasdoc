## @return

The `@return` tag provides a description and a type for a return value of a
function or method.

### Syntax

    @return {type} description
    
### Example

    /**    
     * Returns the user's name.
     *
     * @return {string}
     *           The user's name.  
     */
    User.prototype.getName = function()
    {
        return this.name;
    };
     
### See Also

* [Tags](../tags.html)
