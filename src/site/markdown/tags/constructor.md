## @constructor

A class must be marked with the @constructor tag because otherwise the
function is handled as a normal function instead of a class.

### Syntax

    @constructor

### Example

    /**
     * Constructs a new Point with the specified coordinates.
     *
     * @param {number} x
     *            The X coordinate of the point.
     * @param {number} y
     *            The Y coordinate of the point.
     * @constructor
     * @class 
     * A Point is a container for a two-dimensional coordinate. 
     */
    function Point(x, y)
    {
        this.x = x;
        this.y = y;
    }

### See Also

* [Tags](../tags.html)
* [@class](class.html)
* [@description](description.html)