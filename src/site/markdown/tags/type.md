## @type

The `@type` tag defines the type of a variable or field.

### Syntax

    @type {type}
    
### Example

    /**
     * The list of users.
     * 
     * @type {!Array.<!User>}
     */
    var users = [];
     
### See Also

* [Tags](../tags.html)
