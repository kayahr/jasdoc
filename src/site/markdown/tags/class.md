## @class

The @class tag is used to set a description for a class. In JavaScript a
class is defined by it's constructor so the constructor function and the
class uses the same JSDoc comment. That's why it is necessary to use this
tag to separate between the constructor documentation and the class 
documentation.

Because all text which follows the @class tag belongs to the class description
you have to use the [@description](description.html) tag if you want to
define the constructor documentation after the class documentation.

### Syntax

    @class description

### Example

    /**
     * Constructs a new Point with the specified coordinates.
     *
     * @param {number} x
     *            The X coordinate of the point.
     * @param {number} y
     *            The Y coordinate of the point.
     * @constructor
     * @class 
     * A Point is a container for a two-dimensional coordinate. 
     */
    function Point(x, y)
    {
        this.x = x;
        this.y = y;
    }

### See Also

* [Tags](../tags.html)
* [@constructor](constructor.html)
* [@description](description.html)