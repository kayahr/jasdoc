## @inheritDoc

The `@inheritDoc` tag can be used for overriding methods of classes and
interfaces. The tag tells JaSDoc to use the documentation from the overidden
method.

### Syntax

    @inheritDoc
   
### Example

    /**
     * @inheritDoc
     */
    MyClass.prototype.toString = function() {};

### See Also

* [Tags](../tags.html)
