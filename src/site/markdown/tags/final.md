## @final

The `@final` tag marks a method, field or class das final which means the method
or field can not be overridden by sub classes or a class can't be extended.

### Syntax

    @final
    
### Example

     /**
      * Returns the name.
      *
      * @return {string} The name.
      * @final
      */
     Person.prototype.getName = function()
     {
         return this.name;
     };

### See Also

* [Tags](../tags.html)
