## @extends

The `@extends` tag tells tools like JaSDoc or compilers which class a class
extends. The tag has a single argument which is the name of the base class.

Naturally this tag is only informational and doesn't do any magic 
inheritance. That's up to you.

JasDoc supports multiple inheritance so if your code supports it then you
can define multiple `@extends` tags and this hierarchy will be properly 
documented. Please note that compilers like Google's Closure Compiler might
not support multiple inheritance.

### Syntax

    @extends baseclass
    
The base class can be defined as a simple class name (Example: `BaseClass`)
or as a type (Example: `{BaseClass}`). Compilers might require one specific
syntax, JaSDoc supports both.

### Example

    /**
     * @constructor
     * @extends {myFramework.BaseClass}
     */
     myFramework.SubClass = function()
     {
         // Call super constructor
         myFramework.BaseClass.call(this);
     };
     
     // Do the actual inheritance (Custom code)
     myFramework.extend(myFramework.SubClass, myFramework.BaseClass);

### See Also

* [Tags](../tags.html)
* [@class](class.html)
* [@constructor](constructor.html)
* [@implements](implements.html)
