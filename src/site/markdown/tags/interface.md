## @interface

The `@interface` tag marks a function as an interface. An interface is similar
to a class but is only used by a compiler to validate the code.

### Syntax

    @interface
    
### Example

    /**
     * Interface for selectable objects.
     *
     * @interface
     */
     function Selectable() {};
     
     /**
      * Selects the object.
      */
     Selectable.prototype.select = function() {}
     
### See Also

* [Tags](../tags.html)
* [@implements](implements.html)
