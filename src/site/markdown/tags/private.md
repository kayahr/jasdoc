## @private

The `@private` tag marks a method, constructor or field as private so it can
only be accessed from within the same class.

### Syntax

    @private
    
### Example

    /**
     * Internally called to click the button.
     *
     * @private
     */
    Button.prototype.doClick = function()
    {
       ...
    };
     
### See Also

* [Tags](../tags.html)
* [@protected](../protected.html)
