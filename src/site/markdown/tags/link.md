## {@link}

The inline `{@link}` tag links to some other documented element. The link is printed with a monospace code font.

### Syntax

    {@link namespace.symbol#member label}

The hash character must only be used for non-static class members. Static class members are handled liked namespaced symbols (dot character).

The label is optional. If not specified then the link itself is used as a label.
    
### Example

     /**
      * @deprecated Use {@link Person#getEmail} instead.
      */
     Person.prototype.getEMail = function()
     {
         return this.name;
     };

### See Also

* [Tags](../tags.html)
* [{@linkplain}](linkplain.html)
* [@see](see.html)
