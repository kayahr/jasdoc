## @description

The `@description` tag can be used to switch back to the context of the
documented element when a previous tag switched the context to something else.
The `@class` tag for example switched the documentation context from the
constructor to the class. So if you want to add some more documentation
for the constructor then you must use the `@description` tag to switch back
to the constructor context.

### Syntax

    @description description

### Example

    /**
     * This text belongs to the constructor documentation.
     *
     * @class This text and all other lines
     * after it belongs to the class documentation.
     *
     * @description This text again belongs to the constructor
     * documentation.
     */

### See Also

* [Tags](../tags.html)
* [@class](class.html)
