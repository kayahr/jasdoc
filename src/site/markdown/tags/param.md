## @param

The `@param` tag provides a description and a type for a parameter of a 
function, method or constructor.

### Syntax

    @param {type} name description
    
### Example

    /**
     * @param {string} key
     *           The map key.
     * @param {!Array.<number}> values
     *           The map values to store under the specified key.  
     */
    Repository.prototype.put = function(key, values)
    {
        this.mapping[key] = values;
    };
     
### See Also

* [Tags](../tags.html)
