## @const

The @const tag simply marks a variable as a constant. Compilers can use
this information to check for accidental reassignment of a new value to 
a constant and to inline constant values to optimize the source code.

JaSDoc also supports the `@constant` tag as an alias for `@const` but be
aware that compilers might not support it. It is recommended to always use
`@const` instead of `@constant`.

### Syntax

    @const

### Example

    /** 
     * The application name.
     * @const 
     */
    var APP_NAME = "Gruntmaster 6000";

### See Also

* [Tags](../tags.html)
