## @see

The `@see}` defines a reference to some other documented element or external page or simply defines a reference text. The tag can be used multiple times.

### Syntax

    @see namespace.symbol#member label
    @see <a href="someLink">Label</a>
    @see Some text

The label in the first syntax is optional. If not specified then the reference itself is used as a label. The hash character in the reference must only be used for non-static class members. Static class members are handled liked namespaced symbols (dot character).

### Example

     /**
      * @see Person#getEMail
      */
     ImportantPerson.prototype.getEMail = function()
     {
         return this.name;
     };

### See Also

* [Tags](../tags.html)
* [{@link}](link.html)
* [{@linkplain}](linkplain.html)
