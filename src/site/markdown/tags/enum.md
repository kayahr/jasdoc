## @enum

Enums are objects which define named constants. A JavaScript compiler like
Google's Closure Compiler can use these constants to optimize and validate
your source code. It can inline the constants and can check for switch/case
blocks which do not handle all defined enum constants

### Syntax

    @enum type
   
The type is optional and defaults to `{number}.`

### Example

    /**
     * Alignment constants.
     *
     * @enum {string}
     */
    var Alignment = {
        /** Align to the left. */
        LEFT = "left",
        
        /** Center. */
        CENTER = "center",
        
        /** Align to the right. */
        RIGHT = "right"
    };

### See Also

* [Tags](../tags.html)
