## @implements

The `@implements` tag tells tools like JaSDoc or compilers which interface a
class implementss. The tag has a single argument which is the name of the 
implemented interface.

You can define multiple `@implements` tags if the class implements more than
one interface.

### Syntax

    @implements interface
     
The interface can be defined as a simple interface name (Example: `Clickable`)
or as a type (Example: `{Clickable}`). Compilers might require one specific
syntax, JaSDoc supports both.
   
### Example

    /**
     * @constructor
     * @implements {Clickable}    
     */
     function Button() {};

### See Also

* [Tags](../tags.html)
* [@constructor](constructor.html)
* [@extends](extends.html)
* [@interface](interface.html)
