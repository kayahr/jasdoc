## Themes

The HTML output of JaSDoc is configurable by themes. A theme usually contains 
a fixed set of FreeMarker template files, some additional FreeMarker files 
which are included by the the other templates, a CSS stylesheet file and some 
image files. The default theme contains the following files:

* [namespace.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/namespace.ftl) (Renders a namespace documentation page)
* [class.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/class.ftl) (Renders a class documentation page)
* [interface.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/interface.ftl) (Renders an interface documentation page)
* [enum.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/enum.ftl) (Renders a enum documentation page)
* [head.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/head.ftl) (Included to render the HTML head)
* [header.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/header.ftl) (Included to render the page header)
* [footer.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/footer.ftl) (Included to render the page footer)
* [common.ftl](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/common.ftl) (Provides some common macros used in most pages)
* [styles.css](https://github.com/kayahr/jasdoc/blob/master/src/main/resources/de/ailis/jasdoc/themes/default/styles.css) (The CSS stylesheet)

The HTML output structure is fixed and can't be changed. This structure 
always looks like this:

    index.html (Global namespace documentation)
    SomeClass.html (Documentation for the Class `SomeClass` in the global 
                    namespace)
    SomeInterface.html (Documentation for the interface `SomeInterface` in the 
                        global namespace)
    SomeEnum.html (Documentation for the enum `SomeEnum` in the global namespace)
    ...
    someNamespace/
        index.html (Documentation for the namespace `someNamespace`
        AnotherType.html (Documentation for the class, interface or enum 
                          `AnotherType` in the namespace `someNamespace`)
        ...
    ...

This structure is built by JaSDoc by iterating over all documentation elements 
and rendering the HTML files with the templates `namespace.ftl`, `class.ftl`, 
`interface.ftl` and `enum.ftl`. All Non-FreeMarker files (Stylesheets, Images, 
JavaScript files, ...) are simply recursively copied to the output directory. 
FreeMarker template files which are not directly called by JaSDoc are included 
by the other FreeMarker files.
    
When rendering a page the FreeMarker model is set to the current documentation 
element. So when JasDoc for example renders the page for the Class 
`someNamespace/SomeClass` then the documentation of this class is the 
FreeMarker root model. From this model the template can fetch information 
about the class and navigate to other documentation models (Methods, Fields,
Constructor, Namespace, ...)

### See Also

* [Documentation Model](model/index.html)
