## Tags

This is a list of JSDoc tags supported by JaSDoc:

<table>
  <tr>
    <th>Tag Name</th>
    <th>Tag Description</th>
  </tr>
  <tr>
    <td><a href="tags/author.html">@class</a></td>
    <td>Defines the author of an element.</td>
  </tr>
  <tr>
    <td><a href="tags/class.html">@class</a></td>
    <td>Defines a class description.</td>
  </tr>
  <tr>
    <td><a href="tags/constructor.html">@constructor</a></td>
    <td>Marks a function as a class constructor.</td>
  </tr>
  <tr>
    <td><a href="tags/const.html">@const</a></td>
    <td>Marks a variable as a constant.</td>
  </tr>
  <tr>
    <td><a href="tags/description.html">@description</a></td>
    <td>Continues function description after a class description.</td>
  </tr>
  <tr>
    <td><a href="tags/enum.html">@enum</a></td>
    <td>Marks an object as an enum.</td>
  </tr>
  <tr>
    <td><a href="tags/extends.html">@extends</a></td>
    <td>Specifies the base class which a class extends.</td>
  </tr>
  <tr>
    <td><a href="tags/final.html">@final</a></td>
    <td>Marks a method, field or class as final so it can't be overridden by sub classes.</td>
  </tr>
  <tr>
    <td><a href="tags/implements.html">@implements</a></td>
    <td>Specifies an implemented interface.</td>
  </tr>
  <tr>
    <td><a href="tags/inheritdoc.html">@inheritDoc</a></td>
    <td>Inherits the documentation from the overridden method.</td>
  </tr>
  <tr>
    <td><a href="tags/interface.html">@interface</a></td>
    <td>Defines an interface.</td>
  </tr>
  <tr>
    <td><a href="tags/link.html">{@link}</a></td>
    <td>Link to some other documented element.</td>
  </tr>
  <tr>
    <td><a href="tags/linkplain.html">{@linkplain}</a></td>
    <td>Same as {@link} but using normal text font instead of monospace code font.</td>
  </tr>
  <tr>
    <td><a href="tags/namespace.html">@namespace</a></td>
    <td>Defines a namespace.</td>
  </tr>
  <tr>
    <td><a href="tags/param.html">@param</a></td>
    <td>Provides a description and a type for a function or method parameter.</td>
  </tr>
  <tr>
    <td><a href="tags/private.html">@private</a></td>
    <td>Marks a field or method as private so it can only be used from inside the same class.</td>
  </tr>
  <tr>
    <td><a href="tags/protected.html">@protected</a></td>
    <td>Marks a field or method as protected so it can only be used from inside the same class or from sub classes.</td>
  </tr>
  <tr>
    <td><a href="tags/return.html">@return</a></td>
    <td>Provides a description and a type for function and method return values.</td>
  </tr>
  <tr>
    <td><a href="tags/see.html">@see</a></td>
    <td>Defines a see also reference to some other type or external link or some text.</td>
  </tr>
  <tr>
    <td><a href="tags/type.html">@type</a></td>
    <td>Defines the type of variable or field.</td>
  </tr>
</table>
