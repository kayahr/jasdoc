/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.doc.types;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import de.ailis.jasdoc.types.AnyType;
import de.ailis.jasdoc.types.ArrayType;
import de.ailis.jasdoc.types.BooleanType;
import de.ailis.jasdoc.types.ClassType;
import de.ailis.jasdoc.types.AbstractType;
import de.ailis.jasdoc.types.MapType;
import de.ailis.jasdoc.types.NonNullType;
import de.ailis.jasdoc.types.NullableType;
import de.ailis.jasdoc.types.NumberType;
import de.ailis.jasdoc.types.OptionalType;
import de.ailis.jasdoc.types.StringType;
import de.ailis.jasdoc.types.TypeFactory;
import de.ailis.jasdoc.types.UnionType;
import de.ailis.jasdoc.types.VarArgType;

/**
 * Tests the JsType class.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class JsTypeTest
{
    /** The type factory. */
    private TypeFactory typeFactory;

    /**
     * Setup test.
     */
    @Before
    public void setUp()
    {
        this.typeFactory = new TypeFactory();
    }

    /**
     * Tests parsing a ANY type.
     */
    @Test
    public void testParseAny()
    {
        final AbstractType type = this.typeFactory.valueOf("*");
        assertEquals(AnyType.class, type.getClass());
        assertEquals("*", type.toExpression());
    }

    /**
     * Tests parsing a string type.
     */
    @Test
    public void testParseString()
    {
        final StringType type = (StringType) this.typeFactory.valueOf("string");
        assertEquals("string", type.toExpression());
    }

    /**
     * Tests parsing a boolean type.
     */
    @Test
    public void testParseBoolean()
    {
        final BooleanType type =
            (BooleanType) this.typeFactory.valueOf("boolean");
        assertEquals("boolean", type.toExpression());
    }

    /**
     * Tests parsing a number type.
     */
    @Test
    public void testParseNumber()
    {
        final NumberType type = (NumberType) this.typeFactory.valueOf("number");
        assertEquals("number", type.toExpression());
    }

    /**
     * Tests parsing a variable argument type.
     */
    @Test
    public void testParseVarArg()
    {
        final VarArgType type =
            (VarArgType) this.typeFactory.valueOf("...number");
        assertEquals(NumberType.class, type.getType().getClass());
        assertEquals("...number", type.toExpression());
    }

    /**
     * Tests parsing an optional argument type.
     */
    @Test
    public void testParseOptional()
    {
        final OptionalType type =
            (OptionalType) this.typeFactory.valueOf("number=");
        assertEquals(NumberType.class, type.getType().getClass());
        assertEquals("number=", type.toExpression());
    }

    /**
     * Tests parsing a nullable argument type.
     */
    @Test
    public void testParseNullable()
    {
        final NullableType type =
            (NullableType) this.typeFactory.valueOf("?number");
        assertEquals(NumberType.class, type.getType().getClass());
        assertEquals("?number", type.toExpression());
    }

    /**
     * Tests parsing a non nullable argument type.
     */
    @Test
    public void testParseNonNullable()
    {
        final NonNullType type =
            (NonNullType) this.typeFactory.valueOf("!number");
        assertEquals(NumberType.class, type.getType().getClass());
        assertEquals("!number", type.toExpression());
    }

    /**
     * Tests parsing an array type.
     */
    @Test
    public void testParseArray()
    {
        final ArrayType type =
            (ArrayType) this.typeFactory.valueOf("Array.<number>");
        assertEquals(NumberType.class, type.getType().getClass());
        assertEquals("Array.<number>", type.toExpression());
    }

    /**
     * Tests parsing an array type.
     */
    @Test
    public void testParseArrayArray()
    {
        final ArrayType type =
            (ArrayType) this.typeFactory.valueOf("Array.<Array.<number>>");
        final ArrayType subType = (ArrayType) type.getType();
        assertEquals(NumberType.class, subType.getType().getClass());
        assertEquals("Array.<Array.<number>>", type.toExpression());
    }

    /**
     * Tests parsing an array type.
     */
    @Test
    public void testParseCustom()
    {
        final ClassType type =
            (ClassType) this.typeFactory.valueOf("namespace.SomeClass");
        assertEquals("namespace.SomeClass", type.getName());
        assertEquals("namespace.SomeClass", type.toExpression());
    }

    /**
     * Tests parsing a map type.
     */
    @Test
    public void testParseMap()
    {
        final MapType type =
            (MapType) this.typeFactory.valueOf("Object.<string,number>");
        assertEquals(StringType.class, type.getKeyType().getClass());
        assertEquals(NumberType.class, type.getValueType().getClass());
        assertEquals("Object.<string,number>", type.toExpression());
    }

    /**
     * Tests parsing a union type.
     */
    @Test
    public void testParseUnion()
    {
        final UnionType type =
            (UnionType) this.typeFactory.valueOf("string|number|boolean");
        assertEquals(3, type.getTypes().size());
        assertEquals(StringType.class, type.getTypes().get(0).getClass());
        assertEquals(NumberType.class, type.getTypes().get(1).getClass());
        assertEquals(BooleanType.class, type.getTypes().get(2).getClass());
        assertEquals("string|number|boolean", type.toExpression());
    }
}
