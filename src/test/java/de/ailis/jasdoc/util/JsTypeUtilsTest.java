/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.md for licensing information.
 */

package de.ailis.jasdoc.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests the JsTypeUtils class.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
public final class JsTypeUtilsTest
{
    /**
     * Tests the splitByType method.
     */
    @Test
    public void testSplitByType()
    {
        String[] parts = JsTypeUtils.splitByType("  ");
        assertEquals(2, parts.length);
        assertEquals("", parts[0]);
        assertEquals("", parts[1]);

        parts = JsTypeUtils.splitByType("{");
        assertEquals(2, parts.length);
        assertEquals("{", parts[0]);
        assertEquals("", parts[1]);

        parts = JsTypeUtils.splitByType("a");
        assertEquals(2, parts.length);
        assertEquals("a", parts[0]);
        assertEquals("", parts[1]);

        parts = JsTypeUtils.splitByType("a}");
        assertEquals(2, parts.length);
        assertEquals("a}", parts[0]);
        assertEquals("", parts[1]);

        parts = JsTypeUtils.splitByType("{ a ");
        assertEquals(2, parts.length);
        assertEquals("{ a", parts[0]);
        assertEquals("", parts[1]);

        parts = JsTypeUtils.splitByType("  {Object.<string, number>} var");
        assertEquals(2, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("var", parts[1]);

        parts = JsTypeUtils.splitByType("  {Object.<string, number>}var");
        assertEquals(2, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("var", parts[1]);

        parts = JsTypeUtils.splitByType("  {Object.<string, number>} ");
        assertEquals(2, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("", parts[1]);

        parts = JsTypeUtils.splitByType("{string} test");
        assertEquals(2, parts.length);
        assertEquals("{string}", parts[0]);
        assertEquals("test", parts[1]);
    }

    /**
     * Tests the splitByTypeAndName method.
     */
    @Test
    public void testSplitByTypeAndName()
    {
        String[] parts = JsTypeUtils.splitByTypeAndName("  ");
        assertEquals(3, parts.length);
        assertEquals("", parts[0]);
        assertEquals("", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName("{");
        assertEquals(3, parts.length);
        assertEquals("{", parts[0]);
        assertEquals("", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName("a");
        assertEquals(3, parts.length);
        assertEquals("a", parts[0]);
        assertEquals("", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName("a}");
        assertEquals(3, parts.length);
        assertEquals("a}", parts[0]);
        assertEquals("", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName("{ a ");
        assertEquals(3, parts.length);
        assertEquals("{ a", parts[0]);
        assertEquals("", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName(
            "  {Object.<string, number>} var");
        assertEquals(3, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("var", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName(
            "  {Object.<string, number>}var");
        assertEquals(3, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("var", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName("  {Object.<string, number>} ");
        assertEquals(3, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName("{string} test");
        assertEquals(3, parts.length);
        assertEquals("{string}", parts[0]);
        assertEquals("test", parts[1]);
        assertEquals("", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName(
            "  {Object.<string, number>}var some doc");
        assertEquals(3, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("var", parts[1]);
        assertEquals("some doc", parts[2]);

        parts = JsTypeUtils.splitByTypeAndName(
            "{Object.<string, number>} var some doc");
        assertEquals(3, parts.length);
        assertEquals("{Object.<string, number>}", parts[0]);
        assertEquals("var", parts[1]);
        assertEquals("some doc", parts[2]);
    }
}
