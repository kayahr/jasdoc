/**
 * @constructor
 */
function Class() {}

/**
 * Before {@linkplain Class} After
 */
Class.prototype.method1 = function() {};

/**
 * Before {@linkplain UnknownClass} After
 */
Class.prototype.method2 = function() {};

/**
 * Before {@linkplain Class Some Label} After
 */
Class.prototype.method3 = function() {};

/**
 * Before {@linkplain UnknownClass Some Label} After
 */
Class.prototype.method4 = function() {};

/**
 * Before {@linkplain Class#method1 Label} After
 */
Class.prototype.method5 = function() {};

/**
 * Before {@linkplain Class.method6 Label} After
 */
Class.method6 = function() {};
