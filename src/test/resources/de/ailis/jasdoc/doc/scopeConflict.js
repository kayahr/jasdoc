/** Scope 1 Doc */
scope = {
    func1: function() {}
};

/** Scope 2 Doc */
scope = {
    func2: function() {}
};
