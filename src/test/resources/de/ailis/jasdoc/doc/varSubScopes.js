/** @namespace Parent Scope */
var parent = {
        
    /**
     * @namespace Sub scope 1.
     */
    sub1: {
        /** @namespace Sub sub scope */
        sub: {}
    },
    
    /** Some number. */
    someNumber: 43,
    
    /** Some string. */
    someString: "hui",
    
    /** @namespace Sub scope 2. */
    sub2: {
    }
};
