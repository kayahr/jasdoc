/**
 * Short description. A longer description which is so long it needs another
 * line to tell you how long it is.
 * 
 * @param {number} param1 The first parameter. Another long description which
 *           needs more lines.
 * @param     {string} param2
 *           The second parameter with a short description which spans
 *           multiple lines. And some more stuff.
 * @return 
 * 
 *   {number|string}
 *           The return value. Again we have a long description which fills
 *           multiple lines. This time it is even so long it needs another
 *           line.
 */
function funcName(param1, param2)
{
    return param1 + param2;
}