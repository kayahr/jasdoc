/**
 * Enum description.
 * 
 * @enum {number}
 */
Enum1 = {
    /** First constant. */
    FIRST: 1,
    
    /** 
     * Second constant. 
     * @type {string}
     */
    SECOND: 2
}
