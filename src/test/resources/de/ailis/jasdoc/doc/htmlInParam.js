/**
 * @param {!Array.<string>} param1 
 *            <b>Bold</b> <ul><li>test</li></ul> <i>Italic</i><script>alert("test")</script>.
 *            <b>Broken <i>tag structure</b></i>.
 *            
 */
function funcName(param1) {}