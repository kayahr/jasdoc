/** Some scope. */
someScope = {
    /**
     * Bar function.
     */
    bar: function(a, xxx) {},

    foo: /** Foo function. */ function(a) {}
}