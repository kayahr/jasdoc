/**
 * @constructor
 */
function Class() {}

/**
 * Before {@link Class} After
 */
Class.prototype.method1 = function() {};

/**
 * Before {@link UnknownClass} After
 */
Class.prototype.method2 = function() {};

/**
 * Before {@link Class Some Label} After
 */
Class.prototype.method3 = function() {};

/**
 * Before {@link UnknownClass Some &#125; Label} After
 */
Class.prototype.method4 = function() {};

/**
 * Before {@link Class#method1 Label} After
 */
Class.prototype.method5 = function() {};

/**
 * Before {@link Class.method6 Label} After
 */
Class.method6 = function() {};
