/**
 * @constructor
 */
function Class() {}

/**
 * @see Class#method2 This is a label
 */
Class.prototype.method1 = function() {};

/**
 * @see Just some text
 */
Class.prototype.method2 = function() {};

/**
 * @see <a href="http://github.com/">GitHub</a>
 */
Class.prototype.method3 = function() {};
