/** Var Func 1 */
var 

varFunc1 = function() {},

/** Var Func 2 */
varFunc2 = function() {},

someNumber = 53,

someString = "test",
    
varFunc3 = /** Var Func 3 */ function() {},

varFunc4 = function() {};
