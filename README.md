JaSDoc
======

JaSDoc generates JSDoc API documentation from your JavaScript source code. It 
is a platform-independent command-line application written in [Java][1] 
providing the following notable features:  

* Rock-solid source code parsing based on [Rhino][2].
* Supports complex [type expressions][3], interfaces and enums as
  used by Google's [Closure Compiler][4].
* Extensive HTML support in JSDoc comments thanks to [JSoup][5].
* Supports themes using [FreeMarker][6] as template engine.

<div style="color:red">
Attention! JaSDoc is currently in an early alpha stage and only supports well-formed JavaScript code like Google's Closure Library for example. Any code which dynamically creates functions or classes during runtime is not supported yet. So if your code needs special tags like @borrows, @constructs, @field, @function, @inner, @lends, @name, @property or @static to tell the documentation tool how your code is structured then it won't work with JaSDoc yet.
</div>
 
 
Download
--------

The latest release of JaSDoc can always be found in source form or in binary
form at GitHub:

<http://github.com/kayahr/jasdoc>


Installation
------------

Simply extract the downloaded binary ZIP file wherever you like and then you 
can run `jasdoc` in this directory without doing any more magic. It is 
recommended to change the `PATH` environment variable of your system to 
include the directory where you installed JaSDoc so you can run `jasdoc` from 
the command-line without specifying the installation directory.


Requirements
------------

JaSDoc is written in [Java][1] and requires the Java Runtime Environment
version 6 or higher. JaSDoc works fine with Oracle Java and OpenJDK.


Usage
-----

Run `jasdoc --help` to display a short usage information. To generate an
actual API documentation you just have to specify the JavaScript files or
folders which are scanned recursively for JavaScript files:

    jasdoc /some/source/folder
    
The HTML output is written to the directory `out` in the current working
directory. You can change the output directory with the `-d` option:

    jasdoc -d /tmp/apidoc /some/source/folder
    

Donation
--------

If you like JaSDoc, please consider making a small [donation][10] to support
my work on free software like this. Thank you!

[![Donate for JaSDoc][11]][10]
    

Build from source
-----------------

If you prefer compiling JaSDoc yourself then you need the 
[Java Development Kit][12] version 6 or higher, [Maven][13] version 3 or
higher and [Git][14]. If these tools are installed correctly then you can
clone the JaSDoc source code from GitHub and compile it with these commands:

    git clone https://github.com/kayahr/jasdoc.git
    cd jasdoc
    mvn package
    
When compilation was successful then the binary ZIP file can be found in the
`target` directory. Compilation may take several minutes on the first run
because Maven must download tons of dependencies. So please be patient.

Thanks to Maven JaSDoc can be easily imported into Java IDEs like [Eclipse][15] 
(With [m2e plugin][16]), [NetBeans IDE][17] and [IntelliJ IDEA][18].


Reports bugs
------------

JaSDoc is pretty new and may still contain dozens of bugs. And JavaScript is
a flexible language allowing many different coding strategies which may
confuse JaSDoc. If JaSDoc doesn't work with your JavaScript code or you found
any other kind of problem then please file an [issue][7]. If possible attach
an example of your code which isn't handled correctly by JaSDoc.


License
-------

Copyright (C) 2012 Klaus Reimer <k@ailis.de>

This program is free software: you can redistribute it and/or modify it
under the terms of the [GNU General Public License][9] as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the [GNU General Public License][9] for
more details.


[1]: http://java.com/ "Java Runtime Environment"
[2]: http://www.mozilla.org/rhino/ "JavaScript for Java"
[3]: https://developers.google.com/closure/compiler/docs/js-for-compiler#types "Closure Compiler JSDoc Type Expressions"
[4]: https://developers.google.com/closure/compiler/ "Closure Compiler"
[5]: http://jsoup.org/
[6]: http://freemarker.org/ "FreeMarker Template Engine"
[7]: https://github.com/kayahr/jasdoc/issues "JaSDoc Issue Tracking"
[9]: https://github.com/kayahr/jasdoc/blob/master/LICENSE.md "JaSDoc license"
[10]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M4SAJQ8NMWDZC "Donation for JaSDoc"
[11]: https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif "Donate button image"
[12]: http://www.oracle.com/technetwork/java/javase/downloads/index.html "Java Development Kit"
[13]: http://maven.apache.org/ "Maven"
[14]: http://git-scm.com/ "Git"
[15]: http://www.eclipse.org/ "Eclipse"
[16]: http://www.eclipse.org/m2e/ "Eclipse m2e plugin"
[17]: http://netbeans.org/ "NetBeans IDE"
[18]: http://www.jetbrains.com/idea/ "IntelliJ IDEA"
